# PhD directory
Repository for my codes of PhD thesis

## project-tree

* dynamic impact - Folder for impacted glass plate calculations
  * glass_1l_elastic_impact - Impact of single-layer float glass without damage and damping
  * lg_elastic_impact - Impact of multi-layer laminated glass without damage and damping
  * lg_viscoelastic_impact - Damped impact of multi-layer laminated glass without damage
* exp_pp - Postprocess of experimental data
* pf_beams - benchmark tests of fragile beam model