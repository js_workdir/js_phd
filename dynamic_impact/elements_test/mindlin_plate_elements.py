# -------------------------
# mindlin_plate_elements.py
# -------------------------

# -------------------------
# Description:
# - Mindlin plate model
# - Test of different elements
# - Implemented in fenics
#
# Last edit: 20.11. 2022
# -------------------------


import fenics as fe
import numpy as np
import matplotlib.pyplot as plt


# --------------------
# Plate solver
# --------------------
def plate_solver(el_type, sup_type, h_i, el_type_2, degrees):
    def eps(v):
        return fe.sym(fe.grad(v))

    def edge(x, on_boundary):
        return on_boundary

    # Geometry paramaters
    l, b, h = 1.0, 1.0, h_i  # Width, Height, Thickness

    # Number of elements
    nx, ny = 10, 10

    # Degree orders
    deg = degrees[0]  # Order of elements
    deg_displ = degrees[1]
    deg_shear = degrees[2]  # Order of shear elements

    # Material parameters
    E, nu = 1, 0.3  # Young's module, Poisson ratio
    mu, lmbda = 0.5 * E / (1 + nu), E * nu / (1 + nu) / (1 - 2 * nu)  # Lame's coefficients

    elem_type = fe.CellType.Type.quadrilateral
    #elem_type = dfx.mesh.CellType.quadrilateral
    if el_type == "rect":
        elem_type = fe.CellType.Type.quadrilateral
        #elem_type = dfx.mesh.CellType.quadrilateral
    elif el_type == "tri":
        elem_type = fe.CellType.Type.triangle
        #elem_type = dfx.mesh.CellType.triangle

    mesh = fe.RectangleMesh.create([fe.Point(0.0, 0.0), fe.Point(l, b)], [nx, ny], elem_type)
    #mesh = dfx.mesh.create_rectangle(MPI.COMM_WORLD, [np.array([0.0, 0.0]), np.array([l, b])], [nx, ny],
    #                                 elem_type)

    # Define spaces
    print(mesh.ufl_cell())
    element = fe.MixedElement([fe.VectorElement(el_type_2, mesh.ufl_cell(), deg),
                                fe.FiniteElement(el_type_2, mesh.ufl_cell(), deg)])
    V = fe.FunctionSpace(mesh, element)

    theta_tr, w_tr = fe.TrialFunctions(V)
    # X_tr = ufl.TrialFunction(V)
    theta_test, w_test = fe.TestFunctions(V)
    # X_test = ufl.TestFunction(V)
    S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])
    # dx_shear = ufl.Measure("dx", metadata={"quadrature_degree": 0})
    # dx_shear = ufl.dx
    metadata = {"quadrature_degree": deg_shear}
    dx_shear = fe.Measure("dx", metadata=metadata)
    metadata_2 = {"quadrature_degree": deg_displ}
    dx_displ = fe.Measure("dx", metadata=metadata_2)
    # dm(0, { “quadrature_degree”: 3 })
    k = 5.0 / 6.0

    u_D = fe.Function(V)
    #V0 = V.sub(1).collapse()

    #tdim = mesh.topology.dim
    #fdim = tdim - 1
    #mesh.topology.create_connectivity(fdim, tdim)
    #print(dfx.mesh.exterior_facet_indices)
    # plot_fce(None, "mesh", False)
    #boundary_facets = dfx.mesh.exterior_facet_indices(mesh.topology)
    # a = dfx.cpp.mesh.compute_boundary_facets(mesh.topology)
    # b = np.where(np.array(a) == 1)[0]

    # dfx.fem.locate

    # boundary_dofs_ss = dfx.fem.locate_dofs_topological((V.sub(1), V0), fdim, b)
    #boundary_dofs_ss = dfx.fem.locate_dofs_topological(V=V.sub(1), entity_dim=fdim, entities=boundary_facets)
    #boundary_dofs_all = dfx.fem.locate_dofs_topological(V, entity_dim=fdim, entities=boundary_facets)

    bc = []
    if sup_type == "ss":
        bc.append(fe.DirichletBC(V.sub(1), fe.Constant(0.0), edge))
    elif sup_type == "clamped":
        bc.append(fe.DirichletBC(V.sub(0), fe.Constant((0.0, 0.0)), edge))
        bc.append(fe.DirichletBC(V.sub(1), fe.Constant(0.0), edge))

    # Forms for matrices K and M
    K_form = 0.0
    D1 = (E * h ** 3) / (12.0 * (1.0 - nu ** 2))
    kappa_tr, kappa_test = eps(S * theta_tr), eps(S * theta_test)
    K_form += fe.inner(D1 * (nu * fe.div(S * theta_tr) * fe.Identity(2) + (1.0 - nu) * kappa_tr),
                        kappa_test) * dx_displ
    D2 = (E * k * h) / (2.0 * (1.0 + nu))
    K_form += D2 * fe.inner(fe.grad(w_tr) + S * theta_tr, fe.grad(w_test) + S * theta_test) * dx_shear
    f_ext = 1.0
    F_form = f_ext * w_test * fe.dx

    #problem = dfx.fem.petsc.LinearProblem(K_form, F_form, bcs=bc,
    #                                      petsc_options={"ksp_type": "preonly", "pc_type": "lu"})
    X = fe.Function(V)
    fe.solve(K_form == F_form, X, bcs=bc)
    #X = problem.solve()

    #w_fin = X.sub(1).collapse()
    phi_fin, w_fin = X.split(deepcopy=True)

    # cell_candidates = dolfinx.geometry.compute_collisions_point(bb_tree, point)
    # cell = dolfinx.geometry.select_colliding_cells(mesh, cell_candidates, point, 1)

    #print(w_fin.x.array.real)
    #w_max = np.amax(w_fin.x.array.real)
    w_max = np.amax(w_fin.vector().get_local())
    defl = w_max * D1 / (f_ext * l ** 4)

    return l / h, defl


Hs = np.linspace(0.0001, 0.02, 10)
ws_tri_11 = []
ws_tri_20 = []
ws_quad_11 = []
ws_quad_42 = []
ws_quad_12 = []
ws_quad_red = []
xx = []
exact = []
#exact_val = 0.0138*1*1**4/(1*)
for hhi in Hs:
    sol1, sol2 = plate_solver("rect", "clamped", hhi, "P", [1, 2, 0])
    xx.append(sol1)
    ws_quad_11.append(sol2)
    #exact.append(0.00126725)
    exact.append(0.00126532)

for hhi in Hs:
    #sol1, sol2 = plate_solver("tri", "clamped", hhi, "P", [1, 1, 1])
    ws_tri_11.append(sol2)
    #sol1, sol2 = plate_solver("tri", "clamped", hhi, "P", [2, 1])
    ws_tri_20.append(sol2)
    sol1, sol2 = plate_solver("rect", "clamped", hhi, "P", [2, 4, 2])
    ws_quad_42.append(sol2)
    #sol1, sol2 = plate_solver("rect", "clamped", hhi, "P", [1, 2])
    ws_quad_12.append(sol2)
    sol1, sol2 = plate_solver("rect", "clamped", hhi, "P", [2, 2, 2])
    ws_quad_red.append(sol2)


#plt.plot(xx, ws_tri_11, label="tri_11")
#plt.plot(xx, ws_tri_20, label="tri_20")
plt.plot(xx, ws_quad_11, label="quad_11")
plt.plot(xx, ws_quad_42, label="quad_22")
plt.plot(xx, ws_quad_red, label="quad_red")
#plt.plot(xx, ws_quad_12, label="quad_12")
plt.plot(xx, exact)
plt.legend()
plt.semilogx()
plt.show()
