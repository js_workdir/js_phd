# -------------------------
# hertzlaw_2D_pp.py
# -------------------------

# -------------------------
# Description:
# - Postprocess file for first 1l glass
#
# Last edit: 23.11. 2022
# -------------------------

import numpy as np
import matplotlib.pyplot as plt


# --------------------
# Functions
# --------------------
def plot_num_01(data_i, label_i, coef, ind_i):
    plt.plot(data_i[:, 0], coef*data_i[:, ind_i], label=label_i)


# --------------------
# Data loading
# --------------------
# Experiments
data_exp_1_20 = np.loadtxt("exp/obyc1_20cm.csv", delimiter=",")  # Crack
#data_exp_1_30 = np.loadtxt("exp/obyc1_30cm.csv", delimiter=",")  # Crack
data_exp_2_35 = np.loadtxt("exp/obyc2_35cm.csv", delimiter=",")  # Crack

# numerics
data_num_001 = np.loadtxt("solutions/large/first_test/impactor.txt")
data_num_002 = np.loadtxt("solutions/small/plate_mesh_test_100_rect/impactor.txt")
data_num_003 = np.loadtxt("solutions/small/plate_01_001/impactor.txt")
data_num_004 = np.loadtxt("solutions/small/plate_01_002/impactor.txt")
data_num_005 = np.loadtxt("solutions/small/plate_01_005/impactor.txt")
data_num_006 = np.loadtxt("solutions/small/plate_01_006/impactor.txt")
data_num_006_2 = np.loadtxt("solutions/small/plate_01_006_kshear/impactor.txt")
data_num_007 = np.loadtxt("solutions/small/gc_change_02/impactor.txt")
data_num_008 = np.loadtxt("solutions/small/gc_change_03/impactor.txt")
data_num_011 = np.loadtxt("solutions/small/gc_change_04/impactor.txt", skiprows=1)
data_num_012 = np.loadtxt("solutions/small/gc_change_06/impactor.txt", skiprows=1)

data_num_009 = np.loadtxt("solutions/small/plate_01_65/impactor.txt", skiprows=1)
data_num_010 = np.loadtxt("solutions/small/plate_01_100/impactor.txt", skiprows=1)
data_num_gc_01 = np.loadtxt("solutions/small/gc_change_07/impactor.txt", skiprows=1)
data_num_gc_02 = np.loadtxt("solutions/small/gc_change_08/impactor.txt", skiprows=1)
data_num_gc_03 = np.loadtxt("solutions/small/gc_change_09/impactor.txt", skiprows=1)

m_imp = 48.2

# --------------------
# Plotting
# --------------------
plot_num_01(data_exp_1_20, "exp1", 1, 2)
#plot_num_01(data_exp_2_35, "exp2", 1, 2)
plt.plot(data_num_gc_01[:, 0], -data_num_gc_01[:, 2]*m_imp, label="gc_1")
plt.plot(data_num_gc_02[:, 0], -data_num_gc_02[:, 2]*m_imp, label="gc_2")
plt.plot(data_num_gc_03[:, 0], -data_num_gc_03[:, 2]*m_imp, label="gc_3")
plt.xlabel("Time [s]")
plt.ylabel("Contact force [N]")
plt.legend()
plt.show()

#plot_num_01(data_exp_1_20, "exp1", 1, 2)
plot_num_01(data_exp_2_35, "exp2", 1, 2)
plt.plot(data_exp_1_20[:, 0]-0.0001, data_exp_1_20[:, 2], label="exp")
plt.plot(data_num_009[:, 0], -data_num_009[:, 2]*m_imp, label="num_3")
plt.plot(data_num_010[:, 0], -data_num_010[:, 2]*m_imp, label="num_01_100")
plt.plot(data_num_011[:, 0], -data_num_011[:, 2]*m_imp, label="gc_04")
plt.plot(data_num_012[:, 0], -data_num_012[:, 2]*m_imp, label="gc_06")
plt.plot(data_num_gc_01[:, 0], -data_num_gc_01[:, 2]*m_imp, label="gc_1")
plt.plot(data_num_gc_02[:, 0], -data_num_gc_02[:, 2]*m_imp, label="gc_2")
plt.plot(data_num_006_2[:, 0], -data_num_006_2[:, 2]*m_imp, label="num_3")
plt.xlabel("Time [s]")
plt.ylabel("Contact force [N]")
plt.legend()
plt.show()

plot_num_01(data_exp_1_20, "exp1", 1, 2)
plot_num_01(data_exp_2_35, "exp2", 1, 2)
plt.plot(data_num_007[:, 0], -data_num_007[:, 2]*m_imp, label="num_3")
plt.xlabel("Time [s]")
plt.ylabel("Contact force [N]")
plt.legend()
plt.show()


# Experiments - contact force for 5 cm
#plt.plot(data_num_003[:, 0], data_num_003[:, 2], label="num1")
#plt.plot(data_num_005[:, 0], data_num_005[:, 2], label="num2")
plot_num_01(data_exp_1_20, "exp1", 1, 2)
#plot_num_01(data_exp_1_30, "exp2", 1, 2)
plt.plot(data_num_006[:, 0], -data_num_006[:, 2]*48.2, label="num_1")
plt.plot(data_num_006_2[:, 0], -data_num_006_2[:, 2]*48.2, label="num_2")
plt.plot(data_num_007[:, 0], -data_num_007[:, 2]*48.2, label="num_3")
plt.plot(data_num_008[:, 0], -data_num_008[:, 2]*48.2, label="num_3")
plt.xlabel("Time [s]")
plt.ylabel("Contact force")
plt.legend()
plt.show()
