# -------------------------
# hertzlaw_first_code.py
# -------------------------

# -------------------------
# Description:
# - Impact of steel impactor into small glass plate
# - Implemented with damage via phasefield fracture model
# - Contact by Hertz law
# - Implemented in FEniCS
# - Explicit integration scheme
#
# Last edit: 08.11. 2022
# -------------------------

import fenics as fe
import matplotlib.pyplot as plt
import math
import numpy as np
import time
import os
import lumping_scheme_quad

from petsc4py import PETSc
from mshr import Polygon, generate_mesh


# --------------------
# Functions and classes
# --------------------
def left(x, on_boundary):
    return on_boundary and x[0] < 0.0001


def right_edge(x, on_boundary):
    return on_boundary and fe.near(x[0], 0.5 * l_x)


def top_edge(x, on_boundary):
    return on_boundary and fe.near(x[1], 0.5 * l_y)


# Projection on each element
def local_project(fce, space):
    lp_trial, lp_test = fe.TrialFunction(space), fe.TestFunction(space)
    lp_a = fe.inner(lp_trial, lp_test)*fe.dx
    lp_L = fe.inner(fce, lp_test)*fe.dx
    local_solver = fe.LocalSolver(lp_a, lp_L)
    local_solver.factorize()
    lp_f = fe.Function(space)
    local_solver.solve_local_rhs(lp_f)
    return lp_f


# Maximum of two function with UFL
def max_fce(a, b):
    return 0.5*(a+b+abs(a-b))


def eps(v):
    return fe.sym(fe.grad(v))


def sigma(v):
    return lmbda * fe.tr(eps(v)) * fe.Identity(3) + 2.0 * mu * eps(v)


def sigma_vd(u_i, d_i):
    # return sigma_p_vd(u_i, lmbda, mu) + sigma_n_vd(u_i, lmbda, mu)
    return ((1-d_i)**2 + k_res)*sigma_p_vd(u_i) + sigma_n_vd(u_i)


# Positive stress tensor - volumetric-deviatoric
def sigma_p_vd(u_i):
    kn = lmbda + mu
    return kn*mc_bracket(fe.tr(eps(u_i)))*fe.Identity(2) + 2*mu*fe.dev(eps(u_i))


# Negative stress tensor - volumetric-deviatoric
def sigma_n_vd(u_i):
    kn = lmbda + mu
    return -kn*mc_bracket(-fe.tr(eps(u_i)))*fe.Identity(2)


def sigma_sd(u_i, d_i):
    return ((1 - d_i) ** 2 + k_res) * sigma_p_sd(u_i) + sigma_n_sd(u_i)


# Positive stress tensor - spectral decomposition
def sigma_p_sd(u_i):
    return lmbda*(fe.tr(eps_p(u_i)))*fe.Identity(2) + 2.0*mu*(eps_p(u_i))


# Negative stress tensor - spectral decomposition
def sigma_n_sd(u_i):
    return lmbda*(fe.tr(eps_n(u_i)))*fe.Identity(2) + 2.0*mu*(eps_n(u_i))


# positive strain tensor
def eps_p(v):
    v00, v01, v10, v11 = eig_v(eps(v))
    v00 = fe.conditional(fe.gt(v00, 0.0), v00, 0.0)
    v11 = fe.conditional(fe.gt(v11, 0.0), v11, 0.0)
    w00, w01, w10, w11 = eig_w(eps(v))
    wp = ([w00, w01], [w10, w11])
    wp = fe.as_tensor(wp)
    vp = ([v00, v01], [v10, v11])
    vp = fe.as_tensor(vp)
    return wp*vp*fe.inv(wp)


# negative strain tensor
def eps_n(v):
    v00, v01, v10, v11 = eig_v(eps(v))
    v00 = fe.conditional(fe.lt(v00, 0.0), v00, 0.0)
    v11 = fe.conditional(fe.lt(v11, 0.0), v11, 0.0)
    w00, w01, w10, w11 = eig_w(eps(v))
    wn = ([w00, w01], [w10, w11])
    wn = fe.as_tensor(wn)
    vn = ([v00, v01], [v10, v11])
    vn = fe.as_tensor(vn)
    return wn*vn*fe.inv(wn)


# eigenvalues for 2x2 matrix
def eig_v(a):
    v00 = a[0, 0]/2 + a[1, 1]/2 - fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2
    v01 = 0.0
    v10 = 0.0
    v11 = a[0, 0]/2 + a[1, 1]/2 + fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2
    return v00, v01, v10, v11


# eigenvectors for 2x2 matrix
def eig_w(a):
    w00 = -a[0, 1]/(a[0, 0]/2 - a[1, 1]/2 + fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2)
    w01 = -a[0, 1]/(a[0, 0]/2 - a[1, 1]/2 - fe.sqrt(a[0, 0]**2 - 2*a[0, 0]*a[1, 1] + 4*a[0, 1]*a[1, 0] + a[1, 1]**2)/2)
    w10 = 1.0
    w11 = 1.0
    return w00, w01, w10, w11


def create_log_file(folder, p_type, model_type, mesh):
    directory = os.path.dirname(folder + "/")
    if not os.path.exists(directory):
        os.makedirs(directory)
    #f = open(folder + "/log.txt", "w")
    #f.write("Model type: " + str(model_type) + "\n")
    #if model_type == "plate":
    #    f.write("Number of elements: " + str((n_x, n_y)) + "\n")
    #elif model_type == "3d":
    #    f.write("Number of elements: " + str((n_x, n_y, n_z)) + "\n")
    #f.write("Material parameters: E=" + str(E) + ", nu=" + str(nu) + ", rho=" + str(rho) + "\n")
    #f.write("Impactor: m=" + str(m_imp) + ", E=" + str(E_2) + ", nu=" + str(nu_2) + "\n")
    #f.write("Impact height: h0=" + str(h0) + "\n")
    #f.write("Elements: " + str(mesh.ufl_cell()) + "-" + str(deg) + "\n")
    #f.write("Point-load type: " + p_type + "\n")
    f = open(folder + "/log.txt", "w")
    f.write("Number of elements: " + str((n_x, n_y)) + "\n")
    f.write("Material parameters: E=" + str(E) + ", nu=" + str(nu) + ", rho=" + str(rho) + ", ft=" + str(ft_glass) +
            ", lc=" + str(lc) + ", Gc=" + str(Gc_glass) + "\n")
    f.write("Impactor: m=" + str(m_imp) + ", E=" + str(E_2) + ", nu=" + str(nu_2) + "\n")
    f.write("Impact height: h0=" + str(h0) + "\n")
    f.write("Elements: " + str(mesh.ufl_cell()) + "-" + str(deg) + "\n")
    f.write("Point-load type: " + p_type + "\n")
    f.write("Model parameters: n_ni=" + str(n_ni) + ", k_res=" + str(k_res) + ", xi_shear=" + str(xi_shear) +
            ", k_shear=" + str(k_shear) + "\n")


def update_values():
    ddu_old.assign(ddu)
    ddu.assign(4/(dt*dt)*(u-u_bar))
    du.assign(du + 0.5*dt*(ddu + ddu_old))


def mc_bracket(x):
    return 0.5*(x+abs(x))


def calc_time_solution(file):
    impactor = []
    dimpactor = []
    ddimpactor = []
    tt = []
    plate = []
    dplate = []
    ddplate = []

    u_imp = 0.0
    du_imp = v0
    ddu_imp = 0.0

    xdmffile_u = fe.XDMFFile(file + "/u.xdmf")
    xdmffile_u.parameters["flush_output"] = True

    t = t_start
    while t < t_end:

        u_bar.assign(u + dt * du + 0.25 * dt * dt * ddu)
        u_bar_imp = u_imp + dt * du_imp + 0.25 * dt * dt * ddu_imp

        u_vec = u.vector()
        u_bar_vec = u_bar.vector()
        #
        error = 10.0

        u_temp = fe.Function(V)
        u_temp.assign(u)

        iterr = 0.0
        while error > tol:
            EOM = K * u_vec + 4.0 / (dt * dt) * M * (u_vec - u_bar_vec)
            J = K + 4.0 / (dt * dt) * M
            dF_hertz = 1.5 * k_0 * mc_bracket(u_imp - u_temp((l_x / 2, l_y / 2, 0.0))[2]) ** 0.5
            F_hertz = k_0 * mc_bracket(u_imp - u_temp((l_x / 2, l_y / 2, 0.0))[2]) ** 1.5
            p1 = fe.PointSource(V.sub(2), fe.Point(l_x / 2, l_y / 2, 0.0), -F_hertz)
            p2 = fe.PointSource(V.sub(2), fe.Point(l_x / 2, l_y / 2, 0.0), dF_hertz)
            p2.apply(J)
            p1.apply(EOM)

            fe.solve(J, delta_u.vector(), EOM)
            u_new = u_vec - delta_u.vector()
            EOM = K * u_new + 4.0 / (dt * dt) * M * (u_new - u_bar_vec)
            p1.apply(EOM)
            # error1 = (u_new - u_vec).norm("l2")
            error1 = EOM.norm("l2")
            # error1 = K*u_new + (4.0/(dt*dt)*M*(u_new - u_bar_vec))
            u_temp.vector()[:] = u_new
            dF_hertz = 1.5 * k_0 * mc_bracket(u_imp - u_temp((l_x / 2, l_y / 2, 0.0))[2]) ** 0.5
            F_hertz = k_0 * mc_bracket(u_imp - u_temp((l_x / 2, l_y / 2, 0.0))[2]) ** 1.5

            J2 = 4.0 / (dt * dt) * m_imp + dF_hertz
            EOM2 = 4.0 / (dt * dt) * m_imp * (u_imp - u_bar_imp) + F_hertz

            u_imp_new = u_imp - EOM2 / J2
            # print(EOM2)
            # print(J2)
            error2 = (m_imp * (4.0 / (dt * dt) * (u_imp_new - u_bar_imp)) + F_hertz)
            # error2 = (m_imp * (4.0 / (dt * dt) *(0.25, 0.25, 0.0) (u_imp_new - u_bar_imp)) + F_hertz)

            error = max(abs(error1), abs(error2))
            print("error1 = ", error1, ", error2 = ", error2)
            print("t = ", t)

            if iterr > max_iter:
                print("max iterations reached")
                break

            iterr += 1

            u_vec = u_new
            u_imp = u_imp_new
            u_temp.vector()[:] = u_vec

        u.vector()[:] = u_vec

        ddu_old.assign(ddu)
        ddu.assign(4 / (dt * dt) * (u - u_bar))
        du.assign(du + 0.5 * dt * (ddu + ddu_old))

        ddu_old_imp = ddu_imp
        ddu_imp = 4 / (dt * dt) * (u_imp - u_bar_imp)
        du_imp = du_imp + 0.5 * dt * (ddu_imp + ddu_old_imp)

        xdmffile_u.write(u, t)

        tt.append(t)
        impactor.append(u_imp)
        dimpactor.append(du_imp)
        ddimpactor.append(ddu_imp)
        plate.append(u((l_x / 2, l_y / 2, 0.0))[2])
        dplate.append(du((l_x / 2, l_y / 2, 0.0))[2])
        ddplate.append(ddu((l_x / 2, l_y / 2, 0.0))[2])

        t = t + dt

    xdmffile_u.close()

    end = time.time()

    np.savetxt(file + "/output_data.txt", np.column_stack((tt, impactor, dimpactor, ddimpactor, plate, dplate, ddplate)),
               header="t\tu_imp\tdu_imp\tddu_imp\tu_plate\tdu_plate\tddu_plate")

    plt.plot(tt, ddimpactor)
    plt.ylabel("t")
    plt.xlabel("Acceleration of impactor")
    plt.figure()
    plt.plot(tt, impactor, label="impactor")
    plt.plot(tt, plate, label="plate")
    plt.legend()
    plt.xlabel("t")
    plt.ylabel("Displacement")
    plt.show()


def get_K_form(V, model_type):
    if model_type == "plate":
        dx_shear = fe.dx(metadata={"quadrature_degree": 0})
        k = 5.0 / 6.0

        S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

        w_tr, u_tr, phi_tr, imp_tr = fe.TrialFunctions(V)
        w_test, u_test, phi_test, imp_test = fe.TestFunctions(V)

        K_form = 0.0

        D1 = (E * l_z ** 3) / (12.0 * (1.0 - nu ** 2))
        D2 = (E * k * l_z) / (2.0 * (1.0 + nu))
        D3 = (E * l_z) / (1.0 - nu ** 2)
        kappa_tr, kappa_test = eps(S * phi_tr), eps(S * phi_test)
        du_tr, du_test = eps(u_tr), eps(u_test)
        K_form += fe.inner(D1 * (nu * fe.div(S * phi_tr) * fe.Identity(2) + (1.0 - nu) * kappa_tr), kappa_test) * fe.dx
        K_form += fe.inner(D3 * (nu * fe.div(u_tr) * fe.Identity(2) + (1.0 - nu) * du_tr), du_test) * fe.dx
        K_form += D2 * fe.inner(fe.grad(w_tr) + S * phi_tr, fe.grad(w_test) + S * phi_test) * dx_shear
    elif model_type == "3d":
        imp_tr, u_tr = fe.TrialFunctions(V)
        imp_test, u_test = fe.TestFunctions(V)
        K_form = fe.inner(sigma(u_tr), eps(u_test))*fe.dx
    else:
        raise Exception("Only plate/3d model_type implemented!")

    return K_form


def get_K_form_vector(V, u, d, dec_type):
    dx_shear = fe.dx(metadata={"quadrature_degree": 0})
    k = 5.0/6.0

    S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

    w_tr, u_tr, phi_tr, imp_tr = fe.split(u)
    #w_tr, u_tr, phi_tr, imp_tr = fe.TrialFunctions(V)
    w_test, u_test, phi_test, imp_test = fe.TestFunctions(V)
    #w_tr, u_tr, phi_tr = map_to_layers(lg_trial, H)
    #w_test, u_test, phi_test = map_to_layers(lg_test, H)

    K_form = 0.0

    num_int = np.linspace(-0.5*l_z, 0.5*l_z, n_ni)
    dh = abs(num_int[1] - num_int[0])

    for i in range(len(num_int) - 1):
        gauss = 0.5*(num_int[i] + num_int[i+1])
        u_z = u_tr + S*phi_tr*gauss
        u_z_test = u_test + S*phi_test*gauss
        #K_form += (1-d)**2*fe.inner(sigma(u_z), eps(u_z_test))*dh*fe.dx
        if dec_type == "hyb_sd":
            K_form += (1-d)**2*fe.inner(sigma(u_z), eps(u_z_test))*dh*fe.dx
        elif dec_type == "vol":
            print("hej")
            K_form += fe.inner(sigma_vd(u_z, d), eps(u_z_test))*dh*fe.dx
        elif dec_type == "sd":
            K_form += fe.inner(sigma_sd(u_z, d), eps(u_z_test))*dh*fe.dx
        else:
            raise Exception("Only hyb_sd/vol/sd decomposition implemented!")

    #K_form *= (1-d)**2
    #u_form += D2 * fe.inner(fe.grad(w_) + S * theta_, fe.grad(w_test) + S * theta_test) * dx_shear
    #u_form -= fe.Constant(0.0) * w_test * fe.dx

    #D1 = (E*H**3)/(12.0*(1.0 - nu**2))
    D2 = (E*k*l_z)/(2.0*(1.0 + nu))
    #D3 = (E*H)/(1.0 - nu**2)
    #K_form += fe.inner(D1*(nu*fe.div(S*phi_tr)*fe.Identity(2) + (1.0 - nu)*kappa_tr),kappa_test)*fe.dx
    #K_form += fe.inner(D3*(nu*fe.div(u_tr)*fe.Identity(2) + (1.0 - nu)*du_tr), du_test)*fe.dx
    #K_form += ((1-d)**2 + k_res)*D2*fe.inner(fe.grad(w_tr) + S*phi_tr, fe.grad(w_test) + S*phi_test)*dx_shear
    #K_form += D2*fe.inner(fe.grad(w_tr) + S*phi_tr, fe.grad(w_test) + S*phi_test)*dx_shear

    if xi_shear:
        K_form += D2*fe.inner(fe.grad(w_tr) + S*phi_tr, fe.grad(w_test) + S*phi_test)*dx_shear
    else:
        print("hej2")
        K_form += ((1-d)**2 + k_shear)*D2*fe.inner(fe.grad(w_tr) + S*phi_tr, fe.grad(w_test) + S*phi_test)*dx_shear

    return K_form


def get_M_form(V, lump, model_type):
    if model_type == "plate":
        if lump:
            if el_type == "rect":
                dx_m = fe.dx(scheme="lumped", degree=2)
            elif el_type == "tri":
                dx_m = fe.dx(scheme="vertex", metadata={"degree": 1, "representation": "quadrature"})
        else:
            dx_m = fe.dx

        w_tr, u_tr, phi_tr, imp_tr = fe.TrialFunctions(V)
        w_test, u_test, phi_test, imp_test = fe.TestFunctions(V)
        S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

        M_form = 0.0

        M_form += rho * l_z ** 3 / 12.0 * fe.dot(S * phi_tr, S * phi_test) * dx_m
        M_form += rho * l_z * fe.dot(w_tr, w_test) * dx_m
        M_form += rho * l_z * fe.dot(u_tr, u_test) * dx_m

        M_form_2 = rho_imp_2d * fe.dot(imp_tr, imp_test) * fe.dx
    elif model_type == "3d":
        if lump:
            if el_type == "rect":
                dx_m = fe.dx(scheme="lumped", degree=2)
            elif el_type == "tri":
                print("hej3d")
                dx_m = fe.dx(scheme="lumped", degree=2)
                #dx_m = fe.dx(scheme="vertex", metadata={"degree": 1, "representation": "quadrature"})
        else:
            dx_m = fe.dx

        imp_tr, u_tr = fe.TrialFunctions(V)
        imp_test, u_test = fe.TestFunctions(V)

        M_form = rho * fe.dot(u_tr, u_test) * dx_m

        M_form_2 = rho_imp_3d * fe.dot(imp_tr, imp_test) * fe.dx
    else:
        raise Exception("Only plate/3d model_type implemented!")
    return M_form, M_form_2


# Return formulation for damage
def get_d_form(W, u, d, type_d, hist_var):
    d_test = fe.TestFunction(W)
    d_tr = fe.TrialFunction(W)
    #w_tr, u_tr, phi_tr, imp_tr = fe.split(u)
    #S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

    # Positive energy
    #u_z_l = u_tr + S*phi_tr*0.5*H
    #u_z_u = u_tr - S*phi_tr*0.5*H
    #en = max_fce(psi_sd_plus(u_z_l)*H, psi_sd_plus(u_z_u)*H)

    en = get_positive_energy(u)

    if type_d == "bourdin":
        #max_en = max_fce(en, hist_var)
        #hist_var_fce = fe.project(max_en, W0)
        #hist_var
        #en = max_fce(en, hist)
        en = hist_var

    d_form = 0.0
    if type_d == "pham":
        d_form += -2 * en * fe.inner(1.0 - d, d_test) * fe.dx
        d_form += l_z*3.0/8.0*Gc_glass*(1.0/lc*d_test + 2*lc*fe.inner(fe.grad(d), fe.grad(d_test)))*fe.dx
    elif type_d == "bourdin":
        d_form += -2*en*fe.inner(1.0 - d_tr, d_test)*fe.dx
        d_form += l_z*Gc_glass*(1.0/lc*fe.inner(d_tr, d_test) + lc*fe.inner(fe.grad(d_tr), fe.grad(d_test)))*fe.dx

    return d_form


def get_positive_energy(u):
    w_tr, u_tr, phi_tr, imp_tr = fe.split(u)
    S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

    # Positive energy
    u_z_l = u_tr + S*phi_tr*0.5*l_z
    u_z_u = u_tr - S*phi_tr*0.5*l_z
    return max_fce(psi_sd_plus(u_z_l)*l_z, psi_sd_plus(u_z_u)*l_z)


# Positive energy - spectral decomposition
def psi_sd_plus(u_i):
    eps_sym = eps(u_i)

    eps1 = (1. / 2.) * fe.tr(eps_sym) + fe.sqrt((1. / 4.) * (fe.tr(eps_sym) ** 2) - fe.det(eps_sym))
    eps2 = (1. / 2.) * fe.tr(eps_sym) - fe.sqrt((1. / 4.) * (fe.tr(eps_sym) ** 2) - fe.det(eps_sym))

    tr_eps_plus = mc_bracket(fe.tr(eps_sym))
    sum_eps_plus_squared = mc_bracket(eps1) ** 2 + mc_bracket(eps2) ** 2

    return 0.5*lmbda*tr_eps_plus ** 2 + mu * sum_eps_plus_squared


# Negative energy - spectral decomposition
def psi_sd_minus(u_i):
    eps_sym = eps(u_i)

    eps1 = (1. / 2.) * fe.tr(eps_sym) + fe.sqrt((1. / 4.) * (fe.tr(eps_sym) ** 2) - fe.det(eps_sym))
    eps2 = (1. / 2.) * fe.tr(eps_sym) - fe.sqrt((1. / 4.) * (fe.tr(eps_sym) ** 2) - fe.det(eps_sym))

    tr_eps_minus = -mc_bracket(-fe.tr(eps_sym))
    sum_eps_minus_squared = mc_bracket(-eps1) ** 2 + mc_bracket(-eps2) ** 2

    return 0.5 * lmbda * tr_eps_minus ** 2 + mu * sum_eps_minus_squared


def explicit_solver_plate(folder, mesh, saving, save_rate, point_type, el_type_i, dec_type_i):
    def prepare_solver_d(hist_i):
        lower = d
        # lower = hist_i
        # hist
        # threshold = fe.interpolate(fe.Constant(0.9), W)
        # lower = fe.conditional(fe.lt(d, threshold), 0.0, d)
        upper = fe.interpolate(fe.Constant(1.0), W)

        # Solution of damage formulation
        H = fe.derivative(d_form, d, fe.TrialFunction(W))

        problem = fe.NonlinearVariationalProblem(d_form, d, [], H)
        problem.set_bounds(lower, upper)

        solver = fe.NonlinearVariationalSolver(problem)
        solver.parameters.update(snes_solver_parameters)
        return solver

    # Damage snes variational inequalities solver based on Newton's method
    def solve_damage(d_type, solver):
        if d_type == "pham":
            lower = d
            upper = fe.interpolate(fe.Constant(1.0), W)

            # Solution of damage formulation
            H = fe.derivative(d_form, d, fe.TrialFunction(W))

            problem = fe.NonlinearVariationalProblem(d_form, d, [], H)
            problem.set_bounds(lower, upper)

            solver = fe.NonlinearVariationalSolver(problem)
            solver.parameters.update(snes_solver_parameters)
            solver.solve()
        elif d_type == "pham2":
            solver.solve()
            #lowerr = fe.conditional(fe.lt(d, threshold), 0.0, d)
            #hist.assign(local_project(lowerr, W0))
        elif d_type == "bourdin":
            fe.solve(fe.lhs(d_form) == fe.rhs(d_form), d, [])
            hist.assign(local_project(max_fce(hist, get_positive_energy(X)), W0))

    # --------------------
    # Define spaces
    # --------------------
    p1_element = fe.FiniteElement("P", mesh.ufl_cell(), deg)
    v1_element = fe.VectorElement("P", mesh.ufl_cell(), deg)
    r1_element = fe.FiniteElement("R", mesh.ufl_cell(), 0)
    element = fe.MixedElement([p1_element, v1_element, v1_element, r1_element])
    V = fe.FunctionSpace(mesh, element)
    V_sca = fe.FunctionSpace(mesh, p1_element)
    V_vec = fe.FunctionSpace(mesh, v1_element)
    V_rea = fe.FunctionSpace(mesh, r1_element)
    W = fe.FunctionSpace(mesh, "CG", 1)
    W0 = fe.FunctionSpace(mesh, "CG", 1)

    print("Number of unknowns: " + str(V.dim()))
    impactor = []
    impactor_acc = []
    tt = []
    u_center = []
    u_corner = []

    # Displacement functions
    X = fe.Function(V)  # Displacement in time t_i
    X_old = fe.Function(V)  # Displacement in time t_{i-1}
    X_old2 = fe.Function(V)  # Displacement in time t_{i-2}
    d = fe.Function(W)  # Damage function
    hist = fe.Function(W0)  # History function

    # Initial velocity
    fa = fe.FunctionAssigner(V, [V_sca, V_vec, V_vec, V_rea])
    s_init = fe.interpolate(fe.Constant(0.0), V_sca)
    v_init = fe.interpolate(fe.Constant((0.0, 0.0)), V_vec)
    r_init = fe.interpolate(fe.Constant(-dt * v0), V_rea)
    fa.assign(X_old2, [s_init, v_init, v_init, r_init])

    # Indices of points
    ind_center = find_dof(p_center, 0, V)
    ind_corner = find_dof(p_corner, 0, V)
    print(ind_corner)

    # Point distribution
    f_point = fe.Expression("abs(x[0] - 0.5*L) < tol & abs(x[1] - 0.5*B) < tol ? 1.0 : 0.0", tol=0.001, L=l_x,
                            B=l_y,
                            degree=1)
    f_fce_vect = fe.interpolate(f_point, V_sca)
    f_fce_real = fe.interpolate(fe.Constant(1 / (0.25 * l_x * l_y)), V_rea)

    # fe.plot(f_fce_vect)
    file_hit = fe.File(folder + "/hit_fce.pvd")
    file_hit << f_fce_vect
    # plt.show()

    w_test, u_test, phi_test, imp_test = fe.TestFunctions(V)
    f_dest_form = fe.dot(f_fce_vect, w_test) * fe.dx
    f_dest_form_2 = fe.dot(f_fce_real, imp_test) * fe.dx

    a_loc_vec_2 = fe.assemble(f_dest_form)
    a_loc_vec_2[:] = -a_loc_vec_2[:] / sum(a_loc_vec_2[:])
    a_loc_real = fe.assemble(f_dest_form_2)
    index_real = np.nonzero(a_loc_real[:])[0]
    index_hit = np.argmin(a_loc_vec_2[:])
    # print(aaa)
    # print(a_loc_vec_2[aaa])
    # p2.apply(a_loc_vec_2)

    if point_type == "point":
        a_loc_vec = a_loc_vec_1
    elif point_type == "dist":
        a_loc_vec = a_loc_vec_2 + a_loc_real
    else:
        raise Exception("Only point/dist point type implemented!")

    # Dirichlet boundary conditions
    # w1, u1, phi1, r
    bc1 = fe.DirichletBC(V.sub(2).sub(1), fe.Constant(0.0), right_edge)
    bc2 = fe.DirichletBC(V.sub(2).sub(0), fe.Constant(0.0), top_edge)
    bc3 = fe.DirichletBC(V.sub(1).sub(0), fe.Constant(0.0), right_edge)
    bc4 = fe.DirichletBC(V.sub(1).sub(1), fe.Constant(0.0), top_edge)
    bc = [bc1, bc2, bc3, bc4]

    # File with solution
    file = fe.XDMFFile(folder + "/displ_w.xdmf")
    file.parameters["flush_output"] = True
    file_u = fe.XDMFFile(folder + "/displ_u.xdmf")
    file_u.parameters["flush_output"] = True
    file_d = fe.XDMFFile(folder + "/damage_d.xdmf")
    file_d.parameters["flush_output"] = True

    # Forms for matrices K and M
    #K_form = get_K_form(V, "plate")
    K_form_vector = get_K_form_vector(V, X, d, dec_type_i)
    M_form_1, M_form_2 = get_M_form(V, True, "plate")

    # Assembling of matrices
    #K_matrix = fe.assemble(K_form)
    K_vector = fe.assemble(K_form_vector)
    [bci.apply(K_vector) for bci in bc]
    M_matrix = fe.assemble(M_form_1) + fe.assemble(M_form_2)
    [bci.apply(M_matrix) for bci in bc]

    M_fce = fe.Function(V)
    M_vect = M_fce.vector()
    M_matrix.get_diagonal(M_vect)

    d_form = get_d_form(W, X, d, "pham", hist)
    solver_d = prepare_solver_d(hist)

    # Main time loop

    saveindex = save_rate
    t = t_start
    begin_t = time.time()
    create_log_file(folder, point_type, "plate", mesh)
    while t < t_end:
        print("Time instant: " + str(t))

        # K_matrix = fe.assemble(K_form)
        # [bci.apply(K_matrix) for bci in bc]

        # Hertz nonlinear force
        # F_hertz = 0.25*k_0*mc_bracket(X_old.sub(3)(pp) - X_old.sub(0)(pp))**1.5
        #F_hertz = np.asscalar(0.25*k_0*mc_bracket(X_old.vector()[index_real] - X_old.vector()[index_hit])**1.5)
        F_hertz = np.asscalar(0.25 * k_0 * mc_bracket(X_old.vector()[index_real] - X_old.vector()[index_hit]) ** 1.5)
        # F_hertz = 0

        # Update of RHS
        w_vector = -dt ** 2 * K_vector - M_matrix * (
                    X_old2.vector() - 2.0 * X_old.vector()) - dt ** 2 * F_hertz * a_loc_vec
        #w_vector = -dt ** 2 * K_matrix*X_old.vector() - M_matrix * (
        #            X_old2.vector() - 2.0 * X_old.vector()) - dt ** 2 * F_hertz * a_loc_vec
        # w_vector = -dt**2*K_matrix*X_old.vector() - M_matrix*(X_old2.vector() - 2.0*X_old.vector()) -dt**2*F_hertz*a_loc_vec
        [bci.apply(w_vector) for bci in bc]

        iMw = fe.Function(V)
        iMw.vector().set_local(w_vector.get_local() / M_vect.get_local())
        X.assign(iMw)

        solve_damage("pham2", solver_d)

        K_vector = fe.assemble(K_form_vector)
        [bci.apply(K_vector) for bci in bc]

        if saving:
            if saveindex >= save_rate:
                w, u, phi, u_imp = X.split(deepcopy=True)
                file.write(w, t)
                file_u.write(u, t)
                file_d.write(d, t)

                # impactor.append(u_imp(pp))
                impactor.append(X.vector()[index_real])
                temp_acc = X.vector()[index_real] - 2*X_old.vector()[index_real] + X_old2.vector()[index_real]
                temp_acc /= dt**2
                impactor_acc.append(temp_acc)
                tt.append(t)
                u_center.append(X.vector()[ind_center])
                u_corner.append(X.vector()[ind_corner])

                saveindex = 0
            else:
                saveindex += 1

        X_old2.assign(X_old)
        X_old.assign(X)

        t += dt

    end_t = time.time()
    file.close()

    np.savetxt(folder + "/impactor.txt", np.column_stack([tt, impactor, impactor_acc, u_center, u_corner]))

    return end_t - begin_t, impactor, impactor_acc, tt


def find_dof(p, d, VS):
    v_dofs = VS.tabulate_dof_coordinates()
    v0_dofs = VS.sub(d).dofmap().dofs()
    for i in range(0, len(v0_dofs)):
        v_x = v_dofs[v0_dofs[i], 0]
        v_y = v_dofs[v0_dofs[i], 1]
        if fe.near(v_x, p.x()) and fe.near(v_y, p.y()):
            return v0_dofs[i]
    return -1


def find_dof_3d(p, d, VS):
    v_dofs = VS.tabulate_dof_coordinates()
    v0_dofs = VS.sub(1).sub(d).dofmap().dofs()
    for i in range(0, len(v0_dofs)):
        v_x = v_dofs[v0_dofs[i], 0]
        v_y = v_dofs[v0_dofs[i], 1]
        v_z = v_dofs[v0_dofs[i], 2]
        if fe.near(v_x, p.x()) and fe.near(v_y, p.y()) and fe.near(v_z, p.z()):
            return v0_dofs[i]
    return -1


# --------------------
# Parameters
# --------------------
n_x, n_y, n_z = 100, 100, 1  # Number of elements
l_x, l_y, l_z = 0.5, 0.5, 0.0146  # Dimensions of sample
#E = 70.0e9  # Young's modulus of glass
#nu = 0.23  # Poisson's ratio of glass
E, nu = 72.0e9, 0.22
E_2 = 210.0e9  # Young's modulus of impactor
nu_2 = 0.3  # Poisson's ratio of impactor
R = 0.05  # Radius of impactor
rho = 2500.0  # Glass density
# F = 1.0
#m_imp = 52.36  # Impactor mass
#m_imp = 48.2  # Impactor mass
m_imp = 48.7
h0 = 0.2  # initial height of impactor
v0 = math.sqrt(2*9.81*h0)
sur = l_x * l_y  # Area of mesh
vol = l_x * l_y * l_z  # Volume of mesh
rho_imp_2d = m_imp / sur  # Impactor weight distributed across whole 2d mesh
#rho_imp_3d = m_imp / vol  # Impactor weight distributed across whole 3d mesh
#k = 1.0e6
#c0 = 1.0e9

# Material parameters
#E, nu, rho = 72e9, 0.22, 2500.0  # Young's module, Poisson ratio, density
#mu, lmbda = 0.5*E/(1+nu), E*nu/(1+nu)/(1-2*nu)  # Lame's coefficients
#lmbda = 2*mu*lmbda/(lmbda+2*mu)  # Plane stress correction
ft_glass = 55.0e6  # Strengths of glass
# Fracture parameters
h_min = l_x/n_x
#lc = h_min
lc=0.010606601717798144
print("lc=", lc)
Gc_glass = lc*8.0/3.0*ft_glass**2/E
print("Gc_glass=", Gc_glass)
k_res = 0.0
xi_shear = False
k_shear = 0.05


# Degree orders
deg = 1  # Order of elements
deg_3d = 2
deg_shear = 0  # Order of shear elements
n_ni = 40  # Number of gauss points for thickness integration
el_type = "tri"  # Type of elements

#dt = 2.5e-5  # time increment
dt = 2.0e-7
t_start = 0.0  # start time
#t_end = 4.0e-3  # end time
t_end = 1.0e-4

max_iter = 30
tol = 1.0e-4

mu = E / 2 / (1 + nu)  # Lame's constants
lmbda = E * nu / (1 + nu) / (1 - 2 * nu)

k_0 = 4.0/3.0*math.sqrt(R)/((1.0-nu**2)/E+(1.0-nu_2**2)/E_2)  # stiffness of contact

# Important points
p_center = fe.Point(0.5*l_x, 0.5*l_y)
p_corner = fe.Point(0.0, 0.0)
#p_center_3d = fe.Point(0.5*l_x, 0.5*l_y, l_z)
#p_corner_3d = fe.Point(0.0, 0.0, l_z)

# --------------------
# Geometry
# --------------------
# Mesh
elem_type = fe.CellType.Type.quadrilateral
if el_type == "rect":
    elem_type = fe.CellType.Type.quadrilateral
elif el_type == "tri":
    elem_type = fe.CellType.Type.triangle
# mesh = fe.RectangleMesh.create([fe.Point(0.0, 0.0), fe.Point(0.5*L, 0.5*B)], [nx, ny], elem_type)
# mesh = fe.BoxMesh.create([fe.Point(0.0, 0.0, 0.0), fe.Point(L, B, H)], [nx, ny, nz],fe.CellType.Type.hexahedron)
# mesh = fe.RectangleMesh(fe.Point(0.0, 0.0), fe.Point(0.5*L, 0.5*B), nx, ny, "left/right")
mesh_2d = fe.RectangleMesh(fe.Point(0.0, 0.0), fe.Point(0.5 * l_x, 0.5 * l_y), n_x, n_y, "right")
domain = Polygon([fe.Point(0.0, 0.0), fe.Point(0.5 * L, 0.0), fe.Point(0.5 * L, 0.5 * B), fe.Point(0.0, 0.5 * B)])
mesh_2d = generate_mesh(domain, 1.2 * nx)
#mesh_3d = fe.BoxMesh(fe.Point(0.0, 0.0, 0.0), fe.Point(0.5*l_x, 0.5*l_y, l_z), n_x, n_y, n_z)  # triangular mesh

# mesh plot
#fe.plot(mesh_2d)
#plt.show()

# Time parameters
# t_start = 0.0
# t_end = 0.03
# t_end = 4.0e-3
# t_end = 1.0e-5
# dt = 4.0e-7

snes_solver_parameters = {"nonlinear_solver": "snes",
                          "snes_solver": {"method": "vinewtonssls",
                                          "line_search": "basic",
                                          "linear_solver": "mumps",
                                          "maximum_iterations": 100,
                                          "relative_tolerance": 1e-3,
                                          "absolute_tolerance": 1e-4,
                                          "report": True,
                                          "error_on_nonconvergence": False}}

# --------------------
# Time solution
# --------------------
# First test
time1, i1, ii1, t1 = explicit_solver_plate("solutions/small/plate_01_001", mesh_2d, True, 20, "dist", el_type, "vol")
