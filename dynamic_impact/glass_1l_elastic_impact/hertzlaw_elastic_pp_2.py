# -------------------------
# hertzlaw_elastic_pp_2.py
# -------------------------

# -------------------------
# Description:
# - Postprocess file for elastic impact
# - Second file
#
# Last edit: 16.01. 2022
# -------------------------


import matplotlib.pyplot as plt
import numpy as np


# --------------------
# Functions
# --------------------
def plot_num_01(data_i, label_i, coef, ind_i):
    plt.plot(data_i[:, 0], coef*data_i[:, ind_i], label=label_i)


def plot_num_vel(data_i, label_i, coef, ind):
    dtt = data_i[1, 0] - data_i[0, 0]
    plt.plot(data_i[:, 0], coef*np.gradient(data_i[:, ind]) / dtt, label=label_i)


# --------------------
# Data loading
# --------------------
# Experiments
data_exp_1_2 = np.loadtxt("exp/obyc1_2cm.csv", delimiter=",")
data_exp_1_5 = np.loadtxt("exp/obyc1_5cm.csv", delimiter=",")
data_exp_1_10 = np.loadtxt("exp/obyc1_10cm.csv", delimiter=",")
data_exp_1_15 = np.loadtxt("exp/obyc1_15cm.csv", delimiter=",")
data_exp_1_20 = np.loadtxt("exp/obyc1_20cm.csv", delimiter=",")
data_exp_2_2 = np.loadtxt("exp/obyc2_2cm.csv", delimiter=",")
data_exp_2_20 = np.loadtxt("exp/obyc2_20cm.csv", delimiter=",")
data_exp_2_20_2 = np.loadtxt("exp/obyc2_202cm.csv", delimiter=",")
data_exp_2_20_3 = np.loadtxt("exp/obyc2_203cm.csv", delimiter=",")
data_exp_2_25 = np.loadtxt("exp/obyc2_25cm.csv", delimiter=",")
data_exp_2_30 = np.loadtxt("exp/obyc2_30cm.csv", delimiter=",")
data_exp_2_35 = np.loadtxt("exp/obyc2_35cm.csv", delimiter=",")
data_exp_1_3 = np.loadtxt("exp/05_3cm_export.csv", delimiter="\t", skiprows=1)

# Numerics - plate
data_num_1_10_imp = np.loadtxt("solutions/small/10cm_new_quad_full/impactor.txt")
data_num_1_10_imp2 = np.loadtxt("solutions/small/10cm_new_quad_notfull/impactor.txt")

m_imp = 48.7

# --------------------
# Plotting
# --------------------
# Time resolution sensitivity
plot_num_01(data_exp_1_10, "exp", 1, 2)
plot_num_01(data_num_1_10_imp, "dt=4e-5", -m_imp, 2)
plot_num_01(data_num_1_10_imp2, "dt=4e-5", -m_imp, 2)
plt.xlabel("Time [s]")
plt.ylabel("Force [N]")
plt.legend()
plt.show()