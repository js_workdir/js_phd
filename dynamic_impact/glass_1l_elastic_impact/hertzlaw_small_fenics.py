# -------------------------
# hertzlaw_small_fenics
# -------------------------

# -------------------------
# Description:
# - Impact of steel impactor into small glass plate
# - Contact by Hertz law
# - Implemented in FEniCS
# - Implicit Newmark integration scheme
#
# Last edit: 22.08. 2022
# -------------------------

import fenics as fe
import matplotlib.pyplot as plt
import math
import numpy as np
import time
from petsc4py import PETSc


# --------------------
# Functions and classes
# --------------------
def left(x, on_boundary):
    return on_boundary and x[0] < 0.0001


def eps(v):
    return fe.sym(fe.grad(v))


def sigma(v):
    return lmbda * fe.tr(eps(v)) * fe.Identity(3) + 2.0 * mu * eps(v)


def update_values():
    ddu_old.assign(ddu)
    ddu.assign(4/(dt*dt)*(u-u_bar))
    du.assign(du + 0.5*dt*(ddu + ddu_old))


def mc_bracket(x):
    return 0.5*(x+abs(x))


def calc_time_solution(file):
    impactor = []
    dimpactor = []
    ddimpactor = []
    tt = []
    plate = []
    dplate = []
    ddplate = []
    u_center = []
    u_corner = []

    u_imp = 0.0
    du_imp = v0
    ddu_imp = 0.0

    xdmffile_u = fe.XDMFFile(file + "/u.xdmf")
    xdmffile_u.parameters["flush_output"] = True

    t = t_start
    while t < t_end:

        u_bar.assign(u + dt * du + 0.25 * dt * dt * ddu)
        u_bar_imp = u_imp + dt * du_imp + 0.25 * dt * dt * ddu_imp

        u_vec = u.vector()
        u_bar_vec = u_bar.vector()
        #
        error = 10.0

        u_temp = fe.Function(V)
        u_temp.assign(u)

        iterr = 0.0
        while error > tol:
            EOM = K * u_vec + 4.0 / (dt * dt) * M * (u_vec - u_bar_vec)
            J = K + 4.0 / (dt * dt) * M
            dF_hertz = 1.5 * k_0 * mc_bracket(u_imp - u_temp((l_x / 2, l_y / 2, 0.0))[2]) ** 0.5
            F_hertz = k_0 * mc_bracket(u_imp - u_temp((l_x / 2, l_y / 2, 0.0))[2]) ** 1.5
            p1 = fe.PointSource(V.sub(2), fe.Point(l_x / 2, l_y / 2, 0.0), -F_hertz)
            p2 = fe.PointSource(V.sub(2), fe.Point(l_x / 2, l_y / 2, 0.0), dF_hertz)
            p2.apply(J)
            p1.apply(EOM)

            fe.solve(J, delta_u.vector(), EOM, solver_parameters={'linear_solver': 'cg',
                         'preconditioner': 'ilu'})
            u_new = u_vec - delta_u.vector()
            EOM = K * u_new + 4.0 / (dt * dt) * M * (u_new - u_bar_vec)
            p1.apply(EOM)
            # error1 = (u_new - u_vec).norm("l2")
            error1 = EOM.norm("l2")
            # error1 = K*u_new + (4.0/(dt*dt)*M*(u_new - u_bar_vec))
            u_temp.vector()[:] = u_new
            dF_hertz = 1.5 * k_0 * mc_bracket(u_imp - u_temp((l_x / 2, l_y / 2, 0.0))[2]) ** 0.5
            F_hertz = k_0 * mc_bracket(u_imp - u_temp((l_x / 2, l_y / 2, 0.0))[2]) ** 1.5

            J2 = 4.0 / (dt * dt) * m_imp + dF_hertz
            EOM2 = 4.0 / (dt * dt) * m_imp * (u_imp - u_bar_imp) + F_hertz

            u_imp_new = u_imp - EOM2 / J2
            # print(EOM2)
            # print(J2)
            error2 = (m_imp * (4.0 / (dt * dt) * (u_imp_new - u_bar_imp)) + F_hertz)
            # error2 = (m_imp * (4.0 / (dt * dt) *(0.25, 0.25, 0.0) (u_imp_new - u_bar_imp)) + F_hertz)

            error = max(abs(error1), abs(error2))
            print("error1 = ", error1, ", error2 = ", error2)
            print("t = ", t)

            if iterr > max_iter:
                print("max iterations reached")
                break

            iterr += 1

            u_vec = u_new
            u_imp = u_imp_new
            u_temp.vector()[:] = u_vec

        u.vector()[:] = u_vec

        ddu_old.assign(ddu)
        ddu.assign(4 / (dt * dt) * (u - u_bar))
        du.assign(du + 0.5 * dt * (ddu + ddu_old))

        ddu_old_imp = ddu_imp
        ddu_imp = 4 / (dt * dt) * (u_imp - u_bar_imp)
        du_imp = du_imp + 0.5 * dt * (ddu_imp + ddu_old_imp)

        xdmffile_u.write(u, t)

        tt.append(t)
        impactor.append(u_imp)
        dimpactor.append(du_imp)
        ddimpactor.append(ddu_imp)
        plate.append(u((l_x / 2, l_y / 2, 0.0))[2])
        dplate.append(du((l_x / 2, l_y / 2, 0.0))[2])
        ddplate.append(ddu((l_x / 2, l_y / 2, 0.0))[2])

        t = t + dt

    xdmffile_u.close()

    end = time.time()

    np.savetxt(file + "/output_data.txt", np.column_stack((tt, impactor, dimpactor, ddimpactor, plate, dplate, ddplate)),
               header="t\tu_imp\tdu_imp\tddu_imp\tu_plate\tdu_plate\tddu_plate")

    plt.plot(tt, ddimpactor)
    plt.ylabel("t")
    plt.xlabel("Acceleration of impactor")
    plt.figure()
    plt.plot(tt, impactor, label="impactor")
    plt.plot(tt, plate, label="plate")
    plt.legend()
    plt.xlabel("t")
    plt.ylabel("Displacement")
    plt.show()


def find_dof(p, d, VS):
    v_dofs = VS.tabulate_dof_coordinates()
    v0_dofs = VS.sub(d).dofmap().dofs()
    for i in range(0, len(v0_dofs)):
        v_x = v_dofs[v0_dofs[i], 0]
        v_y = v_dofs[v0_dofs[i], 1]
        if fe.near(v_x, p.x()) and fe.near(v_y, p.y()):
            return v0_dofs[i]
    return -1


# --------------------
# Parameters
# --------------------
n_x, n_y, n_z = 20, 20, 3  # Number of elements
l_x, l_y, l_z = 0.5, 0.5, 0.0146  # Dimensions of sample
E_1 = 70.0e9  # Young's modulus of glass
nu_1 = 0.23  # Poisson's ratio of glass
E_2 = 210.0e9  # Young's modulus of impactor
nu_2 = 0.3  # Poisson's ratio of impactor
R = 0.05  # Radius of impactor
rho = 2500.0  # Glass density
# F = 1.0
m_imp = 48.2  # Impactor mass
#m_imp = 52.36  # Impactor mass
h0 = 0.1  # initial height of impactor
v0 = math.sqrt(2*9.81*h0)
#k = 1.0e6
#c0 = 1.0e9

#dt = 2.5e-5  # time increment
dt = 2.5e-5
t_start = 0.0  # start time
#t_end = 4.0e-3  # end time
t_end = 8.0e-3

max_iter = 30
tol = 1.0e-4

mu = E_1 / 2 / (1 + nu_1)  # Lame's constants
lmbda = E_1 * nu_1 / (1 + nu_1) / (1 - 2 * nu_1)

k_0 = 4.0/3.0*math.sqrt(R)/((1.0-nu_1**2)/E_1+(1.0-nu_2**2)/E_2)  # stiffness of contact

# Important points
p_center = fe.Point(0.5*l_x, 0.5*l_y)
p_corner = fe.Point(0.0, 0.0)

# --------------------
# Geometry
# --------------------
mesh = fe.BoxMesh(fe.Point(0.0, 0.0, 0.0), fe.Point(l_x, l_y, l_z), n_x, n_y, n_z)  # triangular mesh

# mesh plot
fe.plot(mesh)
plt.show()

# --------------------
# Define spaces
# --------------------
V = fe.VectorFunctionSpace(mesh, "CG", 2)
u_tr = fe.TrialFunction(V)
u_test = fe.TestFunction(V)

# Mixedspace
S = fe.FunctionSpace(mesh, "CG", 2)
R = fe.FunctionSpace(mesh, "R", 0)
elementV = fe.FiniteElement("CG", mesh.ufl_cell(), 2)
elementR = fe.FiniteElement("R", mesh.ufl_cell(), 0)
mix_elem = fe.MixedElement([elementR, elementV])
space = fe.FunctionSpace(mesh, mix_elem)

f1 = fe.Function(S)
f2 = fe.Function(space)
print(f1.vector()[:])
print(f2.vector()[:])
f3 = fe.Function(R)
f3.vector()[0] = 1.0

fe.plot(f3)
plt.show()

# --------------------
# Boundary conditions
# --------------------
# bc = [fe.DirichletBC(V, fe.Constant((0.0, 0.0, 0.0)), left)]
bc = []

top = fe.AutoSubDomain(lambda x: fe.near(x[2], l_z))
edges = fe.AutoSubDomain(lambda x: fe.near(x[0], 0.0) or fe.near(x[0], l_x) or fe.near(x[1], 0.0) or fe.near(x[1], l_y))
boundaries = fe.MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
boundaries.set_all(0)
edges.mark(boundaries, 1)
ds = fe.ds(subdomain_data=boundaries)

mesh_file = fe.File("mesh.pvd")
mesh_file << boundaries

# --------------------
# Initialization
# --------------------
u = fe.Function(V, name="Displacement")
u_bar = fe.Function(V)
u_hat = fe.Function(V)
du = fe.Function(V)
ddu = fe.Function(V)
ddu_old = fe.Function(V)
delta_u = fe.Function(V)

u_imp = 0.0
du_imp = v0
ddu_imp = 0.0

print(V.dim())
RR = fe.FunctionSpace(mesh, "R", 0)
print(RR.dim())

# --------------------
# Variational forms
# --------------------
A = fe.inner(sigma(u_tr), eps(u_test))*fe.dx + 4*rho/(dt*dt)*fe.dot(u_tr - u_bar, u_test)*fe.dx
K_form = fe.inner(sigma(u_tr), eps(u_test))*fe.dx
#K_form += k*fe.dot(u_tr, u_test)*ds(1)
M_form = rho*fe.dot(u_tr, u_test)*fe.dx
#C_form = c0*fe.dot(u_tr, u_test)*ds(1)
K = fe.PETScMatrix()
K = fe.assemble(K_form, tensor=K)
M = fe.assemble(M_form)
#C = fe.assemble(C_form)

# --------------------
# Time solution
# --------------------
#calc_time_solution_damp("ke10_middle_damp")
calc_time_solution("solutions/small/solutions/small/10cm_3d_tri_def_004")
