# -------------------------
# hertzlaw_2D_first_test.py
# -------------------------

# -------------------------
# description:
# - Impact of steel impactor into small glass plate
# - Damage model is implemented
# - Contact by Hertz law
# - Implemented in FEniCS
# - Explicit integration scheme
#
# last edit: 08.11. 2022
# -------------------------

import fenics as fe
import matplotlib.pyplot as plt
import time
import numpy as np
import math
import lumping_scheme_quad
import os

from mshr import Polygon, generate_mesh


def eps(v):
    return fe.sym(fe.grad(v))


# Elastic stress tensor
def sigma(u_i):
    return lmbda * (fe.tr(eps(u_i))) * fe.Identity(2) + 2.0 * mu * (eps(u_i))


def sigma_sd(u_i, d_i):
    return ((1 - d_i) ** 2 + k_res) * sigma_p_sd(u_i) + sigma_n_sd(u_i)


# Positive stress tensor - spectral decomposition
def sigma_p_sd(u_i):
    return lmbda * (fe.tr(eps_p(u_i))) * fe.Identity(2) + 2.0 * mu * (eps_p(u_i))


# Negative stress tensor - spectral decomposition
def sigma_n_sd(u_i):
    return lmbda * (fe.tr(eps_n(u_i))) * fe.Identity(2) + 2.0 * mu * (eps_n(u_i))


# Positive stress tensor - volumetric-deviatoric
def sigma_p_vd(u_i):
    kn = lmbda + mu
    return kn * mc_bracket(fe.tr(eps(u_i))) * fe.Identity(2) + 2 * mu * fe.dev(eps(u_i))


# Negative stress tensor - volumetric-deviatoric
def sigma_n_vd(u_i):
    kn = lmbda + mu
    return -kn * mc_bracket(-fe.tr(eps(u_i))) * fe.Identity(2)


def sigma_vd(u_i, d_i):
    # return sigma_p_vd(u_i, lmbda, mu) + sigma_n_vd(u_i, lmbda, mu)
    return ((1 - d_i) ** 2 + k_res) * sigma_p_vd(u_i) + sigma_n_vd(u_i)


# positive strain tensor
def eps_p(v):
    v00, v01, v10, v11 = eig_v(eps(v))
    v00 = fe.conditional(fe.gt(v00, 0.0), v00, 0.0)
    v11 = fe.conditional(fe.gt(v11, 0.0), v11, 0.0)
    w00, w01, w10, w11 = eig_w(eps(v))
    wp = ([w00, w01], [w10, w11])
    wp = fe.as_tensor(wp)
    vp = ([v00, v01], [v10, v11])
    vp = fe.as_tensor(vp)
    return wp * vp * fe.inv(wp)


# negative strain tensor
def eps_n(v):
    v00, v01, v10, v11 = eig_v(eps(v))
    v00 = fe.conditional(fe.lt(v00, 0.0), v00, 0.0)
    v11 = fe.conditional(fe.lt(v11, 0.0), v11, 0.0)
    w00, w01, w10, w11 = eig_w(eps(v))
    wn = ([w00, w01], [w10, w11])
    wn = fe.as_tensor(wn)
    vn = ([v00, v01], [v10, v11])
    vn = fe.as_tensor(vn)
    return wn * vn * fe.inv(wn)


# eigenvalues for 2x2 matrix
def eig_v(a):
    v00 = a[0, 0] / 2 + a[1, 1] / 2 - fe.sqrt(
        a[0, 0] ** 2 - 2 * a[0, 0] * a[1, 1] + 4 * a[0, 1] * a[1, 0] + a[1, 1] ** 2) / 2
    v01 = 0.0
    v10 = 0.0
    v11 = a[0, 0] / 2 + a[1, 1] / 2 + fe.sqrt(
        a[0, 0] ** 2 - 2 * a[0, 0] * a[1, 1] + 4 * a[0, 1] * a[1, 0] + a[1, 1] ** 2) / 2
    return v00, v01, v10, v11


# eigenvectors for 2x2 matrix
def eig_w(a):
    w00 = -a[0, 1] / (a[0, 0] / 2 - a[1, 1] / 2 + fe.sqrt(
        a[0, 0] ** 2 - 2 * a[0, 0] * a[1, 1] + 4 * a[0, 1] * a[1, 0] + a[1, 1] ** 2) / 2)
    w01 = -a[0, 1] / (a[0, 0] / 2 - a[1, 1] / 2 - fe.sqrt(
        a[0, 0] ** 2 - 2 * a[0, 0] * a[1, 1] + 4 * a[0, 1] * a[1, 0] + a[1, 1] ** 2) / 2)
    w10 = 1.0
    w11 = 1.0
    return w00, w01, w10, w11


def get_pos_eig(fce):
    mesh = fce.function_space().mesh()
    vert_vals = fce.vector().get_local()
    temp = vert_vals.reshape([mesh.num_vertices(), 2, 2])
    [eigL, eigR] = np.linalg.eig(temp)
    eigL1 = eigL[:, 0]
    eigL2 = eigL[:, 1]
    psiL = 0.5 * lmbda * np.power((eigL1 + eigL2).clip(0), 2) + mu * (np.power(eigL1.clip(0), 2)
                                                                      + np.power(eigL2.clip(0), 2))
    eig = fe.Function(fe.FunctionSpace(mesh, 'CG', 1))
    eig.vector().set_local(psiL.flatten())
    return eig


def get_pos_strain(fce):
    mesh = fce.function_space().mesh()
    vert_vals = fce.vector().get_local()
    temp = vert_vals.reshape([mesh.num_vertices(), 2, 2])
    [eigL, eigR] = np.linalg.eig(temp)
    eigL1 = eigL[:, 0]
    eigL2 = eigL[:, 1]
    eigR1 = eigR[:, 0]
    eigR2 = eigR[:, 1]

    eigL1.clip(0)

    psiL = 0.5 * lmbda * np.power((eigL1 + eigL2).clip(0), 2) + mu * (np.power(eigL1.clip(0), 2)
                                                                      + np.power(eigL2.clip(0), 2))
    eig = fe.Function(fe.FunctionSpace(mesh, 'CG', 1))
    eig.vector().set_local(psiL.flatten())
    return eig


def mc_bracket(x):
    return 0.5 * (x + abs(x))


# Maximum of two function with UFL
def max_fce(a, b):
    return 0.5 * (a + b + abs(a - b))


def right_edge(x, on_boundary):
    return on_boundary and fe.near(x[0], 0.5 * L)


def top_edge(x, on_boundary):
    return on_boundary and fe.near(x[1], 0.5 * B)


# Projection on each element
def local_project(fce, space):
    lp_trial, lp_test = fe.TrialFunction(space), fe.TestFunction(space)
    lp_a = fe.inner(lp_trial, lp_test) * fe.dx
    lp_L = fe.inner(fce, lp_test) * fe.dx
    local_solver = fe.LocalSolver(lp_a, lp_L)
    local_solver.factorize()
    lp_f = fe.Function(space)
    local_solver.solve_local_rhs(lp_f)
    return lp_f


# Positive energy - spectral decomposition
def psi_sd_plus(u_i):
    eps_sym = eps(u_i)

    eps1 = (1. / 2.) * fe.tr(eps_sym) + fe.sqrt((1. / 4.) * (fe.tr(eps_sym) ** 2) - fe.det(eps_sym))
    eps2 = (1. / 2.) * fe.tr(eps_sym) - fe.sqrt((1. / 4.) * (fe.tr(eps_sym) ** 2) - fe.det(eps_sym))

    tr_eps_plus = mc_bracket(fe.tr(eps_sym))
    sum_eps_plus_squared = mc_bracket(eps1) ** 2 + mc_bracket(eps2) ** 2

    return 0.5 * lmbda * tr_eps_plus ** 2 + mu * sum_eps_plus_squared


# Negative energy - spectral decomposition
def psi_sd_minus(u_i):
    eps_sym = eps(u_i)

    eps1 = (1. / 2.) * fe.tr(eps_sym) + fe.sqrt((1. / 4.) * (fe.tr(eps_sym) ** 2) - fe.det(eps_sym))
    eps2 = (1. / 2.) * fe.tr(eps_sym) - fe.sqrt((1. / 4.) * (fe.tr(eps_sym) ** 2) - fe.det(eps_sym))

    tr_eps_minus = -mc_bracket(-fe.tr(eps_sym))
    sum_eps_minus_squared = mc_bracket(-eps1) ** 2 + mc_bracket(-eps2) ** 2

    return 0.5 * lmbda * tr_eps_minus ** 2 + mu * sum_eps_minus_squared


def get_K_form(V):
    dx_shear = fe.dx(metadata={"quadrature_degree": 0})
    k = 5.0 / 6.0

    S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

    w_tr, u_tr, phi_tr, imp_tr = fe.TrialFunctions(V)
    w_test, u_test, phi_test, imp_test = fe.TestFunctions(V)
    # w_tr, u_tr, phi_tr = map_to_layers(lg_trial, H)
    # w_test, u_test, phi_test = map_to_layers(lg_test, H)

    K_form = 0.0

    D1 = (E * H ** 3) / (12.0 * (1.0 - nu ** 2))
    D2 = (E * k * H) / (2.0 * (1.0 + nu))
    D3 = (E * H) / (1.0 - nu ** 2)
    kappa_tr, kappa_test = eps(S * phi_tr), eps(S * phi_test)
    du_tr, du_test = eps(u_tr), eps(u_test)
    K_form += fe.inner(D1 * (nu * fe.div(S * phi_tr) * fe.Identity(2) + (1.0 - nu) * kappa_tr), kappa_test) * fe.dx
    K_form += fe.inner(D3 * (nu * fe.div(u_tr) * fe.Identity(2) + (1.0 - nu) * du_tr), du_test) * fe.dx
    K_form += D2 * fe.inner(fe.grad(w_tr) + S * phi_tr, fe.grad(w_test) + S * phi_test) * dx_shear

    return K_form


def get_K_form_numint(V, d, dec_type):
    dx_shear = fe.dx(metadata={"quadrature_degree": 0})
    k = 5.0 / 6.0

    S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

    w_tr, u_tr, phi_tr, imp_tr = fe.TrialFunctions(V)
    w_test, u_test, phi_test, imp_test = fe.TestFunctions(V)
    # w_tr, u_tr, phi_tr = map_to_layers(lg_trial, H)
    # w_test, u_test, phi_test = map_to_layers(lg_test, H)

    K_form = 0.0

    num_int = np.linspace(-0.5 * H, 0.5 * H, n_ni)
    dh = abs(num_int[1] - num_int[0])

    for i in range(len(num_int) - 1):
        gauss = 0.5 * (num_int[i] + num_int[i + 1])
        u_z = u_tr + S * phi_tr * gauss
        u_z_test = u_test + S * phi_test * gauss
        # K_form += (1-d)**2*fe.inner(sigma(u_z), eps(u_z_test))*dh*fe.dx
        if dec_type == "hyb_sd":
            K_form += (1 - d) ** 2 * fe.inner(sigma(u_z), eps(u_z_test)) * dh * fe.dx
        elif dec_type == "vol":
            K_form += fe.inner(sigma_vd(u_z, d), eps(u_z_test)) * dh * fe.dx
        elif dec_type == "sd":
            K_form += fe.inner(sigma_sd(u_z, d), eps(u_z_test)) * dh * fe.dx

    # K_form *= (1-d)**2
    # u_form += D2 * fe.inner(fe.grad(w_) + S * theta_, fe.grad(w_test) + S * theta_test) * dx_shear
    # u_form -= fe.Constant(0.0) * w_test * fe.dx

    # D1 = (E*H**3)/(12.0*(1.0 - nu**2))
    D2 = (E * k * H) / (2.0 * (1.0 + nu))
    # D3 = (E*H)/(1.0 - nu**2)
    kappa_tr, kappa_test = eps(S * phi_tr), eps(S * phi_test)
    du_tr, du_test = eps(u_tr), eps(u_test)
    # K_form += fe.inner(D1*(nu*fe.div(S*phi_tr)*fe.Identity(2) + (1.0 - nu)*kappa_tr),kappa_test)*fe.dx
    # K_form += fe.inner(D3*(nu*fe.div(u_tr)*fe.Identity(2) + (1.0 - nu)*du_tr), du_test)*fe.dx
    if xi_shear:
        K_form += D2 * fe.inner(fe.grad(w_tr) + S * phi_tr, fe.grad(w_test) + S * phi_test) * dx_shear
    else:
        print("hej")
        K_form += ((1 - d) ** 2 + k_shear) * D2 * fe.inner(fe.grad(w_tr) + S * phi_tr,
                                                           fe.grad(w_test) + S * phi_test) * dx_shear

    return K_form


def get_K_form_vector(V, u, d, dec_type):
    dx_shear = fe.dx(metadata={"quadrature_degree": 0})
    k = 5.0 / 6.0

    S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

    w_tr, u_tr, phi_tr, imp_tr = fe.split(u)
    # w_tr, u_tr, phi_tr, imp_tr = fe.TrialFunctions(V)
    w_test, u_test, phi_test, imp_test = fe.TestFunctions(V)
    # w_tr, u_tr, phi_tr = map_to_layers(lg_trial, H)
    # w_test, u_test, phi_test = map_to_layers(lg_test, H)

    K_form = 0.0

    num_int = np.linspace(-0.5 * H, 0.5 * H, n_ni)
    dh = abs(num_int[1] - num_int[0])

    for i in range(len(num_int) - 1):
        gauss = 0.5 * (num_int[i] + num_int[i + 1])
        u_z = u_tr + S * phi_tr * gauss
        u_z_test = u_test + S * phi_test * gauss
        # K_form += (1-d)**2*fe.inner(sigma(u_z), eps(u_z_test))*dh*fe.dx
        if dec_type == "hyb_sd":
            K_form += (1 - d) ** 2 * fe.inner(sigma(u_z), eps(u_z_test)) * dh * fe.dx
        elif dec_type == "vol":
            print("hej")
            K_form += fe.inner(sigma_vd(u_z, d), eps(u_z_test)) * dh * fe.dx
        elif dec_type == "sd":
            K_form += fe.inner(sigma_sd(u_z, d), eps(u_z_test)) * dh * fe.dx
        else:
            raise Exception("Only hyb_sd/vol/sd decomposition implemented!")

    # K_form *= (1-d)**2
    # u_form += D2 * fe.inner(fe.grad(w_) + S * theta_, fe.grad(w_test) + S * theta_test) * dx_shear
    # u_form -= fe.Constant(0.0) * w_test * fe.dx

    # D1 = (E*H**3)/(12.0*(1.0 - nu**2))
    D2 = (E * k * H) / (2.0 * (1.0 + nu))
    # D3 = (E*H)/(1.0 - nu**2)
    # K_form += fe.inner(D1*(nu*fe.div(S*phi_tr)*fe.Identity(2) + (1.0 - nu)*kappa_tr),kappa_test)*fe.dx
    # K_form += fe.inner(D3*(nu*fe.div(u_tr)*fe.Identity(2) + (1.0 - nu)*du_tr), du_test)*fe.dx
    # K_form += ((1-d)**2 + k_res)*D2*fe.inner(fe.grad(w_tr) + S*phi_tr, fe.grad(w_test) + S*phi_test)*dx_shear
    # K_form += D2*fe.inner(fe.grad(w_tr) + S*phi_tr, fe.grad(w_test) + S*phi_test)*dx_shear

    if xi_shear:
        K_form += D2 * fe.inner(fe.grad(w_tr) + S * phi_tr, fe.grad(w_test) + S * phi_test) * dx_shear
    else:
        print("hej2")
        K_form += ((1 - d) ** 2 + k_shear) * D2 * fe.inner(fe.grad(w_tr) + S * phi_tr,
                                                           fe.grad(w_test) + S * phi_test) * dx_shear

    return K_form


def get_psi_K_form(u, d):
    dx_shear = fe.dx(metadata={"quadrature_degree": 0})
    k = 5.0 / 6.0

    S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

    # w_tr, u_tr, phi_tr, imp_tr = fe.TrialFunctions(V)
    w_tr, u_tr, phi_tr, imp_tr = fe.split(u)
    # u_test, w_test, phi_test = fe.TestFunctions(V)
    # w_test, u_test, phi_test, imp_test = fe.TestFunctions(V)
    # w_tr, u_tr, phi_tr = map_to_layers(lg_trial, H)
    # w_test, u_test, phi_test = map_to_layers(lg_test, H)

    K_form = 0.0

    num_int = np.linspace(-0.5 * H, 0.5 * H, n_ni)
    dh = abs(num_int[1] - num_int[0])

    for i in range(len(num_int) - 1):
        gauss = 0.5 * (num_int[i] + num_int[i + 1])
        u_z = u_tr + S * phi_tr * gauss
        # u_z_test = u_test + S*phi_test*gauss
        # K_form += fe.inner(sigma(u_z), eps(u_z_test))*dh*fe.dx
        K_form += ((1 - d) ** 2 * psi_sd_plus(u_z) + psi_sd_minus(u_z)) * dh * fe.dx

    # u_form += D2 * fe.inner(fe.grad(w_) + S * theta_, fe.grad(w_test) + S * theta_test) * dx_shear
    # u_form -= fe.Constant(0.0) * w_test * fe.dx

    # D1 = (E*H**3)/(12.0*(1.0 - nu**2))
    D2 = (E * k * H) / (2.0 * (1.0 + nu))
    # D3 = (E*H)/(1.0 - nu**2)
    # kappa_tr, kappa_test = eps(S*phi_tr), eps(S*phi_test)
    # du_tr, du_test = eps(u_tr), eps(u_test)
    # K_form += fe.inner(D1*(nu*fe.div(S*phi_tr)*fe.Identity(2) + (1.0 - nu)*kappa_tr),kappa_test)*fe.dx
    # K_form += fe.inner(D3*(nu*fe.div(u_tr)*fe.Identity(2) + (1.0 - nu)*du_tr), du_test)*fe.dx
    # K_form += D2*fe.inner(fe.grad(w_tr) + S*phi_tr, fe.grad(w_test) + S*phi_test)*dx_shear
    K_form += 0.5 * D2 * fe.inner(fe.grad(w_tr) + S * phi_tr, fe.grad(w_tr) + S * phi_tr) * dx_shear

    return K_form


def get_M_form(V, lump):
    if lump:
        if el_type == "rect":
            dx_m = fe.dx(scheme="lumped", degree=2)
        elif el_type == "tri" or el_type == "tri_ref":
            dx_m = fe.dx(scheme="vertex", metadata={"degree": 1, "representation": "quadrature"})
    else:
        dx_m = fe.dx

    w_tr, u_tr, phi_tr, imp_tr = fe.TrialFunctions(V)
    w_test, u_test, phi_test, imp_test = fe.TestFunctions(V)
    S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

    M_form = 0.0

    M_form += rho * H ** 3 / 12.0 * fe.dot(S * phi_tr, S * phi_test) * dx_m
    M_form += rho * H * fe.dot(w_tr, w_test) * dx_m
    M_form += rho * H * fe.dot(u_tr, u_test) * dx_m

    M_form_2 = rho_imp * fe.dot(imp_tr, imp_test) * fe.dx

    return M_form, M_form_2


def get_positive_energy(u):
    w_tr, u_tr, phi_tr, imp_tr = fe.split(u)
    S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

    # Positive energy
    u_z_l = u_tr + S * phi_tr * 0.5 * H
    u_z_u = u_tr - S * phi_tr * 0.5 * H
    return max_fce(psi_sd_plus(u_z_l) * H, psi_sd_plus(u_z_u) * H)


# Return formulation for damage
def get_d_form(W, u, d, type_d, hist_var):
    d_test = fe.TestFunction(W)
    d_tr = fe.TrialFunction(W)
    # w_tr, u_tr, phi_tr, imp_tr = fe.split(u)
    # S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

    # Positive energy
    # u_z_l = u_tr + S*phi_tr*0.5*H
    # u_z_u = u_tr - S*phi_tr*0.5*H
    # en = max_fce(psi_sd_plus(u_z_l)*H, psi_sd_plus(u_z_u)*H)

    en = get_positive_energy(u)

    if type_d == "bourdin":
        # max_en = max_fce(en, hist_var)
        # hist_var_fce = fe.project(max_en, W0)
        # hist_var
        # en = max_fce(en, hist)
        en = hist_var

    d_form = 0.0
    if type_d == "pham":
        d_form += -2 * en * fe.inner(1.0 - d, d_test) * fe.dx
        d_form += H * 3.0 / 8.0 * Gc_glass * (
                    1.0 / lc * d_test + 2 * lc * fe.inner(fe.grad(d), fe.grad(d_test))) * fe.dx
    elif type_d == "bourdin":
        d_form += -2 * en * fe.inner(1.0 - d_tr, d_test) * fe.dx
        d_form += H * Gc_glass * (
                    1.0 / lc * fe.inner(d_tr, d_test) + lc * fe.inner(fe.grad(d_tr), fe.grad(d_test))) * fe.dx

    return d_form


def create_log_file(folder, p_type):
    directory = os.path.dirname(folder + "/")
    if not os.path.exists(directory):
        os.makedirs(directory)
    f = open(folder + "/log.txt", "w")
    f.write("Number of elements: " + str((nx, ny)) + "\n")
    f.write("Material parameters: E=" + str(E) + ", nu=" + str(nu) + ", rho=" + str(rho) + ", ft=" + str(ft_glass) +
            ", lc=" + str(lc) + ", Gc=" + str(Gc_glass) + "\n")
    f.write("Impactor: m=" + str(m_imp) + ", E=" + str(E_imp) + ", nu=" + str(nu_imp) + "\n")
    f.write("Impact height: h0=" + str(h0) + "\n")
    f.write("Elements: " + str(mesh.ufl_cell()) + "-" + str(deg) + "\n")
    f.write("Point-load type: " + p_type + "\n")
    f.write("Model parameters: n_ni=" + str(n_ni) + ", k_res=" + str(k_res) + ", xi_shear=" + str(xi_shear) +
            ", k_shear=" + str(k_shear) + "\n")


def explicit_solver(folder, saving, save_rate, point_type, el_type_i, dec_type_i, k_shear):
    def prepare_solver_d(hist_i):
        lower = d
        # lower = hist_i
        # hist
        # threshold = fe.interpolate(fe.Constant(0.9), W)
        # lower = fe.conditional(fe.lt(d, threshold), 0.0, d)
        upper = fe.interpolate(fe.Constant(1.0), W)

        # Solution of damage formulation
        H = fe.derivative(d_form, d, fe.TrialFunction(W))

        problem = fe.NonlinearVariationalProblem(d_form, d, [], H)
        problem.set_bounds(lower, upper)

        solver = fe.NonlinearVariationalSolver(problem)
        solver.parameters.update(snes_solver_parameters)
        return solver

    # Damage snes variational inequalities solver based on Newton's method
    def solve_damage(d_type, solver):
        if d_type == "pham":
            lower = d
            upper = fe.interpolate(fe.Constant(1.0), W)

            # Solution of damage formulation
            H = fe.derivative(d_form, d, fe.TrialFunction(W))

            problem = fe.NonlinearVariationalProblem(d_form, d, [], H)
            problem.set_bounds(lower, upper)

            solver = fe.NonlinearVariationalSolver(problem)
            solver.parameters.update(snes_solver_parameters)
            solver.solve()
        elif d_type == "pham2":
            solver.solve()
            # lowerr = fe.conditional(fe.lt(d, threshold), 0.0, d)
            # hist.assign(local_project(lowerr, W0))
        elif d_type == "bourdin":
            fe.solve(fe.lhs(d_form) == fe.rhs(d_form), d, [])
            hist.assign(local_project(max_fce(hist, get_positive_energy(X)), W0))

    # Define spaces
    p1_element = fe.FiniteElement("P", mesh.ufl_cell(), deg)
    v1_element = fe.VectorElement("P", mesh.ufl_cell(), deg)
    r1_element = fe.FiniteElement("R", mesh.ufl_cell(), 0)
    element = fe.MixedElement([p1_element, v1_element, v1_element, r1_element])
    V = fe.FunctionSpace(mesh, element)
    V_sca = fe.FunctionSpace(mesh, p1_element)
    V_vec = fe.FunctionSpace(mesh, v1_element)
    V_rea = fe.FunctionSpace(mesh, r1_element)
    W = fe.FunctionSpace(mesh, "CG", 1)
    W0 = fe.FunctionSpace(mesh, "CG", 1)
    print("Number of unknowns: " + str(V.dim()))
    impactor = []
    tt = []
    impactor_acc = []
    tt = []
    u_center = []
    u_corner = []

    # k_shear
    k_shear_test = True
    if k_shear_test == True:
        k_shear = fe.Expression(" (x[0]-0.5*l_x)*(x[0]-0.5*l_x)+(x[1]-0.5*l_y)*(x[1]-0.5*l_y) < rs*rs ? k_shear : 0.0", l_x=0.5, l_y=0.5, rs=0.05*0.5, k_shear=k_shear, degree=1)

    # Localization vector
    # a_loc = fe.Function(V)
    # a_loc_vec_1 = a_loc.vector()
    # p1 = fe.PointSource(V.sub(0), pp, -1.0)
    # p2 = fe.PointSource(V.sub(3), pp, 1.0)
    # p1.apply(a_loc_vec_1)
    # p2.apply(a_loc_vec_1)

    # TODO: ??
    # tr = fe.TrialFunction(V)
    # theta_tr, u_tr, w_tr = fe.TrialFunctions(V)
    # theta_test, u_test, w_test = fe.TestFunctions(V)
    # S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])
    # dx_shear = fe.dx(metadata={"quadrature_degree": 0})

    # Displacement functions
    X = fe.Function(V)  # Displacement in time t_i
    X_old = fe.Function(V)  # Displacement in time t_{i-1}
    X_old2 = fe.Function(V)  # Displacement in time t_{i-2}
    X_d_old = fe.Function(V)
    d = fe.Function(W)
    hist = fe.Function(W0)

    # Initial velocity
    fa = fe.FunctionAssigner(V, [V_sca, V_vec, V_vec, V_rea])
    s_init = fe.interpolate(fe.Constant(0.0), V_sca)
    v_init = fe.interpolate(fe.Constant((0.0, 0.0)), V_vec)
    r_init = fe.interpolate(fe.Constant(-dt * v0), V_rea)
    fa.assign(X_old2, [s_init, v_init, v_init, r_init])
    threshold = fe.interpolate(fe.Constant(0.5), W)

    # Indices of points
    ind_center = find_dof(p_center, 0, V)
    ind_corner = find_dof(p_corner, 0, V)

    # Point distribution
    f_point = fe.Expression("abs(x[0] - 0.5*L) < tol & abs(x[1] - 0.5*B) < tol ? 1.0 : 0.0", tol=0.001, L=L, B=B,
                            degree=1)
    f_fce_vect = fe.interpolate(f_point, V_sca)
    f_fce_real = fe.interpolate(fe.Constant(1 / (0.25 * L * B)), V_rea)

    # fe.plot(f_fce_vect)
    file_hit = fe.File("hit_fce.pvd")
    file_hit << f_fce_vect
    # plt.show()

    w_test, u_test, phi_test, imp_test = fe.TestFunctions(V)
    f_dest_form = fe.dot(f_fce_vect, w_test) * fe.dx
    f_dest_form_2 = fe.dot(f_fce_real, imp_test) * fe.dx
    a_loc_vec_2 = fe.assemble(f_dest_form)
    a_loc_vec_2[:] = -a_loc_vec_2[:] / sum(a_loc_vec_2[:])
    a_loc_real = fe.assemble(f_dest_form_2)
    index_real = np.nonzero(a_loc_real[:])[0]
    index_hit = np.argmin(a_loc_vec_2[:])
    # print(aaa)
    # print(a_loc_vec_2[aaa])
    # p2.apply(a_loc_vec_2)

    if point_type == "point":
        a_loc_vec = a_loc_vec_1
    elif point_type == "dist":
        a_loc_vec = a_loc_vec_2 + a_loc_real
    else:
        raise Exception("Only point/dist point type implemented!")

    # Dirichlet boundary conditions
    # w1, u1, phi1, r
    bc1 = fe.DirichletBC(V.sub(2).sub(1), fe.Constant(0.0), right_edge)
    bc2 = fe.DirichletBC(V.sub(2).sub(0), fe.Constant(0.0), top_edge)
    bc3 = fe.DirichletBC(V.sub(1).sub(0), fe.Constant(0.0), right_edge)
    bc4 = fe.DirichletBC(V.sub(1).sub(1), fe.Constant(0.0), top_edge)
    bc = [bc1, bc2, bc3, bc4]

    # File with solution
    file = fe.XDMFFile(folder + "/displ_w.xdmf")
    file.parameters["flush_output"] = True
    file_u = fe.XDMFFile(folder + "/displ_u.xdmf")
    file_u.parameters["flush_output"] = True
    file_d = fe.XDMFFile(folder + "/damage_d.xdmf")
    file_d.parameters["flush_output"] = True

    # Forms for matrices K and M
    # K_form = get_K_form_numint(V, d, dec_type_i)
    K_form_vector = get_K_form_vector(V, X, d, dec_type_i)
    # psi_K_form = get_psi_K_form(X, d)
    # K_form_ufl_der = fe.derivative(psi_K_form, X, fe.TestFunction(V))
    M_form_1, M_form_2 = get_M_form(V, True)

    # # Forms for matrices K and M
    # K_form = 0.0
    # D1 = (E*H**3)/(12.0*(1.0 - nu**2))
    # D3 = (E*H)/(1.0 - nu**2)
    # kappa_tr, kappa_test = eps(S*theta_tr), eps(S*theta_test)
    # du_tr, du_test = eps(u_tr), eps(u_test)
    # K_form += fe.inner(D1*(nu*fe.div(S*theta_tr)*fe.Identity(2) + (1.0 - nu)*kappa_tr), kappa_test)*fe.dx
    # K_form += fe.inner(D3*(nu*fe.div(u_tr)*fe.Identity(2) + (1.0 - nu)*du_tr), du_test)*fe.dx
    # D2 = (E*k*H)/(2.0*(1.0 + nu))
    # #D2 = (mu*H*5.0)/6.0
    # K_form += D2*fe.inner(fe.grad(w_tr) + S*theta_tr, fe.grad(w_test) + S*theta_test)*dx_shear
    #
    # M_form_core = rho*H**3/12.0*fe.dot(S*theta_tr, S*theta_test)
    # M_form_core += rho*H*fe.dot(w_tr, w_test)
    # M_form_core += rho*H*fe.dot(u_tr, u_test)
    # M_form = M_form_core*fe.dx
    # M_form_quad = M_form_core*fe.dx(scheme="vertex", metadata={"degree": 1, "representation": "quadrature"})
    #
    # l_aux = fe.Constant(1.0)*u_test[0]*fe.dx

    # Assembling of consistent matrices
    # K_matrix = fe.assemble(K_form)
    K_vector = fe.assemble(K_form_vector)
    # K_matrix = fe.assemble(K_form_ufl_der)
    # [bci.apply(K_matrix) for bci in bc]
    [bci.apply(K_vector) for bci in bc]
    M_matrix = fe.assemble(M_form_1) + fe.assemble(M_form_2)
    [bci.apply(M_matrix) for bci in bc]

    M_fce = fe.Function(V)
    M_vect = M_fce.vector()
    M_matrix.get_diagonal(M_vect)

    d_form = get_d_form(W, X, d, "pham", hist)
    solver_d = prepare_solver_d(hist)

    # Main time loop
    saveindex = save_rate
    t = t_start
    begin_t = time.time()
    create_log_file(folder, point_type)
    while t < t_end:
        print("Time instant: " + str(t))

        # K_matrix = fe.assemble(K_form)
        # [bci.apply(K_matrix) for bci in bc]

        # Hertz nonlinear force
        # F_hertz = 0.25*k_0*mc_bracket(X_old.sub(3)(pp) - X_old.sub(0)(pp))**1.5
        F_hertz = np.asscalar(0.25 * k_0 * mc_bracket(X_old.vector()[index_real] - X_old.vector()[index_hit]) ** 1.5)
        # F_hertz = 0

        # Update of RHS
        w_vector = -dt ** 2 * K_vector - M_matrix * (
                    X_old2.vector() - 2.0 * X_old.vector()) - dt ** 2 * F_hertz * a_loc_vec
        # w_vector = -dt**2*K_matrix*X_old.vector() - M_matrix*(X_old2.vector() - 2.0*X_old.vector()) -dt**2*F_hertz*a_loc_vec
        [bci.apply(w_vector) for bci in bc]

        iMw = fe.Function(V)
        iMw.vector().set_local(w_vector.get_local() / M_vect.get_local())
        X.assign(iMw)

        solve_damage("pham2", solver_d)

        K_vector = fe.assemble(K_form_vector)
        [bci.apply(K_vector) for bci in bc]

        if saving:
            if saveindex >= save_rate:
                w, u, phi, u_imp = X.split(deepcopy=True)
                file.write(w, t)
                file_u.write(u, t)

                # solve_damage("pham2", solver_d)

                file_d.write(d, t)

                # K_matrix = fe.assemble(K_form)
                # [bci.apply(K_matrix) for bci in bc]
                # K_vector = fe.assemble(K_form_vector)
                # [bci.apply(K_vector) for bci in bc]

                # impactor.append(u_imp(pp))
                impactor.append(X.vector()[index_real])
                tt.append(t)

                temp_acc = X.vector()[index_real] - 2*X_old.vector()[index_real] + X_old2.vector()[index_real]
                temp_acc /= dt**2
                impactor_acc.append(temp_acc)
                u_center.append(X.vector()[ind_center])
                u_corner.append(X.vector()[ind_corner])

                saveindex = 0
            else:
                saveindex += 1

        X_old2.assign(X_old)
        X_old.assign(X)

        t += dt

    end_t = time.time()
    file.close()

    np.savetxt(folder + "/impactor.txt", np.column_stack([tt, impactor, impactor_acc, u_center, u_corner]))

    return end_t - begin_t, impactor, tt


def find_dof(p, d, VS):
    v_dofs = VS.tabulate_dof_coordinates()
    v0_dofs = VS.sub(d).dofmap().dofs()
    for i in range(0, len(v0_dofs)):
        v_x = v_dofs[v0_dofs[i], 0]
        v_y = v_dofs[v0_dofs[i], 1]
        if fe.near(v_x, p.x()) and fe.near(v_y, p.y()):
            return v0_dofs[i]
    return -1


# Geometry paramaters
L, B, H = 0.5, 0.5, 0.0146  # Width, Height, Thickness

# Number of elements
nx, ny = 50, 50

# Degree orders
deg = 1  # Order of elements
deg_shear = 0  # Order of shear elements
n_ni = 40  # Number of gauss points for thickness integration
el_type = "tri"  # Type of elements

# Mesh
elem_type = fe.CellType.Type.quadrilateral
if el_type == "rect":
    elem_type = fe.CellType.Type.quadrilateral
    mesh = fe.RectangleMesh(fe.Point(0.0, 0.0), fe.Point(0.5 * L, 0.5 * B), nx, ny, "right")
elif el_type == "tri":
    elem_type = fe.CellType.Type.triangle
    mesh = fe.RectangleMesh(fe.Point(0.0, 0.0), fe.Point(0.5 * L, 0.5 * B), nx, ny, "right")
elif el_type == "tri_ref":
    domain = Polygon(
        [fe.Point(0.0, 0.0), fe.Point(0.5 * L, 0.0), fe.Point(0.5 * L, 0.5 * B), fe.Point(0.0, 0.5 * B)])
    mesh = generate_mesh(domain, 0.7 * nx)
    # Refine
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near
        for f in fe.facets(c):
            if (math.sqrt((f.midpoint()[0] - 0.5 * L) ** 2 + (f.midpoint()[1] - 0.5 * B) ** 2)) < 0.1 * L:
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)
    # Refine
    markers = fe.MeshFunction("bool", mesh, 2)
    markers.set_all(False)
    for c in fe.cells(mesh):
        # Mark cells with facet midpoints near
        for f in fe.facets(c):
            if (math.sqrt((f.midpoint()[0] - 0.5 * L) ** 2 + (f.midpoint()[1] - 0.5 * B) ** 2)) < 0.05 * L:
                markers[c] = True
    mesh = fe.refine(mesh, markers, redistribute=False)
# mesh = fe.RectangleMesh.create([fe.Point(0.0, 0.0), fe.Point(0.5*L, 0.5*B)], [nx, ny], elem_type)
# mesh = fe.BoxMesh.create([fe.Point(0.0, 0.0, 0.0), fe.Point(L, B, H)], [nx, ny, nz],fe.CellType.Type.hexahedron)
# mesh = fe.RectangleMesh(fe.Point(0.0, 0.0), fe.Point(0.5*L, 0.5*B), nx, ny, "left/right")
# mesh = fe.RectangleMesh(fe.Point(0.0, 0.0), fe.Point(0.5 * L, 0.5 * B), nx, ny, "right")
#VV = fe.FunctionSpace(mesh, "CG", 1)
#print(VV.dim())
domain = Polygon([fe.Point(0.0, 0.0), fe.Point(0.5 * L, 0.0), fe.Point(0.5 * L, 0.5 * B), fe.Point(0.0, 0.5 * B)])
mesh = generate_mesh(domain, 1.2 * nx)
#VV = fe.FunctionSpace(mesh, "CG", 1)
#print(VV.dim())

fe.plot(mesh)
plt.show()

# Material parameters
E, nu, rho = 72e9, 0.22, 2500.0  # Young's module, Poisson ratio, density
mu, lmbda = 0.5 * E / (1 + nu), E * nu / (1 + nu) / (1 - 2 * nu)  # Lame's coefficients
lmbda = 2 * mu * lmbda / (lmbda + 2 * mu)  # Plane stress correction
ft_glass = 80.0e6  # Strengths of glass
# Fracture parameters
h_min = mesh.hmin()
lc = h_min
Gc_glass = lc * 8.0 / 3.0 * ft_glass ** 2 / E
k_res = 0.0
xi_shear = False
k_shear_i = 0.05
# Gc_glass = lc*256.0/27.0*ft_glass**2/E

# Important points
p_center = fe.Point(0.5*L, 0.5*B)
p_corner = fe.Point(0.0, 0.0)

# Contact parameters
E_imp = 210.0e9  # Young's modulus of impactor
nu_imp = 0.3  # Poisson's ratio of impactor
R = 0.05  # Radius of impactor
k_0 = 4.0 / 3.0 * math.sqrt(R) / ((1.0 - nu ** 2) / E + (1.0 - nu_imp ** 2) / E_imp)  # stiffness of contact
m_imp = 48.7  # Impactor mass
h0 = 0.2  # Initial height of impactor
v0 = math.sqrt(2 * 10 * h0)  # Initial velocity of impactor
vol = L * B  # Area of mesh
rho_imp = m_imp / vol  # Impactor weight distributed across whole mesh
pp = fe.Point(0.5 * L, 0.5 * B)  # Point of impact

# Snes solver parameters
# snes_solver_parameters = {"nonlinear_solver": "snes",
#                          "snes_solver": {"linear_solver": "lu",
#                                          "relative_tolerance": 1.0e-6,
#                                          "absolute_tolerance": 1.0e-6,
#                                          "maximum_iterations": 50,
#                                          "report": True,
#                                          "error_on_nonconvergence": False,
#                                          "line_search": "basic"}}

snes_solver_parameters = {"nonlinear_solver": "snes",
                          "snes_solver": {"method": "vinewtonssls",
                                          "line_search": "basic",
                                          "linear_solver": "mumps",
                                          "maximum_iterations": 100,
                                          "relative_tolerance": 1e-3,
                                          "absolute_tolerance": 1e-4,
                                          "report": True,
                                          "error_on_nonconvergence": False}}

# Time parameters
t_start = 0.0
# t_end = 0.03
t_end = 5.0e-3
# t_end = 1.0e-5
dt = 2.0e-7

time, i1, t1 = explicit_solver("solutions/small/plate_01_006", True, 20, "dist", el_type, "vol", k_shear_i)

plt.plot(t1, i1, label="1.0e6")
plt.xlabel("Time [s]")
plt.ylabel("Impactor displacement [m]")
plt.show()
