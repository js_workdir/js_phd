# -------------------------
# hertzlaw_2D_impact_fenics.py
# -------------------------

# -------------------------
# Description:
# - Impact of steel impactor into small glass plate
# - Modeled as 2D perpendicular cross-section
# - Contact by Hertz law
# - Implemented in FEniCS
# - Implicit Newmark integration scheme
#
# Last edit: 29.08. 2022
# -------------------------

import fenics as fe
import matplotlib.pyplot as plt
import math
import numpy as np
import time
from petsc4py import PETSc


# --------------------
# Functions and classes
# --------------------
def right(x, on_boundary):
    return on_boundary and x[0] < 0.5*l_x + 0.0001


def eps(v):
    return fe.sym(fe.grad(v))


def sigma(v):
    return lmbda * fe.tr(eps(v)) * fe.Identity(2) + 2.0 * mu * eps(v)


def update_values():
    ddu_old.assign(ddu)
    ddu.assign(4/(dt*dt)*(u-u_bar))
    du.assign(du + 0.5*dt*(ddu + ddu_old))


def mc_bracket(x):
    return 0.5*(x+abs(x))


def calc_time_solution(file):
    impactor = []
    dimpactor = []
    ddimpactor = []
    tt = []
    plate = []
    dplate = []
    ddplate = []

    u_imp = 0.0
    du_imp = v0
    ddu_imp = 0.0

    xdmffile_u = fe.XDMFFile(file + "/u.xdmf")
    xdmffile_u.parameters["flush_output"] = True

    t = t_start
    while t < t_end:

        X_bar.assign(X + dt * dX + 0.25 * dt * dt * ddX)
        #u_bar.assign(u + dt * du + 0.25 * dt * dt * ddu)
        #u_bar_imp = u_imp + dt * du_imp + 0.25 * dt * dt * ddu_imp

        X_vec = X.vector()
        X_bar_vec = X_bar.vector()
        #
        error = 10.0

        u_temp = fe.Function(VR)
        u_temp.assign(X)

        iterr = 0.0
        while error > tol:
            EOM = K * X_vec + 4.0 / (dt * dt) * M * (X_vec - X_bar_vec)
            J = K + 4.0 / (dt * dt) * M
            dF_hertz = 1.5 * k_0 * mc_bracket(-X.sub(1)((l_x / 2, 0)) + u_temp.sub(0)((l_x / 2, 0))[1]) ** 0.5
            F_hertz = k_0 * mc_bracket(-X.sub(1)((l_x / 2, 0)) + u_temp.sub(0)((l_x / 2, 0))[1]) ** 1.5
            p1 = fe.PointSource(VR.sub(0).sub(1), fe.Point(l_x / 2, 0), -F_hertz)
            p2 = fe.PointSource(VR.sub(0).sub(1), fe.Point(l_x / 2, 0), dF_hertz)
            p3 = fe.PointSource(VR.sub(1), fe.Point(l_x / 2, 0), F_hertz)
            p4 = fe.PointSource(VR.sub(1), fe.Point(l_x / 2, 0), dF_hertz)
            p2.apply(J)
            p4.apply(J)
            p1.apply(EOM)
            p3.apply(EOM)

            fe.solve(J, delta_X.vector(), EOM)
            X_new = X_vec - delta_X.vector()
            EOM = K * X_new + 4.0 / (dt * dt) * M * (X_new - X_bar_vec)
            p1.apply(EOM)
            error = EOM.norm("l2")
            print("error = ", error)
            print("t = ", t)

            if iterr > max_iter:
                print("max iterations reached")
                break

            iterr += 1

            X_vec = X_new
            u_temp.vector()[:] = X_vec

        X.vector()[:] = X_vec

        ddX_old.assign(ddX)
        ddX.assign(4 / (dt * dt) * (X - X_bar))
        dX.assign(dX + 0.5 * dt * (ddX + ddX_old))

        xdmffile_u.write(X, t)

        tt.append(t)
        impactor.append(X.sub(1)((l_x / 2, 0.0)))
        dimpactor.append(dX.sub(1)((l_x / 2, 0.0)))
        ddimpactor.append(ddX.sub(1)((l_x / 2, 0.0)))
        plate.append(X.sub(0)((l_x / 2, 0.0))[1])
        dplate.append(dX.sub(0)((l_x / 2, 0.0))[1])
        ddplate.append(ddX.sub(0)((l_x / 2, 0.0))[1])

        t = t + dt

    xdmffile_u.close()

    end = time.time()

    np.savetxt(file + "/output_data.txt", np.column_stack((tt, impactor, dimpactor, ddimpactor, plate, dplate, ddplate)),
               header="t\tu_imp\tdu_imp\tddu_imp\tu_plate\tdu_plate\tddu_plate")

    plt.plot(tt, ddimpactor)
    plt.ylabel("t")
    plt.xlabel("Acceleration of impactor")
    plt.figure()
    plt.plot(tt, impactor, label="impactor")
    plt.plot(tt, plate, label="plate")
    plt.legend()
    plt.xlabel("t")
    plt.ylabel("Displacement")
    plt.show()


def find_dof(p, d, VS):
    v_dofs = VS.tabulate_dof_coordinates()
    v0_dofs = VS.sub(d).dofmap().dofs()
    for i in range(0, len(v0_dofs)):
        v_x = v_dofs[v0_dofs[i], 0]
        v_y = v_dofs[v0_dofs[i], 1]
        if fe.near(v_x, p.x()) and fe.near(v_y, p.y()):
            return v0_dofs[i]
    return -1


# --------------------
# Parameters
# --------------------
n_x, n_y = 50, 5  # Number of elements
l_x, l_y = 0.5, 0.0146  # Dimensions of sample
E_1 = 70.0e9  # Young's modulus of glass
nu_1 = 0.23  # Poisson's ratio of glass
E_2 = 210.0e9  # Young's modulus of impactor
nu_2 = 0.3  # Poisson's ratio of impactor
R = 0.05  # Radius of impactor
rho = 2500.0  # Glass density
# F = 1.0
m_imp = 0.5*52.36  # Impactor mass
h0 = 0.1  # initial height of impactor
v0 = math.sqrt(2*10*h0)
#k = 1.0e6
#c0 = 1.0e9
sur = l_x * l_y
rho_imp = m_imp/sur

#dt = 2.5e-5  # time increment
dt = 1.0e-5
t_start = 0.0  # start time
#t_end = 4.0e-3  # end time
t_end = 3.0e-3

max_iter = 30
tol = 1.0e-4

mu = E_1 / 2 / (1 + nu_1)  # Lame's constants
lmbda = E_1 * nu_1 / (1 + nu_1) / (1 - 2 * nu_1)

k_0 = 4.0/3.0*math.sqrt(R)/((1.0-nu_1**2)/E_1+(1.0-nu_2**2)/E_2)  # stiffness of contact


# --------------------
# Geometry
# --------------------
mesh = fe.RectangleMesh(fe.Point(0.0, 0.0), fe.Point(l_x, l_y), n_x, n_y)  # triangular mesh

# mesh plot
fe.plot(mesh)
plt.show()

# --------------------
# Define spaces
# --------------------
#V = fe.VectorFunctionSpace(mesh, "CG", 2)
#u_tr = fe.TrialFunction(V)
#u_test = fe.TestFunction(V)

# Mixedspace
V = fe.VectorFunctionSpace(mesh, "CG", 2)
R = fe.FunctionSpace(mesh, "R", 0)
elementV = fe.VectorElement("CG", mesh.ufl_cell(), 2)
elementR = fe.FiniteElement("R", mesh.ufl_cell(), 0)
mix_elem = fe.MixedElement([elementV, elementR])
VR = fe.FunctionSpace(mesh, mix_elem)

# Functions from space VR
X = fe.Function(VR)  # Function in time t_i
#X_old = fe.Function(VR)  # Function in time t_{i-1}
#X_old2 = fe.Function(VR)  # Function in time t_{i-2}
dX = fe.Function(VR)
ddX = fe.Function(VR)

u_tr, r_tr = fe.TrialFunctions(VR)
u_test, r_test = fe.TestFunctions(VR)

#f1 = fe.Function(S)
#f2 = fe.Function(space)
#print(f1.vector()[:])
#print(f2.vector()[:])
#f3 = fe.Function(R)
#f3.vector()[0] = 1.0
#fe.plot(f3)
#plt.show()

# --------------------
# Boundary conditions
# --------------------
# bc = [fe.DirichletBC(V, fe.Constant((0.0, 0.0, 0.0)), left)]
bc = []

#top = fe.AutoSubDomain(lambda x: fe.near(x[2], l_z))
#edges = fe.AutoSubDomain(lambda x: fe.near(x[0], 0.0) or fe.near(x[0], l_x) or fe.near(x[1], 0.0) or fe.near(x[1], l_y))
#boundaries = fe.MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
#boundaries.set_all(0)
#edges.mark(boundaries, 1)
#ds = fe.ds(subdomain_data=boundaries)

#mesh_file = fe.File("mesh.pvd")
#mesh_file << boundaries

# --------------------
# Initialization
# --------------------
#u = fe.Function(V, name="Displacement")
#u_bar = fe.Function(V)
#u_hat = fe.Function(V)
#du = fe.Function(V)
#ddu = fe.Function(V)
ddX_old = fe.Function(VR)
delta_X = fe.Function(VR)
X_bar = fe.Function(VR)
u_bar = fe.Function(V)

# Initial velocity
fa = fe.FunctionAssigner(VR, [V, R])
v_init = fe.interpolate(fe.Constant((0.0, 0.0)), V)
r_init = fe.interpolate(fe.Constant(-dt*v0), R)
fa.assign(dX, [v_init, r_init])

#u_imp = 0.0
#du_imp = v0
#ddu_imp = 0.0

print(V.dim())
RR = fe.FunctionSpace(mesh, "R", 0)
print(RR.dim())

# Point distribution
f_point = fe.Expression(("0.0", "abs(x[0] - 0.5*L) < tol & abs(x[1]) < tol ? 1.0 : 0.0"), tol=0.001, L=l_x,
                        B=l_y,
                        degree=1)
f_fce_vect = fe.interpolate(f_point, V)
f_fce_real = fe.interpolate(fe.Constant(1 / (l_x * l_y)), R)

# fe.plot(f_fce_vect)
file_hit = fe.File("hit_fce.pvd")
file_hit << f_fce_vect
# plt.show()

#u_test, r_test = fe.TestFunctions(VR)
f_dest_form = fe.dot(f_fce_vect, u_test) * fe.dx
f_dest_form_2 = fe.dot(f_fce_real, r_test) * fe.dx

a_loc_vec_2 = fe.assemble(f_dest_form)
a_loc_vec_2[:] = -a_loc_vec_2[:] / sum(a_loc_vec_2[:])
a_loc_real = fe.assemble(f_dest_form_2)
index_real = np.nonzero(a_loc_real[:])[0]
index_hit = np.argmin(a_loc_vec_2[:])
# print(aaa)
# print(a_loc_vec_2[aaa])
# p2.apply(a_loc_vec_2)

a_loc_vec = a_loc_vec_2 + a_loc_real

# --------------------
# Variational forms
# --------------------
#A = fe.inner(sigma(u_tr), eps(u_test))*fe.dx + 4*rho/(dt*dt)*fe.dot(u_tr - X_bar.sub(0), u_test)*fe.dx
K_form = fe.inner(sigma(u_tr), eps(u_test))*fe.dx
#K_form += k*fe.dot(u_tr, u_test)*ds(1)
M_form = rho*fe.dot(u_tr, u_test)*fe.dx
M_form_2 = rho_imp * fe.dot(r_tr, r_test) * fe.dx
#C_form = c0*fe.dot(u_tr, u_test)*ds(1)
K = fe.PETScMatrix()
K = fe.assemble(K_form, tensor=K)
M = fe.assemble(M_form + M_form_2)
#C = fe.assemble(C_form)

# --------------------
# Time solution
# --------------------
#calc_time_solution_damp("ke10_middle_damp")
calc_time_solution("solutions/cross_hertz")
