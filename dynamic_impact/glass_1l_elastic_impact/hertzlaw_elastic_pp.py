# -------------------------
# hertzlaw_elastic_pp.py
# -------------------------

# -------------------------
# Description:
# - Postprocess file for elastic impact
#
# Last edit: 22.08. 2022
# -------------------------


import matplotlib.pyplot as plt
import numpy as np


# --------------------
# Functions
# --------------------
def plot_num_01(data_i, label_i, coef, ind_i):
    plt.plot(data_i[:, 0], coef*data_i[:, ind_i], label=label_i)


def plot_num_vel(data_i, label_i, coef, ind):
    dtt = data_i[1, 0] - data_i[0, 0]
    plt.plot(data_i[:, 0], coef*np.gradient(data_i[:, ind]) / dtt, label=label_i)


# --------------------
# Data loading
# --------------------
# Experiments
data_exp_1_2 = np.loadtxt("exp/obyc1_2cm.csv", delimiter=",")
data_exp_1_5 = np.loadtxt("exp/obyc1_5cm.csv", delimiter=",")
data_exp_1_10 = np.loadtxt("exp/obyc1_10cm.csv", delimiter=",")
data_exp_1_15 = np.loadtxt("exp/obyc1_15cm.csv", delimiter=",")
data_exp_1_20 = np.loadtxt("exp/obyc1_20cm.csv", delimiter=",")
data_exp_2_2 = np.loadtxt("exp/obyc2_2cm.csv", delimiter=",")
data_exp_2_20 = np.loadtxt("exp/obyc2_20cm.csv", delimiter=",")
data_exp_2_20_2 = np.loadtxt("exp/obyc2_202cm.csv", delimiter=",")
data_exp_2_20_3 = np.loadtxt("exp/obyc2_203cm.csv", delimiter=",")
data_exp_2_25 = np.loadtxt("exp/obyc2_25cm.csv", delimiter=",")
data_exp_2_30 = np.loadtxt("exp/obyc2_30cm.csv", delimiter=",")
data_exp_2_35 = np.loadtxt("exp/obyc2_35cm.csv", delimiter=",")
data_exp_1_3 = np.loadtxt("exp/05_3cm_export.csv", delimiter="\t", skiprows=1)

# Numerics - plate
data_num_1_10_imp = np.loadtxt("solutions/small/10cm/output_data.txt")
data_num_1_10_exp_plate = np.loadtxt("solutions/small/10cm_plate_explicit/impactor.txt")
data_num_1_02_exp_plate_01 = np.loadtxt("solutions/small/2cm_plate_explicit_01/impactor.txt")
data_num_1_10_exp_plate_01 = np.loadtxt("solutions/small/10cm_plate_explicit_01/impactor.txt")
data_num_1_10_exp_plate_02 = np.loadtxt("solutions/small/10cm_plate_explicit_02/impactor.txt")
data_num_1_10_exp_plate_03 = np.loadtxt("solutions/small/10cm_plate_explicit_03/impactor.txt")
data_num_1_15_exp_plate_01 = np.loadtxt("solutions/small/15cm_plate_explicit_03/impactor.txt")
data_num_1_10_exp_plate_ref = np.loadtxt("solutions/small/10cm_plate_explicit_ref/impactor.txt")
data_num_1_10_exp_plate_fine = np.loadtxt("solutions/small/10cm_plate_explicit_fine/impactor.txt")
data_num_1_10_exp_plate_fine2 = np.loadtxt("solutions/small/10cm_plate_explicit_fine2/impactor.txt")
data_num_1_10_exp_plate_tri_fine = np.loadtxt("solutions/small/10cm_plate_explicit_tri_fine/impactor.txt")
data_num_1_10_exp_plate_tri_fine2 = np.loadtxt("solutions/small/10cm_plate_explicit_tri_fine2/impactor.txt")
data_num_1_10_exp_plate_quad = np.loadtxt("solutions/small/10cm_plate_explicit_quad/impactor.txt")
data_num_1_5_exp_plate_quad = np.loadtxt("solutions/small/5cm_plate_explicit_quad/impactor.txt")
data_num_1_30_exp_plate_quad = np.loadtxt("solutions/small/30cm_plate_explicit_quad/impactor.txt")
data_num_1_10_exp_plate_quad_fine = np.loadtxt("solutions/small/10cm_plate_explicit_quad_fine/impactor.txt")
data_num_1_10_exp_plate_quad_fine2 = np.loadtxt("solutions/small/10cm_plate_explicit_quad_fine2/impactor.txt")

# Numerics - 3d
data_num_1_10_exp_3d = np.loadtxt("solutions/small/10cm_3d_explicit/impactor.txt")
data_num_1_10_exp_3d_tri_def = np.loadtxt("solutions/small/solutions/small/10cm_3d_tri_def/output_data.txt", skiprows=1)
data_num_1_10_exp_3d_tri_def_2 = np.loadtxt("solutions/small/10cm_3d_explicit_tri_def/impactor.txt", skiprows=1)

m_imp = 48.7

# --------------------
# Plotting
# --------------------
# Time resolution sensitivity
plot_num_01(data_exp_1_10, "exp", 1, 2)
plot_num_01(data_num_1_10_exp_plate_ref, "dt=4e-5", -m_imp, 2)
plot_num_01(data_num_1_10_exp_plate_fine, "dt=2e-5", -m_imp, 2)
plot_num_01(data_num_1_10_exp_plate_fine2, "dt=1e-5", -m_imp, 2)
plt.xlabel("Time [s]")
plt.ylabel("Force [N]")
plt.legend()
plt.show()

# Mesh sensitivity
plot_num_01(data_exp_1_10, "exp", 1, 1)
#plot_num_01(data_exp_1_10, "exp", 1, 2)
plot_num_01(data_num_1_10_exp_plate_ref, "tri_20x20", -m_imp, 2)
plot_num_01(data_num_1_10_exp_plate_tri_fine, "tri_40x40", -m_imp, 2)
plot_num_01(data_num_1_10_exp_plate_tri_fine2, "tri_80x80", -m_imp, 2)
plot_num_01(data_num_1_10_exp_plate_quad, "quad_20x20", -m_imp, 2)
#plot_num_01(data_num_1_10_exp_plate_quad_fine, "quad_40x40", -m_imp, 2)
#plot_num_01(data_num_1_10_exp_plate_quad_fine2, "quad_80x80", -m_imp, 2)
plot_num_01(data_num_1_10_exp_3d_tri_def, "3d", -m_imp, 3)
plot_num_01(data_num_1_10_exp_3d_tri_def_2, "3d_2", -m_imp, 2)
plt.xlim(-0.0001, 0.0035)
plt.xlabel("Time [s]")
plt.ylabel("Force [N]")
plt.legend()
plt.show()

# Different heights
plot_num_01(data_exp_1_5, "exp_5cm", 1, 1)
plot_num_01(data_exp_2_30, "exp_30cm", 1, 1)
plot_num_01(data_num_1_5_exp_plate_quad, "num_5cm", -m_imp, 2)
plot_num_01(data_num_1_30_exp_plate_quad, "num_30cm", -m_imp, 2)
plt.xlim(-0.0001, 0.0035)
plt.xlabel("Time [s]")
plt.ylabel("Force [N]")
plt.legend()
plt.show()

# Velocities
plot_num_01(data_exp_1_5, "exp", 1, 3)
#plot_num_01(data_num_1_10_exp_plate_quad, "quad_20x20", -m_imp, 2)
plot_num_vel(data_num_1_5_exp_plate_quad, "quad_20x20", -1, 4)
#plot_num_vel(data_num_1_5_exp_plate_quad, "quad_80x80", -1, 4)
#plot_num_01(data_num_1_10_exp_3d_tri_def, "3d", -m_imp, 3)
#plt.xlim(-0.0001, 0.0035)
plt.xlabel("Time [s]")
plt.ylabel("Velocity [m/s]")
plt.legend()
plt.show()




















# Plot - exp
#plot_num_01(data_num_1_10_exp_plate_ref, "dt=4e-7")
#plt.plot(data_exp_1_10[:, 0], data_exp_1_10[:, 2], label="exp")
#plt.plot(data_exp_1_3[:, 0], data_exp_1_3[:, 2], label="exp2")
#plt.plot(data_num_1_10_imp[:, 0], -48.7*data_num_1_10_imp[:, 3], label="imp")
#plt.plot(data_num_1_10_exp_plate[:, 0], -48.7*data_num_1_10_exp_plate[:, 2], label="explicit-plate")
#plt.plot(data_num_1_10_exp_plate_01[:, 0], -48.7*data_num_1_10_exp_plate_01[:, 2], label="explicit-plate")
#plt.plot(data_num_1_10_exp_plate_02[:, 0], -48.7*data_num_1_10_exp_plate_02[:, 2], label="explicit-plate-fine")
#plt.plot(data_num_1_10_exp_plate_03[:, 0], -48.7*data_num_1_10_exp_plate_03[:, 2], label="explicit-plate-quad")
#plt.plot(data_num_1_10_exp_3d[:, 0], -48.7*data_num_1_10_exp_3d[:, 2], label="explicit-3d")
#plt.legend()
#plt.xlim(0, 0.001)
#plt.show()

# Plot - exp
#plt.plot(data_exp_1_2[:, 0], data_exp_1_2[:, 2], label="exp-2cm")
#plt.plot(data_exp_1_15[:, 0], data_exp_1_15[:, 2], label="exp-15cm")
#plt.plot(data_num_1_15_exp_plate_01[:, 0], -48.7*data_num_1_15_exp_plate_01[:, 2], label="explicit-plate-2cm")
#plt.plot(data_num_1_02_exp_plate_01[:, 0], -48.7*data_num_1_02_exp_plate_01[:, 2], label="explicit-plate-15cm")
#plt.legend()
#plt.xlim(0, 0.001)
#plt.show()
