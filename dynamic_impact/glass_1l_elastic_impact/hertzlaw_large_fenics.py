# -------------------------
# hertzlaw_large_fenics
# -------------------------

# -------------------------
# Description:
# - Impact of steel impactor into large glass plate
# - Contact by Hertz law
# - Implemented in FEniCS
#
# Last edit: 22.08. 2022
# -------------------------
