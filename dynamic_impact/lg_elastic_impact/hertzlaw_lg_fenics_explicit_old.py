# -------------------------
# hertzlaw_lg_vis_explicit.py
# -------------------------

# -------------------------
# Description:
# - Impact of steel impactor into laminated glass plate
# - Contact by Hertz law
# - Implemented in FEniCS
# - Explicit integration scheme
#
# Last edit: 25.08. 2022
# -------------------------

import fenics as fe
import matplotlib.pyplot as plt
import math
import numpy as np
import time
import os
import lumping_scheme_quad
from petsc4py import PETSc


# --------------------
# Functions and classes
# --------------------
def left(x, on_boundary):
    return on_boundary and x[0] < 0.0001


def right_edge(x, on_boundary):
    return on_boundary and fe.near(x[0], 0.5 * l_x)


def top_edge(x, on_boundary):
    return on_boundary and fe.near(x[1], 0.5 * l_y)


def eps(v):
    return fe.sym(fe.grad(v))


def create_log_file(folder, p_type, model_type, mesh):
    directory = os.path.dirname(folder + "/")
    if not os.path.exists(directory):
        os.makedirs(directory)
    f = open(folder + "/log.txt", "w")
    f.write("Model type: " + str(model_type) + "\n")
    f.write("Number of elements: " + str((n_x, n_y)) + "\n")
    f.write("Material parameters: E=" + str(E_1) + ", nu=" + str(nu_1) + ", rho=" + str(rho_1) + "\n")
    f.write("Impactor: m=" + str(m_imp) + ", E=" + str(E_2) + ", nu=" + str(nu_2) + "\n")
    f.write("Impact height: h0=" + str(h0) + "\n")
    f.write("Elements: " + str(mesh.ufl_cell()) + "-" + str(deg) + "\n")
    f.write("Point-load type: " + p_type + "\n")


def mc_bracket(x):
    return 0.5*(x+abs(x))


def map_to_layers_test(fce_i, h_i, dpcopy, lnum):
    # w1, u1, phi1, u3, phi3, ...
    u_i = [None] * lnum
    w_i = [None] * lnum
    phi_i = [None] * lnum

    S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])
    S1 = fe.as_tensor([[0.0, -1.0], [1.0, 0.0]])
    fce_i = fe.split(fce_i)

    for j in range(lnum):
        w_i[j] = fce_i[0]
        if j % 2 == 0:
            u_i[j] = fce_i[1 + j]
            phi_i[j] = fce_i[2 + j]

    for j in range(lnum):
        if j % 2 != 0:
            # 1, 3, ...
            u_i[j] = 0.25 * h_i[j-1] * S * phi_i[j-1] - 0.25 * h_i[j+1] * S * phi_i[j+1] + 0.5 * u_i[j+1] + 0.5 * u_i[j-1]
            phi_i[j] = S1 * (-0.5 * S * phi_i[j-1] * h_i[j-1] / h_i[j] - 0.5 * S * phi_i[j+1] * h_i[j+1] / h_i[j] + \
                       u_i[j+1] / h_i[j] - u_i[j-1] / h_i[j])

    return w_i, u_i, phi_i


def get_K_form(V, model_type):
    dx_shear = fe.dx(metadata={"quadrature_degree": 0})

    lg_trial = fe.TrialFunction(V)
    lg_test = fe.TestFunction(V)
    S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

    w_tr, u_tr, phi_tr = map_to_layers_test(lg_trial, h, False, len(h))
    w_test, u_test, phi_test = map_to_layers_test(lg_test, h, False, len(h))

    K_form = 0.0

    for i in range(n_lay):
        D1 = (E[i] * h[i] ** 3) / (12.0 * (1.0 - nu[i] ** 2))
        D2 = (E[i] * k[i] * h[i]) / (2.0 * (1.0 + nu[i]))
        D3 = (E[i] * h[i]) / (1.0 - nu[i] ** 2)
        kappa_tr, kappa_test = eps(S * phi_tr[i]), eps(S * phi_test[i])
        du_tr, du_test = eps(u_tr[i]), eps(u_test[i])
        K_form += fe.inner(D1 * (nu[i] * fe.div(S * phi_tr[i]) * fe.Identity(2) + (1.0 - nu[i]) * kappa_tr),
                           kappa_test) * fe.dx
        K_form += fe.inner(D3 * (nu[i] * fe.div(u_tr[i]) * fe.Identity(2) + (1.0 - nu[i]) * du_tr), du_test) * fe.dx
        K_form += D2 * fe.inner(fe.grad(w_tr[i]) + S * phi_tr[i], fe.grad(w_test[i]) + S * phi_test[i]) * dx_shear

    return K_form


def get_M_form(V, lump, podvod):
    if lump:
        if el_type == "rect":
            dx_m = fe.dx(scheme="lumped", degree=2)
        elif el_type == "tri":
            dx_m = fe.dx(scheme="vertex", metadata={"degree": 1, "representation": "quadrature"})
    else:
        dx_m = fe.dx

    lg_trial = fe.TrialFunction(V)
    lg_test = fe.TestFunction(V)
    S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

    w_tr, u_tr, phi_tr = map_to_layers_test(lg_trial, h, False, len(h))
    w_test, u_test, phi_test = map_to_layers_test(lg_test, h, False, len(h))

    M_form = 0.0

    for i in range(n_lay):
        if i % 2 == 0:
            M_form += rho[i]*h[i]**3/12.0*fe.dot(S*phi_tr[i], S*phi_test[i])*dx_m
            M_form += rho[i]*h[i]*fe.dot(w_tr[i], w_test[i])*dx_m
            M_form += rho[i]*h[i]*fe.dot(u_tr[i], u_test[i])*dx_m
        else:
            if not podvod:
                M_form += rho[i] * h[i] ** 3 / 12.0 * fe.dot(S * phi_tr[i], S * phi_test[i]) * dx_m
                M_form += rho[i] * h[i] * fe.dot(w_tr[i], w_test[i]) * dx_m
                M_form += rho[i] * h[i] * fe.dot(u_tr[i], u_test[i]) * dx_m

    l_tr = fe.TrialFunctions(V)
    l_test = fe.TestFunctions(V)

    M_form_2 = rho_imp_2d * fe.dot(l_tr[-1], l_test[-1]) * fe.dx

    return M_form, M_form_2


def explicit_solver_plate(folder, mesh, saving, save_rate, point_type, el_type_i, vis):
    # --------------------
    # Define spaces
    # --------------------
    p1_element = fe.FiniteElement("P", mesh.ufl_cell(), deg)
    v1_element = fe.VectorElement("P", mesh.ufl_cell(), deg)
    r1_element = fe.FiniteElement("R", mesh.ufl_cell(), 0)
    v1_elems_num = glass_num*2
    element = fe.MixedElement([p1_element] + v1_elems_num*[v1_element] + [r1_element])
    V = fe.FunctionSpace(mesh, element)
    V_sca = fe.FunctionSpace(mesh, p1_element)
    V_vec = fe.FunctionSpace(mesh, v1_element)
    V_rea = fe.FunctionSpace(mesh, r1_element)
    V_mix = [V_sca] + v1_elems_num*[V_vec] + [V_rea]

    print("Number of unknowns: " + str(V.dim()))
    impactor = []
    impactor_acc = []
    tt = []
    u_bottom = []
    u_side = []
    u_corner = []
    u_quarter = []


    # Displacement functions
    X = fe.Function(V)  # Displacement in time t_i
    X_old = fe.Function(V)  # Displacement in time t_{i-1}
    X_old2 = fe.Function(V)  # Displacement in time t_{i-2}

    # Initial velocity
    fa = fe.FunctionAssigner(V, V_mix)
    s_init = fe.interpolate(fe.Constant(0.0), V_sca)
    v_init = fe.interpolate(fe.Constant((0.0, 0.0)), V_vec)
    r_init = fe.interpolate(fe.Constant(-dt*v0), V_rea)
    fa.assign(X_old2, [s_init] + v1_elems_num*[v_init] + [r_init])

    # Indices of points
    ind_bottom = find_dof(p_bottom, 0, V)
    ind_side = find_dof(p_side, 0, V)
    ind_corner = find_dof(p_corner, 0, V)
    ind_quarter = find_dof(p_quarter, 0, V)

    # Point distribution
    f_point = fe.Expression("abs(x[0] - 0.5*L) < tol & abs(x[1] - 0.5*B) < tol ? 1.0 : 0.0", tol=0.001, L=l_x,
                            B=l_y,
                            degree=1)
    f_fce_vect = fe.interpolate(f_point, V_sca)
    f_fce_real = fe.interpolate(fe.Constant(1 / (0.25 * l_x * l_y)), V_rea)

    # fe.plot(f_fce_vect)
    file_hit = fe.File(folder + "/hit_fce.pvd")
    file_hit << f_fce_vect
    # plt.show()

    w_test = fe.TestFunctions(V)

    f_dest_form = fe.dot(f_fce_vect, w_test[0]) * fe.dx
    f_dest_form_2 = fe.dot(f_fce_real, w_test[-1]) * fe.dx

    a_loc_vec_2 = fe.assemble(f_dest_form)
    a_loc_vec_2[:] = -a_loc_vec_2[:] / sum(a_loc_vec_2[:])
    a_loc_real = fe.assemble(f_dest_form_2)
    index_real = np.nonzero(a_loc_real[:])[0]
    index_hit = np.argmin(a_loc_vec_2[:])

    if point_type == "point":
        a_loc_vec = a_loc_vec_1
    elif point_type == "dist":
        a_loc_vec = a_loc_vec_2 + a_loc_real
    else:
        raise Exception("Only point/dist point type implemented!")

    # Dirichlet boundary conditions
    # w1, u1, phi1, u3, phi3, ..., r
    bc = []
    for i in range(glass_num):
        ind1 = i * 2 + 1
        ind2 = i * 2 + 2
        bc.append(fe.DirichletBC(V.sub(ind2).sub(1), fe.Constant(0.0), right_edge))
        bc.append(fe.DirichletBC(V.sub(ind2).sub(0), fe.Constant(0.0), top_edge))
        bc.append(fe.DirichletBC(V.sub(ind1).sub(0), fe.Constant(0.0), right_edge))
        bc.append(fe.DirichletBC(V.sub(ind1).sub(1), fe.Constant(0.0), top_edge))

    # File with solution
    file_w = fe.XDMFFile(folder + "/displ_w.xdmf")
    file_w.parameters["flush_output"] = True
    #file_u = fe.XDMFFile(folder + "/displ_u.xdmf")
    #file_u.parameters["flush_output"] = True

    # Forms for matrices K and M
    K_form = get_K_form(V, "plate")
    M_form_1, M_form_2 = get_M_form(V, True, "plate")

    # Assembling of matrices
    K_matrix = fe.assemble(K_form)
    [bci.apply(K_matrix) for bci in bc]
    M_matrix = fe.assemble(M_form_1) + fe.assemble(M_form_2)
    [bci.apply(M_matrix) for bci in bc]

    M_fce = fe.Function(V)
    M_vect = M_fce.vector()
    M_matrix.get_diagonal(M_vect)

    # Main time loop

    saveindex = save_rate
    t = t_start
    begin_t = time.time()
    create_log_file(folder, point_type, "plate", mesh)
    while t < t_end:
        print("Time instant: " + str(t))

        if vis:
            # Update foil value
            update_foil(t)
            K_matrix = fe.assemble(K_form)
            [bci.apply(K_matrix) for bci in bc]

        # K_matrix = fe.assemble(K_form)
        # [bci.apply(K_matrix) for bci in bc]

        # Hertz nonlinear force
        # F_hertz = 0.25*k_0*mc_bracket(X_old.sub(3)(pp) - X_old.sub(0)(pp))**1.5
        F_hertz = np.asscalar(0.25 * k_0 * mc_bracket(X_old.vector()[index_real] - X_old.vector()[index_hit]) ** 1.5)
        # F_hertz = 0

        # Update of RHS
        w_vector = -dt ** 2 * K_matrix*X_old.vector() - M_matrix * (
                    X_old2.vector() - 2.0 * X_old.vector()) - dt ** 2 * F_hertz * a_loc_vec
        # w_vector = -dt**2*K_matrix*X_old.vector() - M_matrix*(X_old2.vector() - 2.0*X_old.vector()) -dt**2*F_hertz*a_loc_vec
        [bci.apply(w_vector) for bci in bc]

        iMw = fe.Function(V)
        iMw.vector().set_local(w_vector.get_local() / M_vect.get_local())
        X.assign(iMw)

        if saving:
            if saveindex >= save_rate:
                w_fce = X.split(deepcopy=True)
                file_w.write(w_fce[0], t)

                # impactor.append(u_imp(pp))
                impactor.append(X.vector()[index_real])
                temp_acc = X.vector()[index_real] - 2*X_old.vector()[index_real] + X_old2.vector()[index_real]
                temp_acc /= dt**2
                impactor_acc.append(temp_acc)
                tt.append(t)
                u_bottom.append(X.vector()[ind_bottom])
                u_side.append(X.vector()[ind_side])
                u_corner.append(X.vector()[ind_corner])
                u_quarter.append(X.vector()[ind_quarter])

                saveindex = 0
            else:
                saveindex += 1

        X_old2.assign(X_old)
        X_old.assign(X)

        t += dt

    end_t = time.time()
    file_w.close()

    np.savetxt(folder + "/impactor.txt", np.column_stack([tt, impactor, impactor_acc, u_bottom, u_side, u_corner, u_quarter]))

    return end_t - begin_t, impactor, impactor_acc, tt


def find_dof(p, d, VS):
    v_dofs = VS.tabulate_dof_coordinates()
    v0_dofs = VS.sub(d).dofmap().dofs()
    for i in range(0, len(v0_dofs)):
        v_x = v_dofs[v0_dofs[i], 0]
        v_y = v_dofs[v0_dofs[i], 1]
        if fe.near(v_x, p.x()) and fe.near(v_y, p.y()):
            return v0_dofs[i]
    return -1


# Get foil stiffness for given time t_i
def get_G(t_i, t_act):
    a_t = math.pow(10.0, -c1*(t_act - Tref)/(c2 + t_act - Tref))
    temp = Ginf
    n = len(Gs)
    temp += sum(Gs[k]*math.exp(-t_i/(a_t*thetas[k])) for k in range(0, n))
    return temp


# Calculate new lambda and mu for foil
def update_foil(t_i):
    G_foil_t = get_G(t_i, Tact)
    G_foil.g = G_foil_t


# --------------------
# Parameters
# --------------------
n_x, n_y = 20, 20  # Number of elements
l_x, l_y = 0.5, 0.5  # Dimensions of sample
E_1 = 70.0e9  # Young's modulus of glass
nu_1 = 0.22  # Poisson's ratio of glass
rho_1 = 2500.0  # Glass density
rho_2 = 1100.0  # Density of foil
h = [0.005, 0.00228, 0.006, 0.00076, 0.005]  # Thicknesses
#h = [0.005, 0.00152, 0.008, 0.00076, 0.008, 0.00076, 0.005]
h_tot = sum(h)  # Total thickness of lg beam
n_lay = len(h)  # Number of layers
glass_num = int((len(h) + 1) / 2)  # Number of glass layers

Gs = 1.0e3*np.array([1782124.2, 519208.7, 546176.8, 216893.2, 13618.3, 4988.3, 1663.8, 587.2, 258.0, 63.8, 168.4])
thetas = np.array([1.0e-5, 1.0e-4, 1.0e-3, 1.0e-2, 1.0e-1, 1.0, 1.0e1, 1.0e2, 1.0e3, 1.0e4, 1.0e5])
nu_vis = 0.49
Ginf = 232.26e3
c1 = 8.635
c2 = 42.422
Tref = 20.0
Tact = 25.0

#G_foil = get_G(0.003, 20.0)
G_foil = fe.Expression("g", g=0.0, degree=0)  # G value
E_foil = 2.0*G_foil*(1+nu_vis)


# Material parameters
E = (glass_num-1)*[E_1, E_foil] + [E_1] # Young's module
print(E)
nu = (glass_num-1)*[nu_1, nu_vis] + [nu_1]  # Poisson ratio
rho = (glass_num-1)*[rho_1, rho_2] + [rho_1]   # Density
k = (glass_num-1)*[5.0/6.0, 1.0] + [5.0/6.0]

E_2 = 210.0e9  # Young's modulus of impactor
nu_2 = 0.3  # Poisson's ratio of impactor
R = 0.05  # Radius of impactor
# F = 1.0
m_imp = 48.2  # Impactor mass
h0 = 0.05  # initial height of impactor
v0 = math.sqrt(2*9.81*h0)
sur = l_x * l_y  # Area of mesh
rho_imp_2d = m_imp / sur  # Impactor weight distributed across whole 2d mesh

# Degree orders
deg = 1  # Order of elements
deg_3d = 2
deg_shear = 0  # Order of shear elements
el_type = "rect"  # Type of elements

# Time parameters
#dt = 2.5e-5  # time increment
dt = 5.0e-7
t_start = 0.0  # start time
#t_end = 4.0e-3  # end time
t_end = 5.0e-3

max_iter = 30
tol = 1.0e-4

#mu = E / 2 / (1 + nu)  # Lame's constants
#lmbda = E * nu / (1 + nu) / (1 - 2 * nu)

k_0 = 4.0/3.0*math.sqrt(R)/((1.0-nu_1**2)/E_1+(1.0-nu_2**2)/E_2)  # stiffness of contact

# Important points
p_bottom = fe.Point(0.5*l_x, 0.0)
p_side = fe.Point(0.0, 0.5*l_y)
p_corner = fe.Point(0.0, 0.0)
p_quarter = fe.Point(0.25*l_x, 0.0)

# --------------------
# Geometry
# --------------------
# Mesh
elem_type = fe.CellType.Type.quadrilateral
if el_type == "rect":
    elem_type = fe.CellType.Type.quadrilateral
elif el_type == "tri":
    elem_type = fe.CellType.Type.triangle
mesh_2d = fe.RectangleMesh.create([fe.Point(0.0, 0.0), fe.Point(0.5*l_x, 0.5*l_y)], [n_x, n_y], elem_type)
# mesh = fe.BoxMesh.create([fe.Point(0.0, 0.0, 0.0), fe.Point(L, B, H)], [nx, ny, nz],fe.CellType.Type.hexahedron)
# mesh = fe.RectangleMesh(fe.Point(0.0, 0.0), fe.Point(0.5*L, 0.5*B), nx, ny, "left/right")
#mesh_2d = fe.RectangleMesh(fe.Point(0.0, 0.0), fe.Point(0.5 * l_x, 0.5 * l_y), n_x, n_y, "right")

# mesh plot
#fe.plot(mesh_2d)
#plt.show()

# --------------------
# Time solution
# --------------------

# Time resolution sensitivity
time1, i1, ii1, t1 = explicit_solver_plate("solutions/small/7l_5cm_plate_explicit_gfoil_vis", mesh_2d, True, 20, "dist", el_type, True)
#dt = 1.0e-6
#time1, i1, ii1, t1 = explicit_solver_plate("solutions/small/10cm_plate_explicit_01_fine", mesh_2d, True, 20, "dist", el_type)
#time1, i1, ii1, t1 = explicit_solver_plate("solutions/small/10cm_plate_explicit_02", mesh_2d, True, 20, "dist", el_type)
#el_type = "rect"
#mesh_2d = fe.RectangleMesh.create([fe.Point(0.0, 0.0), fe.Point(0.5*l_x, 0.5*l_y)], [n_x, n_y], fe.CellType.Type.quadrilateral)
#time1, i1, ii1, t1 = explicit_solver_plate("solutions/lg/10cm_plate_explicit_01", mesh_2d, True, 20, "dist", el_type)
#time2, i2, ii2, t2 = explicit_solver_3d("solutions/small/10cm_3d_explicit", mesh_3d, True, 20, "dist", el_type)

plt.plot(t1, ii1, label="plate")
plt.xlabel("Time [s]")
plt.ylabel("Impactor acceleration [m]")
plt.legend()
plt.show()

#calc_time_solution_damp("ke10_middle_damp")
#calc_time_solution("solutions/small/10cm")
