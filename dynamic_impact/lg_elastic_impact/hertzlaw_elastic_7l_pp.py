# -------------------------
# hertzlaw_elastic_7l_pp.py
# -------------------------

# -------------------------
# Description:
# - Postprocess file for elastic impact
# - 7layer configuration
#
# Last edit: 06.09. 2022
# -------------------------

import matplotlib.pyplot as plt
import numpy as np
import math
from scipy.signal import filtfilt


# --------------------
# Functions
# --------------------
def plot_num_01(data_i, label_i, coef, ind):
    plt.plot(data_i[:, 0], coef*data_i[:, ind], label=label_i)


def plot_num_acc(data_i, label_i, coef, ind):
    dtt = data_i[1, 0] - data_i[0, 0]
    plt.plot(data_i[:, 0], coef*np.gradient(data_i[:, ind]) / dtt, label=label_i)


def cfc1000_filter(raw_data, dts):
    cfc = 1000.
    omega_d = 2. * math.pi * cfc * 2.0775  # 1.25 * 5./3.
    omega_a = math.tan(omega_d*dts/2.)  # math.sin(omega_d*dts/2.) / math.cos(omega_d*dts/2.)
    # print(omega_d, omega_a)
    a0 = omega_a**2. / (1. + (2.**0.5)*omega_a + omega_a**2.)
    a1 = 2. * a0
    a2 = a0
    b1 = -2. * (omega_a**2. - 1.) / (1. + (2.**0.5)*omega_a + omega_a**2.)
    b2 = (-1. + (2.**0.5*omega_a) - omega_a**2.) / (1. + (2.**0.5*omega_a) + omega_a**2.)
    b = [a0, a1, a2]
    a = [1., -b1, -b2]
    # print(a, b)
    return filtfilt(b, a, raw_data)


def calc_acc(data_i):
    return np.gradient(data_i)
    #return np.diff(data_i, n=1)


# --------------------
# Data loading
# --------------------
# Experiments
# 5cm impact
data_exp_4_6_5cm_1 = np.loadtxt("exp_lg/4_6_5cm_1.csv", delimiter=",", skiprows=1)
data_exp_4_6_5cm_2 = np.loadtxt("exp_lg/4_6_5cm_2.csv", delimiter=",", skiprows=1)
data_exp_4_6_5cm_3 = np.loadtxt("exp_lg/4_6_5cm_3.csv", delimiter=",", skiprows=1)
data_exp_4_7_5cm_1 = np.loadtxt("exp_lg/4_7_5cm_1.csv", delimiter=",", skiprows=1)
data_exp_4_7_5cm_2 = np.loadtxt("exp_lg/4_7_5cm_2.csv", delimiter=",", skiprows=1)
data_exp_4_7_5cm_3 = np.loadtxt("exp_lg/4_7_5cm_3.csv", delimiter=",", skiprows=1)
data_exp_4_8_5cm_1 = np.loadtxt("exp_lg/4_8_5cm_1.csv", delimiter=",", skiprows=1)
data_exp_4_8_5cm_2 = np.loadtxt("exp_lg/4_8_5cm_2.csv", delimiter=",", skiprows=1)
data_exp_4_8_5cm_3 = np.loadtxt("exp_lg/4_8_5cm_3.csv", delimiter=",", skiprows=1)
# 30 cm impact
data_exp_4_6_30cm_1 = np.loadtxt("exp_lg/4_6_30cm_1.csv", delimiter=",", skiprows=1)
data_exp_4_7_30cm_1 = np.loadtxt("exp_lg/4_7_30cm_1.csv", delimiter=",", skiprows=1)

# Numerics
data_num_7l_5cm_01 = np.loadtxt("solutions/small/7l_5cm_plate_explicit_vis/impactor.txt")
#data_num_7l_5cm_02 = np.loadtxt("solutions/small/7l_5cm_plate_explicit_gfoil_vis/impactor.txt")
data_num_7l_30cm_01 = np.loadtxt("solutions/small/7l_30cm_plate_explicit_vis/impactor.txt")

m_imp = 48.7

# --------------------
# Plotting
# --------------------
# Comparison of experiment
plt.title("Experiment vs. numerics, 5cm, 30cm")
plot_num_01(data_exp_4_6_5cm_1, "exp1, 5cm", 1, 1)
plot_num_01(data_exp_4_7_5cm_1, "exp2, 5cm", 1, 1)
plot_num_01(data_exp_4_8_5cm_1, "exp3, 5cm", 1, 1)
plot_num_01(data_exp_4_6_30cm_1, "exp4, 30cm", 1, 1)
plot_num_01(data_exp_4_7_30cm_1, "exp5, 30cm", 1, 1)
plot_num_01(data_num_7l_5cm_01, "num, 5cm", -m_imp, 2)
plot_num_01(data_num_7l_30cm_01, "num, 30cm", -m_imp, 2)
plt.xlabel("Time [s]")
plt.ylabel("Impactor acceleration [m/s^2]")
plt.legend()
plt.show()

# Comparison
plt.title("Data filtering")
data_filt = data_num_7l_5cm_01
plot_num_01(data_filt, "num_def", -m_imp, 2)
dt = data_filt[1, 0]-data_filt[0, 0]
force1 = cfc1000_filter(cfc1000_filter(-m_imp*data_filt[:, 2], dt), dt)
plt.plot(data_filt[:, 0], force1, label="num_def_filt")
plot_num_01(data_exp_4_6_5cm_1, "exp6-1", 1, 1)
plt.xlabel("Time [s]")
plt.ylabel("Impactor acceleration [m/s^2]")
plt.legend()
plt.show()

# Comparison of bottom and side velocity
plt.title("Bottom and side velocity on plate, 5cm")
plot_num_01(data_exp_4_6_5cm_1, "exp1_bottom", 1, 2)
plot_num_01(data_exp_4_7_5cm_1, "exp2_bottom", 1, 2)
plot_num_01(data_exp_4_6_5cm_1, "exp1_side", 1, 3)
plot_num_01(data_exp_4_7_5cm_1, "exp2_side", 1, 3)
plot_num_acc(data_num_7l_5cm_01, "num_vis_bottom", 1, 3)
plot_num_acc(data_num_7l_5cm_01, "num_vis_side", 1, 4)
plt.xlabel("Time [s]")
plt.ylabel("Plate velocity [m/s]")
plt.legend()
plt.show()

# Comparison of bottom and side velocity
plt.title("Bottom and side velocity on plate, 30cm")
plot_num_01(data_exp_4_6_30cm_1, "exp1_bottom", 1, 2)
plot_num_01(data_exp_4_6_30cm_1, "exp1_side", 1, 3)
plot_num_01(data_exp_4_7_30cm_1, "exp2_bottom", 1, 2)
plot_num_01(data_exp_4_7_30cm_1, "exp2_side", 1, 3)
plot_num_acc(data_num_7l_30cm_01, "num_bottom", 1, 3)
plot_num_acc(data_num_7l_30cm_01, "num_side", 1, 4)
plt.xlabel("Time [s]")
plt.ylabel("Plate velocity [m/s]")
plt.legend()
plt.show()

# Comparison of corner velocities
plt.title("Corner velocity on plate, 5cm")
plot_num_01(data_exp_4_6_5cm_1, "exp", 1, 4)
plot_num_acc(data_num_7l_5cm_01, "num_vis_bottom", 1, 5)
plt.xlabel("Time [s]")
plt.ylabel("Plate velocity [m/s]")
plt.legend()
plt.show()

# Comparison of corner velocities
plt.title("Corner velocity on plate, 30cm")
plot_num_01(data_exp_4_6_30cm_1, "exp1", 1, 4)
plot_num_01(data_exp_4_7_30cm_1, "exp2", 1, 4)
plot_num_acc(data_num_7l_30cm_01, "num_bottom", 1, 5)
plt.xlabel("Time [s]")
plt.ylabel("Plate velocity [m/s]")
plt.legend()
plt.show()

# Comparison
plt.title("Data filtering")
data_filt = data_num_7l_5cm_01
plot_num_acc(data_filt, "num", 1, 5)
dt = data_filt[1, 0]-data_filt[0, 0]
#dtt = data_num_gfoil_vis_10[1, 0] - data_num_gfoil_vis_10[0, 0]
#plt.plot(data_i[:, 0], coef * np.gradient(data_num_gfoil_vis_10[:, ind]) / dtt, label=label_i)
force1 = cfc1000_filter(cfc1000_filter(np.gradient(data_filt[:, 5])/dt, dt), dt)
plt.plot(data_filt[:, 0], force1, label="num_filt")
plot_num_01(data_exp_4_6_5cm_1, "exp", 1, 4)
plot_num_01(data_exp_4_6_5cm_1, "exp", 1, 4)
plt.xlabel("Time [s]")
plt.ylabel("Plate velocity [m/s]")
plt.legend()
plt.show()
