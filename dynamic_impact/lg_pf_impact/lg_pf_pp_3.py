# -------------------------
# lg_pf_pp_3py
# -------------------------

# -------------------------
# Description:
# - Postprocess file for first pf lg
# - Third pp file for new solver
#
# Last edit: 23.1. 2023
# -------------------------

import numpy as np
import matplotlib.pyplot as plt


# --------------------
# Functions
# --------------------
def plot_num_vel(data_i, label_i, coef, ind, col="r"):
    dtt = data_i[1, 0] - data_i[0, 0]
    plt.plot(data_i[:, 0], coef*np.gradient(data_i[:, ind]) / dtt, label=label_i, c=col)


def save_velocities(file):
    data_file = np.loadtxt(file + "/impactor.txt", skiprows=1)
    dtt = data_file[1, 0] - data_file[0, 0]
    vel01 = np.gradient(data_file[:, 3])/dtt
    vel02 = np.gradient(data_file[:, 4]) / dtt
    vel03 = np.gradient(data_file[:, 5]) / dtt
    vel04 = np.gradient(data_file[:, 6]) / dtt
    np.savetxt(file + "/velocities.txt", np.column_stack([data_file[:, 0], vel01, vel02, vel03, vel04]))


# --------------------
# Data loading
# --------------------
# numerics
data_5l_meshtestc_01 = np.loadtxt("solutions/5l_meshtestc_01/impactor.txt", skiprows=1)
data_5l_meshtestc_02 = np.loadtxt("solutions/5l_meshtestc_02/impactor.txt", skiprows=1)
data_5l_meshtestc_03 = np.loadtxt("solutions/5l_meshtestc_03/impactor.txt", skiprows=1)
data_5l_meshtestc_04 = np.loadtxt("solutions/5l_meshtestc_04/impactor.txt", skiprows=1)
data_5l_meshtestc_05 = np.loadtxt("solutions/5l_meshtestc_05/impactor.txt", skiprows=1)
data_5l_meshtestc_06 = np.loadtxt("solutions/5l_meshtestc_06/impactor.txt", skiprows=1)

# Experiments
data_exp_3_1_45 = np.loadtxt("../exp/3_1_45cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_1_30 = np.loadtxt("../exp/3_1_30cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_1_25 = np.loadtxt("../exp/3_1_25cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_1_20 = np.loadtxt("../exp/3_1_20cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_1_15 = np.loadtxt("../exp/3_1_15cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_1_15_2 = np.loadtxt("../exp/3_1_15cm_2.csv", skiprows=1, delimiter=",")
data_exp_3_1_10 = np.loadtxt("../exp/3_1_10cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_1_5 = np.loadtxt("../exp/3_1_5cm_1.csv", skiprows=1, delimiter=",")

data_exp_3_2_5 = np.loadtxt("../exp/3_2_5cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_2_10 = np.loadtxt("../exp/3_2_10cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_2_15 = np.loadtxt("../exp/3_2_15cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_2_20 = np.loadtxt("../exp/3_2_20cm_1.csv", skiprows=1, delimiter=",")

data_exp_3_3_40 = np.loadtxt("../exp/3_3_40cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_3_30 = np.loadtxt("../exp/3_3_30cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_3_25 = np.loadtxt("../exp/3_3_25cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_3_20 = np.loadtxt("../exp/3_3_20cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_3_15 = np.loadtxt("../exp/3_3_15cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_3_10 = np.loadtxt("../exp/3_3_10cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_3_5 = np.loadtxt("../exp/3_3_5cm_1.csv", skiprows=1, delimiter=",")

data_exp_3_4_5 = np.loadtxt("../exp/3_4_5cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_4_65 = np.loadtxt("../exp/3_4_65cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_4_70 = np.loadtxt("../exp/3_4_70cm_1.csv", skiprows=1, delimiter=",")

#   save_velocities("solutions/5l_fine_test_4_025")

# --------------------
# Plotting
# --------------------
# Exp and num comparison - coarse 2
plt.plot(data_exp_3_4_65[:, 0], data_exp_3_4_65[:, 1], label="exp1_65cm")
plt.plot(data_exp_3_4_70[:, 0], data_exp_3_4_70[:, 1], label="exp1_70cm")
plt.plot(data_5l_meshtestc_01[:, 0], -data_5l_meshtestc_01[:, 2]*48.2, label="num_c_1")
plt.plot(data_5l_meshtestc_02[:, 0], -data_5l_meshtestc_02[:, 2]*48.2, label="num_c_2")
plt.plot(data_5l_meshtestc_03[:, 0], -data_5l_meshtestc_03[:, 2]*48.2, label="num_c_3")
plt.plot(data_5l_meshtestc_04[:, 0], -data_5l_meshtestc_04[:, 2]*48.2, label="num_c_4")
plt.plot(data_5l_meshtestc_05[:, 0], -data_5l_meshtestc_05[:, 2]*48.2, label="num_c_5")
plt.plot(data_5l_meshtestc_06[:, 0], -data_5l_meshtestc_06[:, 2]*48.2, label="num_c_6")
plt.xlabel("Time [s]")
plt.ylabel("Contact force")
plt.xlim(0, 0.005)
plt.legend()
plt.show()
