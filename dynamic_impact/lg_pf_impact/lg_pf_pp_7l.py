# -------------------------
# lg_pf_pp_7l.py
# -------------------------

# -------------------------
# Description:
# - Postprocess file for 7l pf lg
#
# Last edit: 06.12. 2022
# -------------------------

import numpy as np
import matplotlib.pyplot as plt


# --------------------
# Functions
# --------------------
def plot_num_vel(data_i, label_i, coef, ind, col="r"):
    dtt = data_i[1, 0] - data_i[0, 0]
    plt.plot(data_i[:, 0], coef*np.gradient(data_i[:, ind]) / dtt, label=label_i, c=col)


# --------------------
# Data loading
# --------------------
# numerics
data_num_7l_01 = np.loadtxt("solutions/7l_solver_1_040/impactor.txt", skiprows=1)
data_num_7l_02 = np.loadtxt("solutions/7l_solver_1_035/impactor.txt", skiprows=1)

data_num_7l_fine_both = np.loadtxt("solutions/7l_solver_fine_both/impactor.txt", skiprows=1)
data_num_7l_fine_40 = np.loadtxt("solutions/7l_solver_fine_040/impactor.txt", skiprows=1)
data_num_7l_fine_45 = np.loadtxt("solutions/7l_solver_fine_045_2/impactor.txt", skiprows=1)

# Experiments
data_exp_4_6_30 = np.loadtxt("../exp/4_6_30cm_1.csv", skiprows=1, delimiter=",")
data_exp_4_6_35 = np.loadtxt("../exp/4_6_35cm_1.csv", skiprows=1, delimiter=",")
data_exp_4_6_40 = np.loadtxt("../exp/4_6_40cm_1.csv", skiprows=1, delimiter=",")
data_exp_4_6_45 = np.loadtxt("../exp/4_6_45cm_1.csv", skiprows=1, delimiter=",")
data_exp_4_6_50 = np.loadtxt("../exp/4_6_50cm_1.csv", skiprows=1, delimiter=",")

data_exp_4_7_35 = np.loadtxt("../exp/4_7_35cm_1.csv", skiprows=1, delimiter=",")
data_exp_4_7_40 = np.loadtxt("../exp/4_7_40cm_1.csv", skiprows=1, delimiter=",")
data_exp_4_7_45 = np.loadtxt("../exp/4_7_45cm_1.csv", skiprows=1, delimiter=",")

# --------------------
# Plotting
# --------------------
# Exp and num comparison - coarse 2
plt.plot(data_exp_4_7_35[:, 0], data_exp_4_7_35[:, 1], label="exp2_35cm")
plt.plot(data_exp_4_7_40[:, 0], data_exp_4_7_40[:, 1], label="exp2_40cm")
plt.plot(data_exp_4_7_45[:, 0], data_exp_4_7_45[:, 1], label="exp2_45cm")
plt.plot(data_num_7l_fine_both[:, 0], -data_num_7l_fine_both[:, 2]*48.2, label="num_both")
plt.xlabel("Time [s]")
plt.ylabel("Contact force")
plt.legend()
plt.show()

# Exp and num comparison - coarse 2
plt.plot(data_exp_4_6_30[:, 0], data_exp_4_6_30[:, 1], label="exp1_30cm")
plt.plot(data_exp_4_7_35[:, 0], data_exp_4_7_35[:, 1], label="exp2_35cm")
plt.plot(data_num_7l_02[:, 0], -data_num_7l_02[:, 2]*48.2, label="num_1_35")
plt.plot(data_num_7l_01[:, 0], -data_num_7l_01[:, 2]*48.2, label="num_1_40")
plt.plot(data_num_7l_fine_40[:, 0], -data_num_7l_fine_40[:, 2]*48.2, label="num_fine_40")
plt.plot(data_num_7l_fine_45[:, 0], -data_num_7l_fine_45[:, 2]*48.2, label="num_fine_45")
#plt.plot(data_num_7l_fine_both[:, 0], -data_num_7l_fine_both[:, 2]*48.2, label="num_both")
plt.xlabel("Time [s]")
plt.ylabel("Contact force")
plt.legend()
plt.show()

# Exp and num comparison - coarse 2
plt.plot(data_exp_4_6_30[:, 0], data_exp_4_6_30[:, 1], label="exp1_30cm")
plt.plot(data_exp_4_6_35[:, 0], data_exp_4_6_35[:, 1], label="exp1_35cm")
plt.plot(data_exp_4_6_40[:, 0], data_exp_4_6_40[:, 1], label="exp1_40cm")
plt.plot(data_exp_4_6_45[:, 0], data_exp_4_6_45[:, 1], label="exp1_45cm")
plt.plot(data_exp_4_6_50[:, 0], data_exp_4_6_50[:, 1], label="exp1_50cm")
plt.plot(data_num_7l_01[:, 0], -data_num_7l_01[:, 2]*48.2, label="num_1")
plt.xlabel("Time [s]")
plt.ylabel("Contact force")
plt.legend()
plt.show()
