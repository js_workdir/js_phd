# -------------------------
# lg_pf_pp.py
# -------------------------

# -------------------------
# Description:
# - Postprocess file for first pf lg
#
# Last edit: 13.11. 2022
# -------------------------

import numpy as np
import matplotlib.pyplot as plt


# --------------------
# Functions
# --------------------
def plot_num_vel(data_i, label_i, coef, ind, col="r"):
    dtt = data_i[1, 0] - data_i[0, 0]
    plt.plot(data_i[:, 0], coef*np.gradient(data_i[:, ind]) / dtt, label=label_i, c=col)


# --------------------
# Data loading
# --------------------
# numerics
data_num_50_tri = np.loadtxt("solutions/lg_5l_50_004_tri/impactor.txt")
data_num_50_tri_2 = np.loadtxt("solutions/lg_5l_50_003_tri/impactor.txt")
data_num_100_tri = np.loadtxt("solutions/lg_5l_100_001_tri/impactor.txt")
data_num_100_tri_2 = np.loadtxt("solutions/lg_5l_100_002_tri/impactor.txt")
data_num_100_tri_3 = np.loadtxt("solutions/lg_5l_100_003_tri/impactor.txt")
data_num_100_tri_3_2 = np.loadtxt("solutions/lg_5l_100_004_tri/impactor.txt")
data_num_100_tri_6 = np.loadtxt("solutions/lg_5l_100_005_tri/impactor.txt")  ##
data_num_100_tri_4 = np.loadtxt("solutions/lg_5l_01_40_180_tri_100/impactor.txt")  ##
data_num_100_tri_5 = np.loadtxt("solutions/lg_5l_01_40_180_tri_001/impactor.txt")
data_num_01_10_01 = np.loadtxt("solutions/lg_5l_01_10_200_tri/impactor.txt")
data_num_5l_tri_un = np.loadtxt("solutions/lg_5l_tri_un/impactor.txt")
data_num_kshear_test = np.loadtxt("solutions/test_kshare/impactor.txt")
data_num_kshear_test_2 = np.loadtxt("solutions/test_kshare_2/impactor.txt")
data_num_kshear_test_2_lower = np.loadtxt("solutions/test_kshare_2_lower_005/impactor.txt")
data_num_kshear_test_2_lower_01 = np.loadtxt("solutions/test_kshare_2_lower_01/impactor.txt")

# Experiments
data_exp_3_1_45 = np.loadtxt("../exp/3_1_45cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_1_30 = np.loadtxt("../exp/3_1_30cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_1_25 = np.loadtxt("../exp/3_1_25cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_1_20 = np.loadtxt("../exp/3_1_20cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_1_15 = np.loadtxt("../exp/3_1_15cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_1_15_2 = np.loadtxt("../exp/3_1_15cm_2.csv", skiprows=1, delimiter=",")
data_exp_3_1_10 = np.loadtxt("../exp/3_1_10cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_1_5 = np.loadtxt("../exp/3_1_5cm_1.csv", skiprows=1, delimiter=",")

data_exp_3_2_5 = np.loadtxt("../exp/3_2_5cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_2_15 = np.loadtxt("../exp/3_2_15cm_1.csv", skiprows=1, delimiter=",")

data_exp_3_3_25 = np.loadtxt("../exp/3_3_25cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_3_20 = np.loadtxt("../exp/3_3_20cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_3_15 = np.loadtxt("../exp/3_3_15cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_3_10 = np.loadtxt("../exp/3_3_10cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_3_5 = np.loadtxt("../exp/3_3_5cm_1.csv", skiprows=1, delimiter=",")

data_exp_3_4_5 = np.loadtxt("../exp/3_4_5cm_1.csv", skiprows=1, delimiter=",")
data_exp_3_4_70 = np.loadtxt("../exp/3_4_70cm_1.csv", skiprows=1, delimiter=",")

# --------------------
# Plotting
# --------------------
# Experiments - contact force for 5 cm
plt.plot(data_exp_3_1_5[:, 0], data_exp_3_1_5[:, 1], label="exp1_5cm")
plt.plot(data_exp_3_2_5[:, 0], data_exp_3_2_5[:, 1], label="exp2_5cm")
plt.plot(data_exp_3_3_5[:, 0], data_exp_3_3_5[:, 1], label="exp3_5cm")
plt.plot(data_exp_3_4_5[:, 0], data_exp_3_4_5[:, 1], label="exp4_5cm")
plt.xlabel("Time [s]")
plt.ylabel("Contact force")
plt.legend()
plt.show()

# Experiments - contact force after first crack
plt.plot(data_exp_3_1_15_2[:, 0], data_exp_3_1_15_2[:, 1], label="exp1_15cm")
plt.plot(data_exp_3_2_15[:, 0], data_exp_3_2_15[:, 1], label="exp2_15cm")
plt.plot(data_exp_3_3_20[:, 0], data_exp_3_3_20[:, 1], label="exp3_20cm")
plt.plot(data_exp_3_4_70[:, 0], data_exp_3_4_70[:, 1], label="exp4_70cm")
plt.plot(data_exp_3_3_25[:, 0], data_exp_3_3_25[:, 1], label="exp3_25cm")
plt.xlabel("Time [s]")
plt.ylabel("Contact force")
plt.legend()
plt.show()

# Experiments - contact force for 10 and 15 cm
plt.plot(data_exp_3_1_10[:, 0], data_exp_3_1_10[:, 1], label="exp3_10cm")
plt.plot(data_exp_3_1_15[:, 0], data_exp_3_1_15[:, 1], label="exp3_15cm")
plt.plot(data_exp_3_1_15_2[:, 0], data_exp_3_1_15_2[:, 1], label="exp3_15cm")
plt.xlabel("Time [s]")
plt.ylabel("Contact force")
plt.legend()
plt.show()

# Experiments - contact force for 10 and 15 cm
plt.plot(data_exp_3_1_10[:, 0], data_exp_3_1_10[:, 1], label="exp3_10cm")
plt.plot(data_exp_3_1_15[:, 0], data_exp_3_1_15[:, 1], label="exp3_15cm")
plt.plot(data_exp_3_3_10[:, 0], data_exp_3_3_10[:, 1], label="exp3_3_10cm")
plt.plot(data_exp_3_3_15[:, 0], data_exp_3_3_15[:, 1], label="exp3_3_15cm")
#plt.plot(data_exp_3_1_15_2[:, 0], data_exp_3_1_15_2[:, 1], label="exp3_15cm")
plt.xlabel("Time [s]")
plt.ylabel("Contact force")
plt.legend()
plt.show()

# Experiments - contact force for 10 and 15 cm
plt.plot(data_exp_3_1_10[:, 0], data_exp_3_1_10[:, 1], label="exp3_10cm")
plt.plot(data_exp_3_1_15[:, 0], data_exp_3_1_15[:, 1], label="exp3_15cm")
plt.plot(data_exp_3_1_15_2[:, 0], data_exp_3_1_15_2[:, 1], label="exp3_15cm")
plt.plot(data_exp_3_1_20[:, 0], data_exp_3_1_20[:, 1], label="exp3_20cm")
plt.plot(data_exp_3_3_25[:, 0], data_exp_3_3_25[:, 1], label="exp3_3_20cm")
plt.xlabel("Time [s]")
plt.ylabel("Contact force")
plt.legend()
plt.show()

# Experiments - contact force
#plt.plot(data_exp_3_1_10[:, 0], data_exp_3_1_10[:, 1], label="exp3_10cm")
plt.plot(data_exp_3_1_15[:, 0], data_exp_3_1_15[:, 1], label="exp3_15cm")
plt.plot(data_exp_3_1_25[:, 0], data_exp_3_1_25[:, 1], label="exp3_25cm")
plt.plot(data_exp_3_1_30[:, 0], data_exp_3_1_30[:, 1], label="exp3_30cm")
plt.plot(data_exp_3_1_45[:, 0], data_exp_3_1_45[:, 1], label="exp3_45cm")
plt.xlabel("Time [s]")
plt.ylabel("Contact force")
plt.legend()
plt.show()

# Experiments - velocities
plt.plot(data_exp_3_1_10[:, 0], data_exp_3_1_10[:, 2], label="exp3_10cm")
plt.plot(data_exp_3_1_15[:, 0], data_exp_3_1_15[:, 2], label="exp3_15cm")
plt.plot(data_exp_3_1_25[:, 0], data_exp_3_1_25[:, 2], label="exp3_25cm")
plt.plot(data_exp_3_1_30[:, 0], data_exp_3_1_30[:, 2], label="exp3_30cm")
plt.xlabel("Time [s]")
plt.ylabel("Bottom velocity")
plt.legend()
plt.show()

# Experiments - velocities
plt.plot(data_exp_3_1_10[:, 0], data_exp_3_1_10[:, 4], label="exp3_10cm")
plt.plot(data_exp_3_1_15[:, 0], data_exp_3_1_15[:, 4], label="exp3_15cm")
plt.plot(data_exp_3_1_25[:, 0], data_exp_3_1_25[:, 4], label="exp3_25cm")
plt.plot(data_exp_3_1_30[:, 0], data_exp_3_1_30[:, 4], label="exp3_30cm")
plt.xlabel("Time [s]")
plt.ylabel("Corner velocity")
plt.legend()
plt.show()

# Plot of contact force
plt.plot(data_exp_3_1_5[:, 0], -data_exp_3_1_5[:, 1], "--", c="r", label="exp, 5cm")
plt.plot(data_exp_3_1_10[:, 0], -data_exp_3_1_10[:, 1], "--", c="b", label="exp, 10cm")
plt.plot(data_exp_3_1_30[:, 0], -data_exp_3_1_30[:, 1], "--", c="g", label="exp, 30cm")
plt.plot(data_num_kshear_test_2_lower[:, 0], data_num_kshear_test_2_lower[:, 2]*48.2, c="r", label="num, 5cm")
plt.plot(data_num_kshear_test_2_lower_01[:, 0], data_num_kshear_test_2_lower_01[:, 2]*48.2, c="b", label="num, 10cm")
plt.plot(data_num_kshear_test_2[:, 0], data_num_kshear_test_2[:, 2]*48.2, c="g", label="num, 30cm")
plt.legend()
plt.xlabel("Time [s]")
plt.ylabel("Impactor acceleration")
plt.show()

# Plot of bottom displacement
plt.plot(data_exp_3_1_5[:, 0], data_exp_3_1_5[:, 2], "--", c="r", label="exp")
plt.plot(data_exp_3_1_10[:, 0], data_exp_3_1_10[:, 2], "--", c="b", label="exp")
plt.plot(data_exp_3_1_30[:, 0], data_exp_3_1_30[:, 2], "--", c="g", label="exp")
plot_num_vel(data_num_kshear_test_2_lower, "num, 5cm", 1, 3, col="r")
plot_num_vel(data_num_kshear_test_2_lower_01, "num, 10cm", 1, 3, col="b")
plot_num_vel(data_num_kshear_test_2, "num, 30cm", 1, 3, col="g")
#plot_num_vel(data_num_100_tri_3, "num_100_3", 1, 3)
#plot_num_vel(data_num_100_tri_4, "num_100_4", 1, 3)
#plt.plot(data_num_100_tri_4[:, 0], data_num_100_tri_4[:, 5], label="100_4")
#plt.plot(data_num_100_tri_5[:, 0], data_num_100_tri_5[:, 3], label="100_5")
plt.legend()
plt.xlabel("Time [s]")
plt.ylabel("Bottom velocity")
plt.show()

# Plot of contact force
plt.plot(data_exp_3_1_5[:, 0], -data_exp_3_1_5[:, 1], label="exp")
plt.plot(data_exp_3_1_10[:, 0], -data_exp_3_1_10[:, 1], label="exp10")
plt.plot(data_num_kshear_test_2_lower[:, 0], data_num_kshear_test_2_lower[:, 2]*48.2, label="kshear_lower")
plt.legend()
plt.xlabel("Time [s]")
plt.ylabel("Impactor acceleration")
plt.show()

# Plot of contact force
plt.plot(data_exp_3_1_5[:, 0], -data_exp_3_1_5[:, 1], label="exp5")
plt.plot(data_exp_3_1_10[:, 0], -data_exp_3_1_10[:, 1], label="exp")
plt.plot(data_num_kshear_test_2_lower_01[:, 0], data_num_kshear_test_2_lower_01[:, 2]*48.2, label="kshear_lower")
plt.legend()
plt.xlabel("Time [s]")
plt.ylabel("Impactor acceleration")
plt.show()

# Plot of contact force
plt.plot(data_exp_3_1_25[:, 0], -data_exp_3_1_25[:, 1], label="exp")
plt.plot(data_num_5l_tri_un[:, 0], data_num_5l_tri_un[:, 2]*48.2, label="triun")
plt.plot(data_num_kshear_test[:, 0], data_num_kshear_test[:, 2]*48.2, label="kshear")
plt.plot(data_num_kshear_test_2[:, 0], data_num_kshear_test_2[:, 2]*48.2, label="kshear_2")
plt.plot(data_num_kshear_test_2_lower[:, 0], data_num_kshear_test_2_lower[:, 2]*48.2, label="kshear_lower")
plt.plot(data_num_kshear_test_2_lower_01[:, 0], data_num_kshear_test_2_lower_01[:, 2]*48.2, label="kshear_lower_01")
plt.legend()
plt.xlabel("Time [s]")
plt.ylabel("Impactor acceleration")
plt.show()

# Plot of contact force
plt.plot(data_exp_3_1_25[:, 0], -data_exp_3_1_25[:, 1], label="exp")
#plt.plot(data_num_01_10_01[:, 0], data_num_01_10_01[:, 2], label="01_10")
#plt.plot(data_num_50_tri[:, 0], data_num_50_tri[:, 2], label="50")
plt.plot(data_num_50_tri_2[:, 0], data_num_50_tri_2[:, 2]*48.2, label="num, ft=55MPa")
#plt.plot(data_num_100_tri[:, 0], data_num_100_tri[:, 2]*48.2, label="100")
#plt.plot(data_num_100_tri_2[:, 0], data_num_100_tri_2[:, 2]*48.2, label="100_2")
#plt.plot(data_num_100_tri_3[:, 0], data_num_100_tri_3[:, 2], label="100_3")
#plt.plot(data_num_100_tri_3_2[:, 0], data_num_100_tri_3_2[:, 2], label="100_3")
plt.plot(data_num_100_tri_4[:, 0], data_num_100_tri_4[:, 2]*48.2, label="num, ft=180MPa")
plt.plot(data_num_100_tri_6[:, 0], data_num_100_tri_6[:, 2]*48.2, label="num, ft_1L=55MPa")
plt.legend()
plt.xlabel("Time [s]")
plt.ylabel("Impactor acceleration")
plt.show()

# Plot of bottom displacement
plt.plot(data_exp_3_1_25[:, 0], data_exp_3_1_25[:, 2], label="exp")
#plot_num_vel(data_exp_3_1_10, "exp", 0.001, 2)
#plot_num_vel(data_num_100_tri, "num_100", 1, 3)
plot_num_vel(data_num_100_tri_4, "num_100", 1, 3)
#plot_num_vel(data_num_100_tri_3, "num_100_3", 1, 3)
#plot_num_vel(data_num_100_tri_4, "num_100_4", 1, 3)
#plt.plot(data_num_100_tri_4[:, 0], data_num_100_tri_4[:, 5], label="100_4")
#plt.plot(data_num_100_tri_5[:, 0], data_num_100_tri_5[:, 3], label="100_5")
plt.legend()
plt.xlabel("Time [s]")
plt.ylabel("Bottom velocity")
plt.show()
