import numpy as np
import matplotlib.pyplot as plt
import math

E_2 = 210.0e9  # Young's modulus of impactor
nu_2 = 0.3  # Poisson's ratio of impactor
R = 0.05  # Radius of impactor
# F = 1.0
m_imp = 48.2  # Impactor mass
# h0 = 0.05  # initial height of impactor
E_1 = 70.0e9  # Young's modulus of glass
nu_1 = 0.22  # Poisson's ratio of glass

k_0 = 4.0 / 3.0 * math.sqrt(R) / ((1.0 - nu_1 ** 2) / E_1 + (1.0 - nu_2 ** 2) / E_2)  # stiffness of contact

dd = 0.0005  # indent

F_hertz = 0.25*k_0*dd**1.5

print(F_hertz)
