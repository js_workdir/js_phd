# -------------------------
# Plate_lumping_elastic_3L_hertz.py
# -------------------------

# -------------------------
# Description:
# - Explicit dynamic without damage
# - Mindlin plate theory
# - Multilayer implementation - all layers are elastic
# - Impact via Hertz law
#
# Last edit: 16.12. 2021
# -------------------------

import fenics as fe
import matplotlib.pyplot as plt
import time
import numpy as np
import math


def eps(v):
    return fe.sym(fe.grad(v))


def mc_bracket(x):
    return 0.5*(x+abs(x))


def left_side(x, on_boundary):
    return on_boundary and fe.near(x[0], 0.0)


def right_side(x, on_boundary):
    return on_boundary and fe.near(x[0], 0.5*L)


def all_edges(x, on_boundary):
    return on_boundary


def left_bottom_edges(x, on_boundary):
    return on_boundary and (fe.near(x[0], 0.0) or fe.near(x[1], 0.0))


def right_edge(x, on_boundary):
    return on_boundary and fe.near(x[0], 0.5*L)


def top_edge(x, on_boundary):
    return on_boundary and fe.near(x[1], 0.5*B)


def calc_highest_f(K_i, M_i, V, folder):
    eigensolver = fe.SLEPcEigenSolver(K_i, M_i)
    #eigensolver.parameters["problem_type"] = "gen_hermitian"
    eigensolver.parameters["spectrum"] = "smallest real"
    #eigensolver.parameters["spectral_transform"] = "shift-and-invert"
    #eigensolver.parameters["spectral_shift"] = 0.0
    #eigensolver.parameters['solver'] = "power"
    eigensolver.solve(1)
    print(eigensolver.parameters.str(True))
    r, c, rx, cx = eigensolver.get_eigenpair(0)
    print(eigensolver.get_number_converged())
    if eigensolver.get_number_converged() > 0:
        print("Largest eigenvalue is " + str(r))
        eigenmode = fe.Function(V)
        eigenmode.vector()[:] = rx
        eifile = fe.File(folder + "/eigenmode.pvd")
        eifile << eigenmode
    else:
        r = 0.0
    return r


# Get foil stiffness for given time t_i
def get_G(t_i, t_act):
    a_t = math.pow(10.0, -pvb_c1*(t_act - pvb_tref)/(pvb_c2 + t_act - pvb_tref))
    temp = pvb_ginf
    n = len(pvb_g)
    temp += sum(pvb_g[k]*math.exp(-t_i/(a_t*pvb_thetas[k])) for k in range(0, n))
    return temp


# Only for 3 layered
def map_to_layers(fce_i, h_i):
    # w1, u1, phi1, u3, phi3, ...
    u_i = [None]*3
    w_i = [None]*3
    phi_i = [None]*3

    S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])
    S1 = fe.as_tensor([[0.0, -1.0], [1.0, 0.0]])
    w1_m, u1_m, p1_m, u3_m, p3_m, u_imp = fe.split(fce_i)

    u_i[0] = u1_m
    u_i[2] = u3_m
    w_i[0] = w1_m
    w_i[1] = w1_m
    w_i[2] = w1_m
    phi_i[0] = p1_m
    phi_i[2] = p3_m
    u_i[1] = 0.25*h_i[0]*S*phi_i[0] - 0.25*h_i[2]*S*phi_i[2] + 0.5*u_i[2] + 0.5*u_i[0]
    phi_i[1] = S1*(-0.5*S*phi_i[0]*h_i[0]/h_i[1] - 0.5*S*phi_i[2]*h_i[2]/h_i[1] + u_i[2]/h_i[1] - u_i[0]/h_i[1])

    return w_i, u_i, phi_i


def get_K_form(V):
    dx_shear = fe.dx(metadata={"quadrature_degree": 0})

    lg_trial = fe.TrialFunction(V)
    lg_test = fe.TestFunction(V)
    S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

    w_tr, u_tr, phi_tr = map_to_layers(lg_trial, H)
    w_test, u_test, phi_test = map_to_layers(lg_test, H)

    K_form = 0.0

    for i in range(3):
        D1 = (E[i] * H[i] ** 3) / (12.0 * (1.0 - nu[i] ** 2))
        D2 = (E[i] * k[i] * H[i]) / (2.0 * (1.0 + nu[i]))
        D3 = (E[i] * H[i]) / (1.0 - nu[i] ** 2)
        kappa_tr, kappa_test = eps(S * phi_tr[i]), eps(S * phi_test[i])
        du_tr, du_test = eps(u_tr[i]), eps(u_test[i])
        K_form += fe.inner(D1 * (nu[i] * fe.div(S * phi_tr[i]) * fe.Identity(2) + (1.0 - nu[i]) * kappa_tr),
                           kappa_test) * fe.dx
        K_form += fe.inner(D3 * (nu[i] * fe.div(u_tr[i]) * fe.Identity(2) + (1.0 - nu[i]) * du_tr), du_test) * fe.dx
        K_form += D2 * fe.inner(fe.grad(w_tr[i]) + S * phi_tr[i], fe.grad(w_test[i]) + S * phi_test[i]) * dx_shear

    return K_form


def get_F_form(V):
    lg_test = fe.TestFunctions(V)
    F_form = f_ext*lg_test[0]*fe.dx
    return F_form


def get_M_form(V, lump, podvod):
    if lump:
        dx_m = fe.dx(scheme="vertex", metadata={"degree": 1, "representation": "quadrature"})
    else:
        dx_m = fe.dx

    lg_trial = fe.TrialFunction(V)
    lg_test = fe.TestFunction(V)
    S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

    w_tr, u_tr, phi_tr = map_to_layers(lg_trial, H)
    w_test, u_test, phi_test = map_to_layers(lg_test, H)

    M_form = 0.0

    lg_trial = fe.split(lg_trial)
    lg_test = fe.split(lg_test)

    if podvod:
        M_form += rho[0]*H[0]**3/12.0*fe.dot(S*lg_trial[2], S*lg_test[2])*dx_m
        M_form += rho[0]*H[0]*fe.dot(lg_trial[0], lg_test[0])*dx_m
        M_form += rho[0]*H[0]*fe.dot(lg_trial[1], lg_test[1])*dx_m
        M_form += rho[2]*H[2]**3/12.0*fe.dot(S*lg_trial[4], S*lg_test[4])*dx_m
        M_form += rho[2]*H[2]*fe.dot(lg_trial[0], lg_test[0])*dx_m
        M_form += rho[2]*H[2]*fe.dot(lg_trial[3], lg_test[3])*dx_m

        M_form += rho[1]*H[1]*fe.dot(lg_trial[0], lg_test[0])*dx_m
    else:
        for i in range(3):
            M_form += rho[i]*H[i]**3/12.0*fe.dot(S*phi_tr[i], S*phi_test[i])*dx_m
            M_form += rho[i]*H[i]*fe.dot(w_tr[i], w_test[i])*dx_m
            M_form += rho[i]*H[i]*fe.dot(u_tr[i], u_test[i])*dx_m

    M_form_2 = rho_imp*fe.dot(lg_trial[5], lg_test[5])*fe.dx

    return M_form, M_form_2


def explicit_solver(folder, saving, saverate):

    # Define spaces
    p1_element = fe.FiniteElement("P", mesh.ufl_cell(), deg)
    v1_element = fe.VectorElement("P", mesh.ufl_cell(), deg)
    r1_element = fe.FiniteElement("R", mesh.ufl_cell(), 0)
    element = fe.MixedElement([p1_element, v1_element, v1_element, v1_element, v1_element, r1_element])
    V = fe.FunctionSpace(mesh, element)
    V_sca = fe.FunctionSpace(mesh, p1_element)
    V_vec = fe.FunctionSpace(mesh, v1_element)
    V_rea = fe.FunctionSpace(mesh, r1_element)
    print("Number of unknowns: " + str(V.dim()))

    # Localization vector
    a_loc = fe.Function(V)
    a_loc_vec = a_loc.vector()
    p1 = fe.PointSource(V.sub(0), pp, -1.0)
    p2 = fe.PointSource(V.sub(5), pp, 1.0)
    p1.apply(a_loc_vec)
    p2.apply(a_loc_vec)

    # Displacement functions
    X = fe.Function(V)  # Displacement in time t_i
    X_old = fe.Function(V)  # Displacement in time t_{i-1}
    X_old2 = fe.Function(V)  # Displacement in time t_{i-2}
    vel = fe.Function(V)  # Velocity field of w

    # Initial velocity
    fa = fe.FunctionAssigner(V, [V_sca, V_vec, V_vec, V_vec, V_vec, V_rea])
    s_init = fe.interpolate(fe.Constant(0.0), V_sca)
    v_init = fe.interpolate(fe.Constant((0.0, 0.0)), V_vec)
    r_init = fe.interpolate(fe.Constant(-dt*v0), V_rea)
    fa.assign(X_old2, [s_init, v_init, v_init, v_init, v_init, r_init])

    # Dirichlet boundary conditions
    # w1, u1, phi1, u3, phi3, ...
    #bc1 = fe.DirichletBC(V.sub(0), fe.Constant(0.0), left_bottom_edges)
    bc2 = fe.DirichletBC(V.sub(2).sub(1), fe.Constant(0.0), right_edge)
    bc3 = fe.DirichletBC(V.sub(2).sub(0), fe.Constant(0.0), top_edge)
    bc4 = fe.DirichletBC(V.sub(1).sub(0), fe.Constant(0.0), right_edge)
    bc5 = fe.DirichletBC(V.sub(1).sub(1), fe.Constant(0.0), top_edge)
    bc6 = fe.DirichletBC(V.sub(4).sub(1), fe.Constant(0.0), right_edge)
    bc7 = fe.DirichletBC(V.sub(4).sub(0), fe.Constant(0.0), top_edge)
    bc8 = fe.DirichletBC(V.sub(3).sub(0), fe.Constant(0.0), right_edge)
    bc9 = fe.DirichletBC(V.sub(3).sub(1), fe.Constant(0.0), top_edge)
    bc = [bc2, bc3, bc4, bc5, bc6, bc7, bc8, bc9]

    # File with solution
    file = fe.XDMFFile(folder + "/displ_w.xdmf")
    file.parameters["flush_output"] = True
    file_v = fe.XDMFFile(folder + "/vel_w.xdmf")
    file_v.parameters["flush_output"] = True

    # Forms for matrices K and M
    K_form = get_K_form(V)
    M_form_1, M_form_2 = get_M_form(V, True, True)
    # M_form_2 = get_M_form(V, False)
    # fa = fe.FunctionAssigner(V, [V_sca, V_vec, V_vec, V_vec, V_vec])
    # ones_sca = fe.interpolate(fe.Constant(1.0), V_sca)
    # ones_vec = fe.interpolate(fe.Constant((1.0, 1.0)), V_vec)
    # oness = fe.Function(V)
    # fa.assign(oness, [ones_sca, ones_vec, ones_vec, ones_vec, ones_vec])
    # M_matrix_2 = fe.assemble(fe.action(M_form_2, oness))
    F_form = get_F_form(V)

    # Assembling of consistent matrices
    K_matrix = fe.assemble(K_form)
    [bci.apply(K_matrix) for bci in bc]
    #bc.apply(K_matrix)
    M_matrix = fe.assemble(M_form_1) + fe.assemble(M_form_2)
    # M_matrix_2 = fe.assemble(M_form_2)
    # import sys
    # np.set_printoptions(threshold=sys.maxsize)
    # print("Consistent mass matrix:\n", np.array_str(M_matrix.array(), precision=3))
    # print("Lumped mass matrix:\n", np.array_str(M_matrix_2[:], precision=3))
    [bci.apply(M_matrix) for bci in bc]
    #bc.apply(M_matrix)
    F_vector = fe.assemble(F_form)
    [bci.apply(F_vector) for bci in bc]
    #bc.apply(F_vector)

    M_fce = fe.Function(V)
    M_vect = M_fce.vector()
    M_matrix.get_diagonal(M_vect)

    # Main time loop
    saveindex = saverate
    t = t_start
    begin_t = time.time()
    while t < t_end:
        print("Time instant: " + str(t))

        # Hertz nonlinear force
        F_hertz = 0.25*k_0*mc_bracket(X_old.sub(5)(pp) - X_old.sub(0)(pp))**1.5

        # Update of RHS
        w_vector = -dt**2*K_matrix*X_old.vector() - M_matrix*(X_old2.vector() - 2.0*X_old.vector()) -dt**2*F_hertz*a_loc_vec
        [bci.apply(w_vector) for bci in bc]
        #bc.apply(w_vector)

        iMw = fe.Function(V)
        iMw.vector().set_local(w_vector.get_local()/M_vect.get_local())
        X.assign(iMw)

        if saving:
            if saveindex >= saverate:
                w, u1, phi1, u2, phi2, u_imp = X.split(deepcopy=True)
                file.write(w, t)

                vel.assign((X - X_old)/dt)
                file_v.write(vel, t)

                saveindex = 0
            else:
                saveindex += 1

        X_old2.assign(X_old)
        X_old.assign(X)

        t += dt

    end_t = time.time()
    file.close()
    file_v.close()

    return end_t - begin_t


# Geometry paramaters
L, B = 1.5, 1.5  # Width, Height
H = [0.005, 0.00076, 0.005]  # Thicknesses

# Number of elements
nx, ny = 100, 100

# Degree orders
deg = 1  # Order of elements
deg_shear = 0  # Order of shear elements

# Mesh
#mesh = fe.BoxMesh.create([fe.Point(0.0, 0.0, 0.0), fe.Point(L, B, H)], [nx, ny, nz],fe.CellType.Type.hexahedron)
mesh = fe.RectangleMesh(fe.Point(0.0, 0.0), fe.Point(0.5*L, 0.5*B), nx, ny)

# Foil
pvb_g = 1.0e3*np.array([1782124.2, 519208.7, 546176.8, 216893.2, 13618.3, 4988.3, 1663.8, 587.2, 258.0, 63.8, 168.4])
pvb_thetas = np.array([1.0e-5, 1.0e-4, 1.0e-3, 1.0e-2, 1.0e-1, 1.0, 1.0e1, 1.0e2, 1.0e3, 1.0e4, 1.0e5])
pvb_c1 = 8.635
pvb_c2 = 42.422
pvb_tref = 20.0
pvb_ginf = 232.26e3
nu_foil = 0.49

G_foil = get_G(0.03, 20.0)
E_foil = 2.0*G_foil*(1+nu_foil)
print(G_foil, E_foil)

# Material parameters
E = [72.0e9, E_foil, 72.0e9]  # Young's module
nu = [0.22, nu_foil, 0.22]  # Poisson ratio
rho = [2500.0, 1100.0, 2500.0]   # Density
k = [5.0/6.0, 1.0, 5.0/6.0]
#mu, lmbda = 0.5*E/(1+nu), E*nu/(1+nu)/(1-2*nu)  # Lame's coefficients

# Contact parameters
E_imp = 210.0e9  # Young's modulus of impactor
nu_imp = 0.3  # Poisson's ratio of impactor
R = 0.05  # Radius of impactor
k_0 = 4.0/3.0*math.sqrt(R)/((1.0-nu[0]**2)/E[0]+(1.0-nu_imp**2)/E_imp)  # stiffness of contact
m_imp = 48.7  # Impactor mass
h0 = 0.3  # Initial height of impactor
v0 = math.sqrt(2*10*h0)  # Initial velocity of impactor
vol = L*B  # Area of mesh
rho_imp = m_imp/vol  # Impactor weight distributed across whole mesh
pp = fe.Point(0.5*L, 0.5*B)  # Point of impact

# Time parameters
t_start = 0.0
#t_end = 0.03
t_end = 3.0e-2
dt = 5.0e-7

#u_D = fe.Expression(" t < 1.0e-4 ? -10*t : -1.0e-3", t=0.0, degree=0)
f_ext = fe.Constant(1.0)

time = explicit_solver("Solution_plate/plate_test_3L_nodamage_hertz_2", True, 100)

print(time)
