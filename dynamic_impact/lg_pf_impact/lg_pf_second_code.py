# -------------------------
# lg_pf_first_code.py
# -------------------------

# -------------------------
# Description:
# - Impact of steel impactor into large laminated glass plate
# - Implemented with damage via phasefield fracture model
# - Contact by Hertz law
# - Implemented in FEniCS
# - Explicit integration scheme
#
# Last edit: 10.11. 2022
# -------------------------

import fenics as fe
import matplotlib.pyplot as plt
import math
import numpy as np
import time
import os
import lumping_scheme_quad

from petsc4py import PETSc
from mshr import Polygon, generate_mesh


# --------------------
# Functions and classes
# --------------------

def left(x, on_boundary):
    return on_boundary and x[0] < 0.0001


# Elastic strain tensor
def eps(v):
    return fe.sym(fe.grad(v))


# Positive part operator
def mc_bracket(x):
    return 0.5*(x+abs(x))


# Maximum of two function with UFL
def max_fce(a, b):
    return 0.5*(a+b+abs(a-b))


def map_to_layers_test(fce_i, h_i, dpcopy, lnum):
    # w1, u1, phi1, u3, phi3, ...
    u_i = [None] * lnum
    w_i = [None] * lnum
    phi_i = [None] * lnum

    S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])
    S1 = fe.as_tensor([[0.0, -1.0], [1.0, 0.0]])
    fce_i = fe.split(fce_i)

    for j in range(lnum):
        w_i[j] = fce_i[0]
        if j % 2 == 0:
            u_i[j] = fce_i[1 + j]
            phi_i[j] = fce_i[2 + j]

    for j in range(lnum):
        if j % 2 != 0:
            # 1, 3, ...
            u_i[j] = 0.25 * h_i[j-1] * S * phi_i[j-1] - 0.25 * h_i[j+1] * S * phi_i[j+1] + 0.5 * u_i[j+1] + 0.5 * u_i[j-1]
            phi_i[j] = S1 * (-0.5 * S * phi_i[j-1] * h_i[j-1] / h_i[j] - 0.5 * S * phi_i[j+1] * h_i[j+1] / h_i[j] + \
                       u_i[j+1] / h_i[j] - u_i[j-1] / h_i[j])

    return w_i, u_i, phi_i


# Projection on each element
def local_project(fce, space):
    lp_trial, lp_test = fe.TrialFunction(space), fe.TestFunction(space)
    lp_a = fe.inner(lp_trial, lp_test)*fe.dx
    lp_L = fe.inner(fce, lp_test)*fe.dx
    local_solver = fe.LocalSolver(lp_a, lp_L)
    local_solver.factorize()
    lp_f = fe.Function(space)
    local_solver.solve_local_rhs(lp_f)
    return lp_f


def explicit_solver_plate(folder, mesh_type, material_type, h_i, solver_type, h0, dt):
    def sigma(v):
        return lmbda_1 * fe.tr(eps(v)) * fe.Identity(2) + 2.0 * mu_1 * eps(v)

    def sigma_vd(u_i, d_i):
        # return sigma_p_vd(u_i, lmbda, mu) + sigma_n_vd(u_i, lmbda, mu)
        return ((1 - d_i) ** 2 + k_res) * sigma_p_vd(u_i) + sigma_n_vd(u_i)

    # Positive stress tensor - volumetric-deviatoric
    def sigma_p_vd(u_i):
        kn = lmbda_1 + mu_1
        return kn * mc_bracket(fe.tr(eps(u_i))) * fe.Identity(2) + 2 * mu_1 * fe.dev(eps(u_i))

    # Negative stress tensor - volumetric-deviatoric
    def sigma_n_vd(u_i):
        kn = lmbda_1 + mu_1
        return -kn * mc_bracket(-fe.tr(eps(u_i))) * fe.Identity(2)

    def sigma_sd(u_i, d_i):
        return ((1 - d_i) ** 2 + k_res) * sigma_p_sd(u_i) + sigma_n_sd(u_i)

    # Positive stress tensor - spectral decomposition
    def sigma_p_sd(u_i):
        return lmbda_1 * (fe.tr(eps_p(u_i))) * fe.Identity(2) + 2.0 * mu_1 * (eps_p(u_i))

    # Negative stress tensor - spectral decomposition
    def sigma_n_sd(u_i):
        return lmbda_1 * (fe.tr(eps_n(u_i))) * fe.Identity(2) + 2.0 * mu_1 * (eps_n(u_i))

    # positive strain tensor
    def eps_p(v):
        v00, v01, v10, v11 = eig_v(eps(v))
        v00 = fe.conditional(fe.gt(v00, 0.0), v00, 0.0)
        v11 = fe.conditional(fe.gt(v11, 0.0), v11, 0.0)
        w00, w01, w10, w11 = eig_w(eps(v))
        wp = ([w00, w01], [w10, w11])
        wp = fe.as_tensor(wp)
        vp = ([v00, v01], [v10, v11])
        vp = fe.as_tensor(vp)
        return wp * vp * fe.inv(wp)

    # negative strain tensor
    def eps_n(v):
        v00, v01, v10, v11 = eig_v(eps(v))
        v00 = fe.conditional(fe.lt(v00, 0.0), v00, 0.0)
        v11 = fe.conditional(fe.lt(v11, 0.0), v11, 0.0)
        w00, w01, w10, w11 = eig_w(eps(v))
        wn = ([w00, w01], [w10, w11])
        wn = fe.as_tensor(wn)
        vn = ([v00, v01], [v10, v11])
        vn = fe.as_tensor(vn)
        return wn * vn * fe.inv(wn)

    # eigenvalues for 2x2 matrix
    def eig_v(a):
        v00 = a[0, 0] / 2 + a[1, 1] / 2 - fe.sqrt(
            a[0, 0] ** 2 - 2 * a[0, 0] * a[1, 1] + 4 * a[0, 1] * a[1, 0] + a[1, 1] ** 2) / 2
        v01 = 0.0
        v10 = 0.0
        v11 = a[0, 0] / 2 + a[1, 1] / 2 + fe.sqrt(
            a[0, 0] ** 2 - 2 * a[0, 0] * a[1, 1] + 4 * a[0, 1] * a[1, 0] + a[1, 1] ** 2) / 2
        return v00, v01, v10, v11

    # eigenvectors for 2x2 matrix
    def eig_w(a):
        w00 = -a[0, 1] / (a[0, 0] / 2 - a[1, 1] / 2 + fe.sqrt(
            a[0, 0] ** 2 - 2 * a[0, 0] * a[1, 1] + 4 * a[0, 1] * a[1, 0] + a[1, 1] ** 2) / 2)
        w01 = -a[0, 1] / (a[0, 0] / 2 - a[1, 1] / 2 - fe.sqrt(
            a[0, 0] ** 2 - 2 * a[0, 0] * a[1, 1] + 4 * a[0, 1] * a[1, 0] + a[1, 1] ** 2) / 2)
        w10 = 1.0
        w11 = 1.0
        return w00, w01, w10, w11

    # Positive energy - spectral decomposition
    def psi_sd_plus(u_i):
        eps_sym = eps(u_i)

        eps1 = (1. / 2.) * fe.tr(eps_sym) + fe.sqrt((1. / 4.) * (fe.tr(eps_sym) ** 2) - fe.det(eps_sym))
        eps2 = (1. / 2.) * fe.tr(eps_sym) - fe.sqrt((1. / 4.) * (fe.tr(eps_sym) ** 2) - fe.det(eps_sym))

        tr_eps_plus = mc_bracket(fe.tr(eps_sym))
        sum_eps_plus_squared = mc_bracket(eps1) ** 2 + mc_bracket(eps2) ** 2

        return 0.5 * lmbda_1 * tr_eps_plus ** 2 + mu_1 * sum_eps_plus_squared

    # Edge for boundary condition
    def right_edge(x, on_boundary):
        return on_boundary and fe.near(x[0], 0.5 * l_x)

    # Edge for boundary condition
    def top_edge(x, on_boundary):
        return on_boundary and fe.near(x[1], 0.5 * l_y)

    def create_log_file(folder, p_type, model_type, mesh):
        directory = os.path.dirname(folder + "/")
        if not os.path.exists(directory):
            os.makedirs(directory)
        f = open(folder + "/log.txt", "w")
        f.write("Model type: " + str(model_type) + "\n")
        f.write("Number of elements: " + str((n_x, n_y)) + "\n")
        #f.write("Material parameters: E=" + str(E_1) + ", nu=" + str(nu_1) + ", rho=" + str(rho_1) + ", ft=" +
        #        str(ft_glass) + ", lc=" + str(lc) + ", Gc=" + str(Gc_glass) + "\n")
        f.write("Material parameters: E=" + str(E_1) + ", nu=" + str(nu_1) + ", rho=" + str(rho_1) + "\n")
        for i in range(glass_num):
            f.write("Material parameters - glass " + str(i) + ", ft=" + str(ft_glass[i]) + ", lc=" + str(lc) +
                    ", Gc=" + str(Gc_glass[i]) + "\n")
        f.write("Impactor: m=" + str(m_imp) + ", E=" + str(E_2) + ", nu=" + str(nu_2) + "\n")
        f.write("Impact height: h0=" + str(h0) + "\n")
        f.write("Elements: " + str(mesh.ufl_cell()) + "-" + str(deg) + "\n")
        f.write("Point-load type: " + p_type + "\n")
        f.write("Model parameters: n_ni=" + str(40) + ", k_res=" + str(k_res) + ", xi_shear=" + str(xi_shear) +
                ", k_shear=" + str(k_shear) + "\n")
        #directory = os.path.dirname(folder + "/")
        #if not os.path.exists(directory):
        #    os.makedirs(directory)
        #f = open(folder + "/log.txt", "w")
        #f.write("Number of elements: " + str((nx, ny)) + "\n")
        #f.write("Material parameters: E=" + str(E) + ", nu=" + str(nu) + ", rho=" + str(rho) + ", ft=" + str(ft_glass) +
        #        ", lc=" + str(lc) + ", Gc=" + str(Gc_glass) + "\n")
        #f.write("Impactor: m=" + str(m_imp) + ", E=" + str(E_imp) + ", nu=" + str(nu_imp) + "\n")
        #f.write("Impact height: h0=" + str(h0) + "\n")
        #f.write("Elements: " + str(mesh.ufl_cell()) + "-" + str(deg) + "\n")
        #f.write("Point-load type: " + p_type + "\n")
        #f.write("Model parameters: n_ni=" + str(n_ni) + ", k_res=" + str(k_res) + ", xi_shear=" + str(xi_shear) +
        #        ", k_shear=" + str(k_shear) + "\n")

    def get_K_form_vector(V, u, d, dec_type):
        dx_shear = fe.dx(metadata={"quadrature_degree": 0})

        #w_tr, u_tr, phi_tr, imp_tr = fe.split(u)
        # w_tr, u_tr, phi_tr, imp_tr = fe.TrialFunctions(V)
        #w_test, u_test, phi_test, imp_test = fe.TestFunctions(V)

        #lg_trial = fe.TrialFunction(V)
        lg_test = fe.TestFunction(V)
        S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

        w_tr, u_tr, phi_tr = map_to_layers_test(u, h_i, False, len(h_i))
        w_test, u_test, phi_test = map_to_layers_test(lg_test, h_i, False, len(h_i))

        K_form = 0.0
        ii = 0

        for i in range(n_lay):
            if i % 2 == 0:
                print("Glass")
                num_int = np.linspace(-0.5 * h_i[i], 0.5 * h_i[i], 40)
                dh = abs(num_int[1] - num_int[0])

                for j in range(len(num_int) - 1):
                    gauss = 0.5 * (num_int[j] + num_int[j + 1])
                    print(u_tr[i])
                    u_z = u_tr[i] + S * phi_tr[i] * gauss
                    u_z_test = u_test[i] + S * phi_test[i] * gauss
                    # K_form += (1-d)**2*fe.inner(sigma(u_z), eps(u_z_test))*dh*fe.dx
                    if dec_type == "hyb_sd":
                        K_form += (1 - d[ii]) ** 2 * fe.inner(sigma(u_z), eps(u_z_test)) * dh * fe.dx
                    elif dec_type == "vol":
                        print("hej")
                        K_form += fe.inner(sigma_vd(u_z, d[ii]), eps(u_z_test)) * dh * fe.dx
                    elif dec_type == "sd":
                        K_form += fe.inner(sigma_sd(u_z, d[ii]), eps(u_z_test)) * dh * fe.dx
                    else:
                        raise Exception("Only hyb_sd/vol/sd decomposition implemented!")

                D2 = (E[i] * k[i] * h_i[i]) / (2.0 * (1.0 + nu[i]))

                if xi_shear:
                    K_form += D2 * fe.inner(fe.grad(w_tr[i]) + S * phi_tr[i], fe.grad(w_test[i]) + S * phi_test[i]) * dx_shear
                else:
                    print("hej2")
                    K_form += ((1 - d[ii]) ** 2 + k_shear) * D2 * fe.inner(fe.grad(w_tr[i]) + S * phi_tr[i],
                                                                       fe.grad(w_test[i]) + S * phi_test[i]) * dx_shear

                ii += 1
            else:
                D1 = (E[i] * h_i[i] ** 3) / (12.0 * (1.0 - nu[i] ** 2))
                D2 = (E[i] * k[i] * h_i[i]) / (2.0 * (1.0 + nu[i]))
                D3 = (E[i] * h_i[i]) / (1.0 - nu[i] ** 2)
                kappa_tr, kappa_test = eps(S * phi_tr[i]), eps(S * phi_test[i])
                du_tr, du_test = eps(u_tr[i]), eps(u_test[i])
                K_form += fe.inner(D1 * (nu[i] * fe.div(S * phi_tr[i]) * fe.Identity(2) + (1.0 - nu[i]) * kappa_tr),
                                   kappa_test) * fe.dx
                K_form += fe.inner(D3 * (nu[i] * fe.div(u_tr[i]) * fe.Identity(2) + (1.0 - nu[i]) * du_tr), du_test) * fe.dx
                K_form += D2 * fe.inner(fe.grad(w_tr[i]) + S * phi_tr[i], fe.grad(w_test[i]) + S * phi_test[i]) * dx_shear

        return K_form

    def get_M_form(V, lump, podvod):
        if lump:
            if el_type == "rect":
                dx_m = fe.dx(scheme="lumped", degree=2)
            elif el_type == "tri" or el_type == "tri_un" or el_type == "tri_m2" or el_type == "tri_ref":
                dx_m = fe.dx(scheme="vertex", metadata={"degree": 1, "representation": "quadrature"})
        else:
            dx_m = fe.dx

        lg_trial = fe.TrialFunction(V)
        lg_test = fe.TestFunction(V)
        S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

        w_tr, u_tr, phi_tr = map_to_layers_test(lg_trial, h_i, False, len(h_i))
        w_test, u_test, phi_test = map_to_layers_test(lg_test, h_i, False, len(h_i))

        M_form = 0.0

        for i in range(n_lay):
            if i % 2 == 0:
                M_form += rho[i] * h_i[i] ** 3 / 12.0 * fe.dot(S * phi_tr[i], S * phi_test[i]) * dx_m
                M_form += rho[i] * h_i[i] * fe.dot(w_tr[i], w_test[i]) * dx_m
                M_form += rho[i] * h_i[i] * fe.dot(u_tr[i], u_test[i]) * dx_m
            else:
                if not podvod:
                    M_form += rho[i] * h_i[i] ** 3 / 12.0 * fe.dot(S * phi_tr[i], S * phi_test[i]) * dx_m
                    M_form += rho[i] * h_i[i] * fe.dot(w_tr[i], w_test[i]) * dx_m
                    M_form += rho[i] * h_i[i] * fe.dot(u_tr[i], u_test[i]) * dx_m

        l_tr = fe.TrialFunctions(V)
        l_test = fe.TestFunctions(V)

        M_form_2 = rho_imp_2d * fe.dot(l_tr[-1], l_test[-1]) * fe.dx

        return M_form, M_form_2
    # Get foil stiffness for given time t_i
    def get_G(t_i, t_act):
        a_t = math.pow(10.0, -c1 * (t_act - Tref) / (c2 + t_act - Tref))
        temp = Ginf
        n = len(Gs)
        temp += sum(Gs[k] * math.exp(-t_i / (a_t * thetas[k])) for k in range(0, n))
        return temp

    # Calculate new lambda and mu for foil
    def update_foil(t_i):
        G_foil_t = get_G(t_i, Tact)
        G_foil.g = G_foil_t

    # Return formulation for damage
    def get_d_form(W, u, d, type_d, hist_var):
        d_form = []
        for j in range(len(d)):
            d_test = fe.TestFunction(W)
            d_tr = fe.TrialFunction(W)

            en = get_positive_energy(u, j)
        #d_test = fe.TestFunction(W)
        #d_tr = fe.TrialFunction(W)
        # w_tr, u_tr, phi_tr, imp_tr = fe.split(u)
        # S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

        # Positive energy
        # u_z_l = u_tr + S*phi_tr*0.5*H
        # u_z_u = u_tr - S*phi_tr*0.5*H
        # en = max_fce(psi_sd_plus(u_z_l)*H, psi_sd_plus(u_z_u)*H)

        #en = get_positive_energy(u)

            if type_d == "bourdin":
                # max_en = max_fce(en, hist_var)
                # hist_var_fce = fe.project(max_en, W0)
                # hist_var
                # en = max_fce(en, hist)
                en = hist_var

            d_form.append(0.0)
            lay_ind = j*2
            if type_d == "pham":
                d_form[j] += -2 * en * fe.inner(1.0 - d[j], d_test) * fe.dx
                d_form[j] += h_i[lay_ind] * 3.0 / 8.0 * Gc_glass[j] * (
                            1.0 / lc * d_test + 2 * lc * fe.inner(fe.grad(d[j]), fe.grad(d_test))) * fe.dx
            elif type_d == "bourdin":
                d_form[j] += -2 * en * fe.inner(1.0 - d_tr, d_test) * fe.dx
                d_form[j] += h_i[lay_ind] * Gc_glass[j] * (
                            1.0 / lc * fe.inner(d_tr, d_test) + lc * fe.inner(fe.grad(d_tr), fe.grad(d_test))) * fe.dx

        return d_form

    def get_positive_energy(u, x_ind):
        phi_ind = (x_ind+1)*2
        u_ind = phi_ind - 1
        h_ind = x_ind*2
        x_tr = fe.split(u)
        u_tr = x_tr[u_ind]
        phi_tr = x_tr[phi_ind]
        S = fe.as_tensor([[0.0, 1.0], [-1.0, 0.0]])

        # Positive energy
        u_z_l = u_tr + S * phi_tr * 0.5 * h_i[h_ind]
        u_z_u = u_tr - S * phi_tr * 0.5 * h_i[h_ind]
        return max_fce(psi_sd_plus(u_z_l) * h_i[h_ind], psi_sd_plus(u_z_u) * h_i[h_ind])

    def prepare_solver_d(hist_i):
        solver = []
        for j in range(len(d)):
            lower = d[j]
            # lower = hist_i
            # hist
            # threshold = fe.interpolate(fe.Constant(0.9), W)
            # lower = fe.conditional(fe.lt(d, threshold), 0.0, d)
            upper = fe.interpolate(fe.Constant(1.0), W)

            # Solution of damage formulation
            H = fe.derivative(d_form[j], d[j], fe.TrialFunction(W))

            problem = fe.NonlinearVariationalProblem(d_form[j], d[j], [], H)
            problem.set_bounds(lower, upper)

            solver.append(fe.NonlinearVariationalSolver(problem))
            solver[j].parameters.update(snes_solver_parameters)
        return solver

    # Damage snes variational inequalities solver based on Newton's method
    def solve_damage(d_type, solver):
        for j in range(len(d)):
            if d_type == "pham":
                lower = d[j]
                upper = fe.interpolate(fe.Constant(1.0), W)

                # Solution of damage formulation
                H = fe.derivative(d_form[j], d[j], fe.TrialFunction(W))

                problem = fe.NonlinearVariationalProblem(d_form[j], d[j], [], H)
                problem.set_bounds(lower, upper)

                solver = fe.NonlinearVariationalSolver(problem)
                solver.parameters.update(snes_solver_parameters)
                solver.solve()
            elif d_type == "pham2":
                solver[j].solve()
                #lowerr = fe.conditional(fe.lt(d, threshold), 0.0, d)
                #hist.assign(local_project(lowerr, W0))
            elif d_type == "bourdin":
                fe.solve(fe.lhs(d_form[j]) == fe.rhs(d_form[j]), d[j], [])
                hist[j].assign(local_project(max_fce(hist[j], get_positive_energy(X, j)), W0))

    # --------------------
    # Parameters
    # --------------------
    n_x, n_y, el_type = mesh_type[0], mesh_type[1], mesh_type[2]  # Number of elements
    l_x, l_y = 0.5, 0.5  # Dimensions of sample
    E_1 = 70.0e9  # Young's modulus of glass
    nu_1 = 0.22  # Poisson's ratio of glass
    rho_1 = 2500.0  # Glass density
    rho_2 = 1100.0  # Density of foil
    #h = [0.005, 0.00228, 0.006, 0.00076, 0.005]  # Thicknesses
    # h = [0.005, 0.00152, 0.008, 0.00076, 0.008, 0.00076, 0.005]
    h_tot = sum(h_i)  # Total thickness of lg beam
    n_lay = len(h_i)  # Number of layers
    glass_num = int((len(h_i) + 1) / 2)  # Number of glass layers

    Gs = 1.0e3 * np.array([1782124.2, 519208.7, 546176.8, 216893.2, 13618.3, 4988.3, 1663.8, 587.2, 258.0, 63.8, 168.4])
    thetas = np.array([1.0e-5, 1.0e-4, 1.0e-3, 1.0e-2, 1.0e-1, 1.0, 1.0e1, 1.0e2, 1.0e3, 1.0e4, 1.0e5])
    nu_vis = 0.49
    Ginf = 232.26e3
    c1 = 8.635
    c2 = 42.422
    Tref = 20.0
    Tact = 25.0

    # G_foil = get_G(0.003, 20.0)
    G_foil = fe.Expression("g", g=0.0, degree=0)  # G value
    E_foil = 2.0 * G_foil * (1 + nu_vis)

    # Material parameters
    E = (glass_num - 1) * [E_1, E_foil] + [E_1]  # Young's module
    print(E)
    nu = (glass_num - 1) * [nu_1, nu_vis] + [nu_1]  # Poisson ratio
    rho = (glass_num - 1) * [rho_1, rho_2] + [rho_1]  # Density
    k = (glass_num - 1) * [5.0 / 6.0, 1.0] + [5.0 / 6.0]

    E_2 = 210.0e9  # Young's modulus of impactor
    nu_2 = 0.3  # Poisson's ratio of impactor
    R = 0.05  # Radius of impactor
    # F = 1.0
    m_imp = 48.2  # Impactor mass
    #h0 = 0.05  # initial height of impactor
    v0 = math.sqrt(2 * 9.81 * h0)
    sur = l_x * l_y  # Area of mesh
    rho_imp_2d = m_imp / sur  # Impactor weight distributed across whole 2d mesh

    ft_glass = material_type
    #ft_glass = 55.0e6  # Strengths of glass
    # Fracture parameters
    h_min = l_x / n_x
    # lc = h_min
    #lc = 0.010606601717798144
    #print("lc=", lc)
    #Gc_glass = lc * 8.0 / 3.0 * ft_glass ** 2 / E_1
    #print("Gc_glass=", Gc_glass)
    k_res = solver_type[0]
    xi_shear = solver_type[1]
    #k_shear = 0.05
    k_shear = solver_type[2]
    stoch = False
    k_shear_test = True

    # k_shear_atyp = fe.Expression(math.sqrt((f.midpoint()[0]-0.5*l_x)**2+(f.midpoint()[1]-0.5*l_y)**2)) < 0.05*l_x)
    class k_shear_atyp(fe.UserExpression):
        def __init__(self, rs, **kwargs):
            super().__init__(**kwargs)
            self.rs = rs

        def eval(self, value, x):
            value[0] = 0.0
            if math.sqrt((x[0]-0.5*l_x)**2+(x[1]-0.5*l_y)**2) < self.rs:
                value[0] = k_shear
            else:
                value[0] = 0.0

        def value_shape(self):
            return (2,)

    # Degree orders
    deg = 1  # Order of elements
    deg_3d = 2
    deg_shear = 0  # Order of shear elements
    #el_type = "rect"  # Type of elements

    # Time parameters
    # dt = 2.5e-5  # time increment
    #dt = 5.0e-7
    t_start = 0.0  # start time
    # t_end = 4.0e-3  # end time
    t_end = mesh_type[3]

    save_rate = solver_type[4]
    max_iter = 30
    tol = 1.0e-4
    vis = True

    mu_1 = E_1 / 2 / (1 + nu_1)  # Lame's constants
    lmbda_1 = E_1 * nu_1 / (1 + nu_1) / (1 - 2 * nu_1)

    k_0 = 4.0 / 3.0 * math.sqrt(R) / ((1.0 - nu_1 ** 2) / E_1 + (1.0 - nu_2 ** 2) / E_2)  # stiffness of contact

    # Important points
    p_bottom = fe.Point(0.5 * l_x, 0.0)
    p_side = fe.Point(0.0, 0.5 * l_y)
    p_corner = fe.Point(0.0, 0.0)
    p_quarter = fe.Point(0.25 * l_x, 0.0)

    snes_solver_parameters = {"nonlinear_solver": "snes",
                              "snes_solver": {"method": "vinewtonssls",
                                              "line_search": "basic",
                                              "linear_solver": "mumps",
                                              "maximum_iterations": 150,
                                              "relative_tolerance": 1e-3,
                                              "absolute_tolerance": 1e-4,
                                              "report": True,
                                              "error_on_nonconvergence": False}}

    # --------------------
    # Geometry
    # --------------------
    # Mesh
    #elem_type = fe.CellType.Type.quadrilateral
    if el_type == "rect":
        elem_type = fe.CellType.Type.quadrilateral
        mesh = fe.RectangleMesh.create([fe.Point(0.0, 0.0), fe.Point(0.5 * l_x, 0.5 * l_y)], [n_x, n_y], elem_type)
    elif el_type == "tri":
        elem_type = fe.CellType.Type.triangle
        mesh = fe.RectangleMesh.create([fe.Point(0.0, 0.0), fe.Point(0.5 * l_x, 0.5 * l_y)], [n_x, n_y], elem_type)
    elif el_type == "tri_m2":
        elem_type = fe.CellType.Type.triangle
        mesh = fe.RectangleMesh.create([fe.Point(0.0, 0.0), fe.Point(0.5 * l_x, 0.5 * l_y)], [n_x, n_y], elem_type, "left")
    elif el_type == "tri_un":
        domain = Polygon([fe.Point(0.0, 0.0), fe.Point(0.5*l_x, 0.0), fe.Point(0.5*l_x, 0.5*l_y), fe.Point(0.0, 0.5*l_y)])
        mesh = generate_mesh(domain, 0.7 * n_x)
    elif el_type == "tri_ref":
        domain = Polygon([fe.Point(0.0, 0.0), fe.Point(0.5 * l_x, 0.0), fe.Point(0.5 * l_x, 0.5 * l_y), fe.Point(0.0, 0.5 * l_y)])
        mesh = generate_mesh(domain, 0.7 * n_x)
        # Refine
        markers = fe.MeshFunction("bool", mesh, 2)
        markers.set_all(False)
        for c in fe.cells(mesh):
            # Mark cells with facet midpoints near
            for f in fe.facets(c):
                if (math.sqrt((f.midpoint()[0]-0.5*l_x)**2+(f.midpoint()[1]-0.5*l_y)**2)) < 0.1*l_x:
                    markers[c] = True
        mesh = fe.refine(mesh, markers, redistribute=False)
        # Refine
        markers = fe.MeshFunction("bool", mesh, 2)
        markers.set_all(False)
        for c in fe.cells(mesh):
            # Mark cells with facet midpoints near
            for f in fe.facets(c):
                if (math.sqrt((f.midpoint()[0]-0.5*l_x)**2+(f.midpoint()[1]-0.5*l_y)**2)) < 0.05*l_x:
                    markers[c] = True
        mesh = fe.refine(mesh, markers, redistribute=False)

    elif el_type == "test":
        domain = Polygon([fe.Point(0.0, 0.0), fe.Point(0.5*l_x, 0.0), fe.Point(0.5*l_x, 0.5*l_y), fe.Point(0.0, 0.5*l_y)])
        mesh = generate_mesh(domain, 0.7 * n_x)
        #xs = mesh.coordinates()
        #print(xs[0])
        #xs[:] = (xs/0.75)**2
    # mesh = fe.RectangleMesh.create([fe.Point(0.0, 0.0), fe.Point(0.5 * l_x, 0.5 * l_y)], [n_x, n_y], elem_type)
    # mesh = fe.BoxMesh.create([fe.Point(0.0, 0.0, 0.0), fe.Point(L, B, H)], [nx, ny, nz],fe.CellType.Type.hexahedron)
    # mesh = fe.RectangleMesh(fe.Point(0.0, 0.0), fe.Point(0.5*L, 0.5*B), nx, ny, "left/right")
    # mesh_2d = fe.RectangleMesh(fe.Point(0.0, 0.0), fe.Point(0.5 * l_x, 0.5 * l_y), n_x, n_y, "right")

    fe.plot(mesh)
    plt.show()

    # mesh plot
    # fe.plot(mesh_2d)
    # plt.show()

    # --------------------
    # Define spaces
    # --------------------
    p1_element = fe.FiniteElement("P", mesh.ufl_cell(), deg)
    v1_element = fe.VectorElement("P", mesh.ufl_cell(), deg)
    r1_element = fe.FiniteElement("R", mesh.ufl_cell(), 0)
    v1_elems_num = glass_num*2
    element = fe.MixedElement([p1_element] + v1_elems_num*[v1_element] + [r1_element])
    V = fe.FunctionSpace(mesh, element)
    V_sca = fe.FunctionSpace(mesh, p1_element)
    V_vec = fe.FunctionSpace(mesh, v1_element)
    V_rea = fe.FunctionSpace(mesh, r1_element)
    V_mix = [V_sca] + v1_elems_num*[V_vec] + [V_rea]
    W = fe.FunctionSpace(mesh, "CG", 1)
    W0 = fe.FunctionSpace(mesh, "CG", 1)

    if k_shear_test == True:
        k_shear = fe.Expression(" (x[0]-0.5*l_x)*(x[0]-0.5*l_x)+(x[1]-0.5*l_y)*(x[1]-0.5*l_y) < rs*rs ? k_shear : 0.0", l_x=l_x, l_y=l_y, rs=0.015*l_x, k_shear=k_shear, degree=1)
    #f_test = fe.Function(W)
    #f_test.interpolate(u_f)
    #aaaa = fe.plot(f_test)
    #plt.colorbar(aaaa)
    #plt.show()
    #k_test = k_shear_atyp(0.1*l_x)
    #fe.plot(k_test)
    #plt.show()
    #file_test = fe.XDMFFile(folder + "/test_fce.xdmf")
    #f_test = fe.Function(W)
    #file_test.write(f_test.interpolate(k_test), 0)
    #file_test.close()

    h_min = mesh.hmin()
    lc = 1.0 * h_min
    # lc = 0.010606601717798144
    #ft_field = fe.Function(W)
    ft_field = [fe.Function(W) for i in range(glass_num)]
    Gc_glass = []
    for kk in range(glass_num):
        if stoch == True:
            ft_field[kk].vector()[:] = ft_glass[0] * np.ones(len(ft_field[kk].vector().get_local())) + 50000000 \
                                   * np.random.rand(len(ft_field[kk].vector().get_local()))
            Gc_glass.append(lc * 8.0 / 3.0 * ft_field[kk] ** 2 / E_1)
        else:
            Gc_glass.append(lc * 8.0 / 3.0 * ft_glass[kk] ** 2 / E_1)
        # Gc_glass = lc * 8.0 / 3.0 * ft_glass ** 2 / E_1
        # print("Gc_glass=", Gc_glass)

    print(ft_field[0].vector().get_local())
    plott = fe.plot(ft_field[0])
    plt.colorbar(plott)
    plt.show()

    print("Number of unknowns: " + str(V.dim()))
    impactor = []
    impactor_acc = []
    tt = []
    u_bottom = []
    u_side = []
    u_corner = []
    u_quarter = []


    # Displacement functions
    X = fe.Function(V)  # Displacement in time t_i
    X_old = fe.Function(V)  # Displacement in time t_{i-1}
    X_old2 = fe.Function(V)  # Displacement in time t_{i-2}
    # Damage functions
    d = [fe.Function(W) for i in range(glass_num)]  # Damage function
    hist = [fe.Function(W0) for i in range(glass_num)]  # History function

    # Initial velocity
    fa = fe.FunctionAssigner(V, V_mix)
    s_init = fe.interpolate(fe.Constant(0.0), V_sca)
    v_init = fe.interpolate(fe.Constant((0.0, 0.0)), V_vec)
    r_init = fe.interpolate(fe.Constant(-dt*v0), V_rea)
    fa.assign(X_old2, [s_init] + v1_elems_num*[v_init] + [r_init])

    # Indices of points
    ind_bottom = find_dof(p_bottom, 0, V)
    ind_side = find_dof(p_side, 0, V)
    ind_corner = find_dof(p_corner, 0, V)
    ind_quarter = find_dof(p_quarter, 0, V)

    # Point distribution
    f_point = fe.Expression("abs(x[0] - 0.5*L) < tol & abs(x[1] - 0.5*B) < tol ? 1.0 : 0.0", tol=0.001, L=l_x,
                            B=l_y,
                            degree=1)
    f_fce_vect = fe.interpolate(f_point, V_sca)
    f_fce_real = fe.interpolate(fe.Constant(1 / (0.25 * l_x * l_y)), V_rea)

    # fe.plot(f_fce_vect)
    file_hit = fe.File(folder + "/hit_fce.pvd")
    file_hit << f_fce_vect
    # plt.show()

    w_test = fe.TestFunctions(V)

    f_dest_form = fe.dot(f_fce_vect, w_test[0]) * fe.dx
    f_dest_form_2 = fe.dot(f_fce_real, w_test[-1]) * fe.dx

    a_loc_vec_2 = fe.assemble(f_dest_form)
    a_loc_vec_2[:] = -a_loc_vec_2[:] / sum(a_loc_vec_2[:])
    a_loc_real = fe.assemble(f_dest_form_2)
    index_real = np.nonzero(a_loc_real[:])[0]
    index_hit = np.argmin(a_loc_vec_2[:])

    a_loc_vec = a_loc_vec_2 + a_loc_real

    # Dirichlet boundary conditions
    # w1, u1, phi1, u3, phi3, ..., r
    bc = []
    for i in range(glass_num):
        ind1 = i * 2 + 1
        ind2 = i * 2 + 2
        bc.append(fe.DirichletBC(V.sub(ind2).sub(1), fe.Constant(0.0), right_edge))
        bc.append(fe.DirichletBC(V.sub(ind2).sub(0), fe.Constant(0.0), top_edge))
        bc.append(fe.DirichletBC(V.sub(ind1).sub(0), fe.Constant(0.0), right_edge))
        bc.append(fe.DirichletBC(V.sub(ind1).sub(1), fe.Constant(0.0), top_edge))

    # File with solution
    file_w = fe.XDMFFile(folder + "/displ_w.xdmf")
    file_w.parameters["flush_output"] = True
    file_d = []
    for i in range(glass_num):
        file_d.append(fe.XDMFFile(folder + "/damage_d_{}.xdmf".format(i)))
        file_d[i].parameters["flush_output"] = True
    #file_u = fe.XDMFFile(folder + "/displ_u.xdmf")
    #file_u.parameters["flush_output"] = True

    # Forms for matrices K and M
    #K_form_vector = get_K_form_vector(V, X, d, "vol")
    K_form_vector = get_K_form_vector(V, X, d, solver_type[3])
    M_form_1, M_form_2 = get_M_form(V, True, "plate")

    # Assembling of matrices
    K_vector = fe.assemble(K_form_vector)
    [bci.apply(K_vector) for bci in bc]
    M_matrix = fe.assemble(M_form_1) + fe.assemble(M_form_2)
    [bci.apply(M_matrix) for bci in bc]

    M_fce = fe.Function(V)
    M_vect = M_fce.vector()
    M_matrix.get_diagonal(M_vect)

    d_form = get_d_form(W, X, d, "pham", hist)
    solver_d = prepare_solver_d(hist)

    # Main time loop

    saveindex = save_rate
    t = t_start
    begin_t = time.time()
    create_log_file(folder, "dist", "plate", mesh)
    while t < t_end:
        print("Time instant: " + str(t))

        if vis:
            # Update foil value
            update_foil(t)
            #K_matrix = fe.assemble(K_form)
            #[bci.apply(K_matrix) for bci in bc]
            #K_form_vector = get_K_form_vector(V, X, d, solver_type[3])
            K_vector = fe.assemble(K_form_vector)
            [bci.apply(K_vector) for bci in bc]

        # K_matrix = fe.assemble(K_form)
        # [bci.apply(K_matrix) for bci in bc]

        # Hertz nonlinear force
        # F_hertz = 0.25*k_0*mc_bracket(X_old.sub(3)(pp) - X_old.sub(0)(pp))**1.5
        F_hertz = np.asscalar(0.25 * k_0 * mc_bracket(X_old.vector()[index_real] - X_old.vector()[index_hit]) ** 1.5)
        # F_hertz = 0

        # Update of RHS
        #w_vector = -dt ** 2 * K_matrix*X_old.vector() - M_matrix * (
        #            X_old2.vector() - 2.0 * X_old.vector()) - dt ** 2 * F_hertz * a_loc_vec
        w_vector = -dt ** 2 * K_vector - M_matrix * (
                X_old2.vector() - 2.0 * X_old.vector()) - dt ** 2 * F_hertz * a_loc_vec
        # w_vector = -dt**2*K_matrix*X_old.vector() - M_matrix*(X_old2.vector() - 2.0*X_old.vector()) -dt**2*F_hertz*a_loc_vec
        [bci.apply(w_vector) for bci in bc]

        iMw = fe.Function(V)
        iMw.vector().set_local(w_vector.get_local() / M_vect.get_local())
        X.assign(iMw)

        solve_damage("pham2", solver_d)

        # ?
        #K_vector = fe.assemble(K_form_vector)
        #[bci.apply(K_vector) for bci in bc]

        if saveindex >= save_rate != -1:
            w_fce = X.split(deepcopy=True)
            file_w.write(w_fce[0], t)
            #file_d.write(d, t)
            for i in range(glass_num):
                file_d[i].write(d[i], t)

            # impactor.append(u_imp(pp))
            impactor.append(X.vector()[index_real])
            temp_acc = X.vector()[index_real] - 2*X_old.vector()[index_real] + X_old2.vector()[index_real]
            temp_acc /= dt**2
            impactor_acc.append(temp_acc)
            tt.append(t)
            u_bottom.append(X.vector()[ind_bottom])
            u_side.append(X.vector()[ind_side])
            u_corner.append(X.vector()[ind_corner])
            u_quarter.append(X.vector()[ind_quarter])

            saveindex = 0
        else:
            saveindex += 1

        X_old2.assign(X_old)
        X_old.assign(X)

        t += dt

    end_t = time.time()
    file_w.close()

    np.savetxt(folder + "/impactor.txt", np.column_stack([tt, impactor, impactor_acc, u_bottom, u_side, u_corner, u_quarter]))

    return end_t - begin_t, impactor, impactor_acc, tt


def find_dof(p, d, VS):
    v_dofs = VS.tabulate_dof_coordinates()
    v0_dofs = VS.sub(d).dofmap().dofs()
    for i in range(0, len(v0_dofs)):
        v_x = v_dofs[v0_dofs[i], 0]
        v_y = v_dofs[v0_dofs[i], 1]
        if fe.near(v_x, p.x()) and fe.near(v_y, p.y()):
            return v0_dofs[i]
    return -1


# --------------------
# Solution
# --------------------

# Thicknesses
h_5 = [0.005, 0.00228, 0.006, 0.00076, 0.005]
h_7 = [0.005, 0.00152, 0.008, 0.00076, 0.008, 0.00076, 0.005]

# Time resolution sensitivity
ftlg_un = 18.0e6
ftlg = [165.0e6, 165.0e6, 155.0e6]
# explicit_solver(file, [nx, ny, element_type, end_time], [strengths], [heights], [k_res, xi_shear, k_shear, "dec_type", save_rate], h0, dt)
#time1, i1, ii1, t1 = explicit_solver_plate("solutions/small/5l_5cm_plate_explicit_vis", [20, 20, "rect"], h_5, 0.05)
#time1, i1, ii1, t1 = explicit_solver_plate("solutions/test_kshare_2_lower_010", [50, 50, "tri_ref", 3.0e-3],
#                                           ftlg, h_5, [0.0, False, 0.05, "vol", 100], 0.1, 1.0e-7)
time1, i1, ii1, t1 = explicit_solver_plate("solutions/test_kshare_2_lower_010_2", [50, 50, "tri_ref", 3.0e-3],
                                           ftlg, h_5, [0.0, False, 0.05, "vol", 100], 0.3, 1.0e-7)
#time1, i1, ii1, t1 = explicit_solver_plate("solutions/small/7l_5cm_plate_explicit_vis", [20, 20, "rect"], h_7, 0.05, 5.0e-7)
#time1, i1, ii1, t1 = explicit_solver_plate("solutions/small/7l_30cm_plate_explicit_vis", [20, 20, "rect"], h_7, 0.3, 5.0e-7)

plt.plot(t1, ii1, label="plate")
plt.xlabel("Time [s]")
plt.ylabel("Impactor acceleration [m]")
plt.legend()
plt.show()
