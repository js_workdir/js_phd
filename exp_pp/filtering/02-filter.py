import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
from scipy.signal import filtfilt
from scipy.signal import iirnotch
from scipy.signal import argrelextrema
from scipy import integrate
import os, fnmatch


def cfc1000_filter(raw_data, dts):
    cfc = 1000.
    omega_d = 2. * math.pi * cfc * 2.0775  # 1.25 * 5./3.
    omega_a = math.tan(omega_d*dts/2.)  # math.sin(omega_d*dts/2.) / math.cos(omega_d*dts/2.)
    # print(omega_d, omega_a)
    a0 = omega_a**2. / (1. + (2.**0.5)*omega_a + omega_a**2.)
    a1 = 2. * a0
    a2 = a0
    b1 = -2. * (omega_a**2. - 1.) / (1. + (2.**0.5)*omega_a + omega_a**2.)
    b2 = (-1. + (2.**0.5*omega_a) - omega_a**2.) / (1. + (2.**0.5*omega_a) + omega_a**2.)
    b = [a0, a1, a2]
    a = [1., -b1, -b2]
    # print(a, b)
    return filtfilt(b, a, raw_data)


def notch_filter(data, f0, fs, Q):
    b_notch, a_notch = iirnotch(f0, Q, fs)
    return filtfilt(b_notch, a_notch, data)


# Parameters
input_folder = 'lv5_lg_in/'
output_folder = 'lv5_lg_out/'


for filename in os.listdir(input_folder):
    file = pd.read_csv(input_folder+filename, sep='\t', skiprows=1, header=None, lineterminator="\r")
    #file = np.loadtxt(input_folder+filename, delimiter="\t", skiprows=1)
    zero = np.argmax(file[2]>0.0083)
    end = zero + 2000
    dt = file[0][1]-file[0][0]
    fs = 1/dt
    time = file[0]-file[0][zero]
    acc1 = (file[2]-np.mean(file[2][0:zero]))*1000/0.0961*48.7
    acc2 = (file[3]-np.mean(file[3][0:zero]))*1000/0.097*48.7
    force1 = cfc1000_filter(cfc1000_filter(acc1[zero:end], dt), dt)
    force2 = cfc1000_filter(cfc1000_filter(acc2[zero:end], dt), dt)
    time = time[zero:end]
    cor = integrate.cumtrapz(file[1][zero:end] * 1000 / 0.099, time, initial=0)
    fig = plt.figure(figsize=(12,4))
    plt.subplot(1, 2, 1)
    plt.plot(time, force1, color='red', linestyle='-', label=np.max(force1))
    plt.plot(time, force2, color='green', linestyle='-', label=np.max(force2))
    plt.xlim(left=0)
    plt.ylim(bottom=0)
    plt.xlabel('Time (s)')
    plt.ylabel('Contact force (N)')
    plt.legend()
    plt.subplot(1, 2, 2)
    plt.plot(time, cor, color='blue', linestyle='-')
    plt.xlim(left=0)
#    plt.ylim(bottom=0)
    plt.xlabel('Time (s)')
    plt.ylabel('Velocity (m/s)')
    plt.tight_layout()
    fig.savefig(output_folder+filename+'.jpg')
    plt.close(fig)
    merged = np.vstack((time,force1,force2,cor))
    np.savetxt(output_folder+filename, merged.transpose(), delimiter=',')
