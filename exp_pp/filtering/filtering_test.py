# -------------------------
# filtering_test.py
# -------------------------

# -------------------------
# Description:
# - Test if filtering pass correctly
#
# Last edit: 31.08. 2022
# -------------------------

import numpy as np
import matplotlib.pyplot as plt

# Data loading
data1 = np.loadtxt("lv5_lg_out/05_2cm_export_trim.csv", delimiter=",")
data2 = np.loadtxt("lv5_lg_in/05_2cm_export_trim.csv", delimiter=",", skiprows=1)

# Plot 1
#plt.plot(data1[:, 0], data1[:, 1], label="out")
plt.plot(data2[:, 0], data2[:, 1], label="in")
plt.legend()
plt.show()

# Plot 2
#plt.plot(data1[:, 0], data1[:, 5], label="out")
plt.plot(data2[:, 0], data2[:, 5], label="in")
plt.legend()
plt.show()
