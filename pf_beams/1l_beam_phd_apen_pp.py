# -------------------------
# 1l_beam_test_pp.py
# -------------------------

# -------------------------
# Description:
# - Postprocess file for Tests of beam model
#
# Last edit: 27.03. 2023
# -------------------------

import numpy as np
import matplotlib.pyplot as plt

# -------------------------
# Numerical calculations
# -------------------------
dat_num_01 = np.loadtxt("solutions/beam_test_016/stress_data.txt", skiprows=3)
dat_num_02 = np.loadtxt("solutions/beam_test_02/stress_data.txt", skiprows=3)
dat_num_03 = np.loadtxt("solutions/beam_test_03/stress_data.txt", skiprows=3)
dat_num_04 = np.loadtxt("solutions/beam_test_04/stress_data.txt", skiprows=3)
dat_num_05 = np.loadtxt("solutions/beam_test_05/stress_data.txt", skiprows=3)
dat_num_06 = np.loadtxt("solutions/beam_test_06/stress_data.txt", skiprows=3)
dat_num_07 = np.loadtxt("solutions/beam_test_07/stress_data.txt", skiprows=3)
dat_num_08 = np.loadtxt("solutions/beam_test_08/stress_data.txt", skiprows=3)
dat_num_09 = np.loadtxt("solutions/beam_test_09/stress_data.txt", skiprows=3)
dat_num_10 = np.loadtxt("solutions/beam_test_10/stress_data.txt", skiprows=3)
dat_num_11 = np.loadtxt("solutions/beam_test_11/stress_data.txt", skiprows=3)
dat_num_12 = np.loadtxt("solutions/beam_test_12/stress_data.txt", skiprows=3)
dat_num_13 = np.loadtxt("solutions/beam_test_013/stress_data.txt", skiprows=3)
dat_num_14 = np.loadtxt("solutions/beam_test_014/stress_data.txt", skiprows=3)
dat_num_15 = np.loadtxt("solutions/beam_test_015/stress_data.txt", skiprows=3)

dat_log_01 = np.loadtxt("solutions/beam_test_01/log_file.txt", skiprows=3)
dat_log_02 = np.loadtxt("solutions/beam_test_02/log_file.txt", skiprows=3)
dat_log_03 = np.loadtxt("solutions/beam_test_03/log_file.txt", skiprows=3)
dat_log_04 = np.loadtxt("solutions/beam_test_04/log_file.txt", skiprows=3)
dat_log_05 = np.loadtxt("solutions/beam_test_05/log_file.txt", skiprows=3)
dat_log_06 = np.loadtxt("solutions/beam_test_06/log_file.txt", skiprows=3)
dat_log_07 = np.loadtxt("solutions/beam_test_07/log_file.txt", skiprows=3)
dat_log_08 = np.loadtxt("solutions/beam_test_08/log_file.txt", skiprows=3)
dat_log_09 = np.loadtxt("solutions/beam_test_09/log_file.txt", skiprows=3)
dat_log_10 = np.loadtxt("solutions/beam_test_10/log_file.txt", skiprows=3)
dat_log_11 = np.loadtxt("solutions/beam_test_11/log_file.txt", skiprows=3)
dat_log_12 = np.loadtxt("solutions/beam_test_12/log_file.txt", skiprows=3)
dat_log_13 = np.loadtxt("solutions/beam_test_013/log_file.txt", skiprows=3)
dat_log_14 = np.loadtxt("solutions/beam_test_014/log_file.txt", skiprows=3)
dat_log_15 = np.loadtxt("solutions/beam_test_015/log_file.txt", skiprows=3)

# -------------------------
# Parameters of task
# -------------------------
h = 0.02  # Thickness
lc = 0.1  # Process length
ft = 45.0e6  # Tensile strength
E_glass = 70.0e9  # Young's modulus
Gf = lc * 8.0 / 3.0 * ft ** 2 / E_glass  # Fracture toughness
L = 0.55  # Half of length
b = 0.36  # width
I = 1.0/12.0*b*h**3

# -------------------------
# Analytical model
# -------------------------
prephi = np.linspace(0.06, 0.3, 100)
d1 = [1.0 - b*h*Gf*3.0*L*L/(E_glass*I*lc*8.0*prephi_i*prephi_i) for prephi_i in prephi]
lc = 0.5
Gf = lc * 8.0 / 3.0 * ft ** 2 / E_glass
d2 = [1.0 - b*h*Gf*3.0*L*L/(E_glass*I*lc*8.0*prephi_i*prephi_i) for prephi_i in prephi]
np.savetxt("solutions/anal_sol.txt", np.column_stack([prephi, d1]))

plt.title("Damage evolution")
plt.plot(prephi, d1)
plt.plot(prephi, d2)
plt.show()

plt.title("Damage evolution")
plt.plot(prephi, d1)
plt.plot(dat_log_01[:, 0], dat_log_01[:, 3], label="1")
#plt.plot(dat_log_02[:, 0], dat_log_02[:, 3], label="2")
#plt.plot(dat_log_03[:, 0], dat_log_03[:, 3], label="3")
#plt.plot(dat_log_04[:, 0], dat_log_04[:, 3], label="4")
#plt.plot(dat_log_05[:, 0], dat_log_05[:, 3], label="5")
#plt.plot(dat_log_06[:, 0], dat_log_06[:, 3], label="6")
#plt.plot(dat_log_07[:, 0], dat_log_07[:, 3], label="7")
#plt.plot(dat_log_08[:, 0], dat_log_08[:, 3], label="8")
#plt.plot(dat_log_09[:, 0], dat_log_09[:, 3], label="9")
#plt.plot(dat_log_10[:, 0], dat_log_10[:, 3], label="10")
#plt.plot(dat_log_11[:, 0], dat_log_11[:, 3], label="11")
#plt.plot(dat_log_12[:, 0], dat_log_12[:, 3], label="12")
plt.plot(dat_log_13[:, 0], dat_log_13[:, 3], label="13")
plt.plot(dat_log_14[:, 0], dat_log_14[:, 3], label="14")
plt.plot(dat_log_15[:, 0], dat_log_15[:, 3], label="15")
plt.legend()
plt.show()

plt.title("Stress evolution")
plt.plot(dat_num_01[:, 0], dat_num_01[:, 3], label="1")
#plt.plot(dat_num_02[:, 0], dat_num_02[:, 3], label="2")
#plt.plot(dat_num_03[:, 0], dat_num_03[:, 3], label="3")
#plt.plot(dat_num_04[:, 0], dat_num_04[:, 3], label="4")
#plt.plot(dat_num_05[:, 0], dat_num_05[:, 3], label="5")
#plt.plot(dat_num_06[:, 0], dat_num_06[:, 3], label="6")
#plt.plot(dat_num_07[:, 0], dat_num_07[:, 3], label="7")
#plt.plot(dat_num_08[:, 0], dat_num_08[:, 3], label="8")
#plt.plot(dat_num_09[:, 0], dat_num_09[:, 3], label="9")
#plt.plot(dat_num_10[:, 0], dat_num_10[:, 3], label="10")
#plt.plot(dat_num_11[:, 0], dat_num_11[:, 3], label="11")
#plt.plot(dat_num_12[:, 0], dat_num_12[:, 3], label="12")
plt.plot(dat_num_13[:, 0], dat_num_13[:, 3], label="13")
plt.plot(dat_num_14[:, 0], dat_num_14[:, 3], label="14")
plt.plot(dat_num_15[:, 0], dat_num_15[:, 3], label="15")
plt.legend()
plt.show()
