# -------------------------
# 1l_beam_test_pp.py
# -------------------------

# -------------------------
# Description:
# - Postprocess file for Tests of beam model
#
# Last edit: 27.03. 2023
# -------------------------

import numpy as np
import matplotlib.pyplot as plt

# -------------------------
# Numerical calculations
# -------------------------
dat_num_01 = np.loadtxt("solutions/beam_test_01/stress_data.txt", skiprows=3)
dat_num_02 = np.loadtxt("solutions/beam_test_02/stress_data.txt", skiprows=3)

plt.title("Stress evolution")
plt.plot(dat_num_01[:, 0], dat_num_01[:, 3], label="full")
plt.plot(dat_num_02[:, 0], dat_num_02[:, 3], label="numint")
plt.legend()
plt.show()
