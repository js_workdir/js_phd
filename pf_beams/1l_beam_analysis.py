# -------------------------
# 1l_beam_analysis.py
# -------------------------

# -------------------------
# Description:
# - Tests of beam model
# - Analysis of analytical models
# - Comparison of analytical model and numerical model
#
# Last edit: 11.01. 2022
# -------------------------

import matplotlib.pyplot as plt
import numpy as np
import math

# -------------------------
# Parameters of task
# -------------------------
h = 0.02  # Thickness
lc = 10*0.0009999999999998899  # Process length
ft = 45.0e6  # Tensile strength
E_glass = 70.0e9  # Young's modulus
Gf = lc * 8.0 / 3.0 * ft ** 2 / E_glass  # Fracture toughness
L = 0.55  # Half of length
b = 0.36  # width

I = 1.0/12.0*b*h**3

# -------------------------
# Analytical model
# -------------------------
prephi = np.linspace(0.06, 0.08, 100)
d = [1.0 - b*h*Gf*3.0*L*L/(E_glass*I*lc*8.0*prephi_i*prephi_i) for prephi_i in prephi]

# -------------------------
# Numerical calculations
# -------------------------
dat_num_full = np.loadtxt("solutions/beam_basic_model_fulll/log_file.txt", skiprows=3)
dat_num_numint = np.loadtxt("solutions/beam_basic_model_numint/log_file.txt", skiprows=3)
dat_num_full_st = np.loadtxt("solutions/beam_basic_model_fulll/stress_data.txt", skiprows=3)
dat_num_numint_st = np.loadtxt("solutions/beam_basic_model_numint/stress_data.txt", skiprows=3)
dat_num_full_re = np.loadtxt("solutions/beam_basic_model_fulll/reaction_data.txt", skiprows=3)
dat_num_numint_re = np.loadtxt("solutions/beam_basic_model_numint/reaction_data.txt", skiprows=3)

# -------------------------
# Plotting
# -------------------------
plt.title("Damage evolution")
plt.plot(prephi, d)
plt.plot(dat_num_full[:, 0], dat_num_full[:, 3], "x", label="full")
plt.plot(dat_num_numint[:, 0], dat_num_numint[:, 3], label="numint")
plt.legend()
plt.show()

plt.title("Stress evolution")
plt.plot(dat_num_full_st[:, 0], dat_num_full_st[:, 3], label="full")
plt.plot(dat_num_numint_st[:, 0], dat_num_numint_st[:, 3], label="numint")
plt.legend()
plt.show()

plt.title("Reaction evolution")
plt.plot(dat_num_full_re[:, 0], dat_num_full_re[:, 2], label="full")
plt.plot(dat_num_numint_re[:, 0], dat_num_numint_re[:, 2], label="numint")
plt.legend()
plt.show()

lmc = 4*math.pi/(3*math.sqrt(3))
ut_ul = lmc*0.5/1.0
print(ut_ul)